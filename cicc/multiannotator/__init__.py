import numpy as np
from functools import wraps
import time

def cached (ctrl_var=None):
  def decorator (fn):
    attrname = '_{}'.format(fn.__name__)
    @wraps(fn)
    def wrapper (self, *args):
      if np.all([arg is None for arg in args]):
        if hasattr(self, attrname):
          ctrl = getattr(self, ctrl_var)
          ctrl_timestamp = ctrl[1]
          this = getattr(self, attrname)
          this_timestamp = this[1]
          if ctrl[0] and ctrl_timestamp < this_timestamp:
            return this[0]
        this = (fn(self), time.time())
        setattr(self, attrname, this)
        return this[0]
      else:
        return fn(self, *args)
    
    @wraps(fn)
    def wrapper2 (self, *args):
      if np.all([arg is None for arg in args]):
        if hasattr(self, attrname):
          return getattr(self, attrname)
        this = fn(self)
        setattr(self, attrname, this)
        return this
      else:
        return fn(self, *args)
    
    if ctrl_var is None:
      return wrapper2
    else:
      return wrapper
  return decorator

def alias (name):
  def decorator (fn):
    @wraps(fn)
    def wrapper (self, *args, **kwargs):
      return getattr(self, name)(*args, **kwargs)
    return wrapper
  return decorator

class AnnotationsData:
  @staticmethod
  def read_sql_export (path):
    with open(path, 'r') as f:
      content = f.read()
    
    data = np.array([
      [x[1:-1] for x in row.split(",")]
      for row in content.split("\n")
      if row != ""
    ])
    return AnnotationsData(data)
  
  def __init__ (self, data=None):
    if data is None:
      self.data = np.zeros((0,3))
    else:
      self.data = np.array(data)
    self._compiled = (False, 0)
  
  def add_data (self, item, label, annotator):
    self.data = np.concatenate([self.data, [[item, label, annotator]]])
    self._compiled[0] = False
  
  def select (self, column, value):
    return AnnotationsData(self.data[self.data[:,column] == value])
  
  def select_item (self, item):
    return self.select(0, item)
    
  def select_label (self, label):
    return self.select(1, label)
  
  def select_annotator (self, annotator):
    return self.select(2, annotator)
  
  def exclude (self, column, value):
    return AnnotationsData(self.data[self.data[:,column] != value])
  
  def exclude_item (self, item):
    return self.exclude(0, item)
    
  def exclude_label (self, label):
    return self.exclude(1, label)
  
  def exclude_annotator (self, annotator):
    return self.exclude(2, annotator)
  
  def elements (self, column=None):
    if column is None:
      return self.data
    return sorted(set(self.data[:,column]))
  
  @cached('_compiled')
  def items (self):
    return self.elements(0)
  
  @cached('_compiled')
  def labels (self):
    return self.elements(1)
  
  @cached('_compiled')
  def annotators (self):
    return self.elements(2)
  
  @cached('_compiled')
  def num (self, column=None):
    return len(self.elements(column))
  
  @cached('_compiled')
  def num_items (self):
    return self.num(0)
  
  @cached('_compiled')
  def num_labels (self):
    return self.num(1)
  
  @cached('_compiled')
  def num_annotators (self):
    return self.num(2)
  
  @cached('_compiled')
  def summary (self):
    items = self.items()
    i = self.num_items()
    labels = self.labels()
    n = self.num_labels()
    summary = np.zeros((i,n))
    for row in self.data:
      summary[items.index(row[0]),labels.index(row[1])] += 1
    return summary

  @cached('_compiled')
  def fleiss_observed_agreement (self):
    N = self.num()
    summary = self.summary()
    c = summary.sum(1)[:,np.newaxis]
    return np.sum((summary*(summary-1))/((c-1)))/N
  
  @cached('_compiled')
  def fleiss_expected_agreement (self):
    return np.sum(self.summary().sum(0)**2)/self.num()**2
  
  @cached('_compiled')
  def fleiss_kappa (self):
    Ao = self.fleiss_observed_agreement()
    Ae = self.fleiss_expected_agreement()
    return (Ao-Ae)/(1-Ae)
  
  @staticmethod
  def interval_distance (a, b):
    return (a-b)**2
  
  @staticmethod
  def nominal_distance (a, b):
    return 1 if a != b else 0
  
  @cached('_compiled')
  def krippendorff_observed_disagreement (self, distance=None):
    if distance is None:
      distance = self.nominal_distance
    N = self.num()
    i = self.num_items()
    n = self.num_labels()
    summary = self.summary()
    c = summary.sum(1)
    ret = np.zeros(i)
    for k1 in range(n):
      for k2 in range(n):
        ret += summary[:,k1]*summary[:,k2]*distance(k1,k2)
    return np.sum(ret/(c-1))/N
  
  @cached('_compiled')
  def krippendorff_expected_disagreement (self, distance=None):
    if distance is None:
      distance = self.nominal_distance
    n = self.num_labels()
    totals = self.summary().sum(0)
    ret = 0
    for k1 in range(n):
      for k2 in range(n):
        ret += totals[k1]*totals[k2]*distance(k1,k2)
    return np.sum(ret)/(self.num()*(self.num()-1))
  
  @cached('_compiled')
  def krippendorff_alpha (self, distance=None):
    Do = self.krippendorff_observed_disagreement(distance)
    De = self.krippendorff_expected_disagreement(distance)
    return 1 - Do/De
  
  @cached('_compiled')
  def frequencies (self):
    return self.summary()/self.summary().sum(1)[:,np.newaxis]
  
  @cached('_compiled')
  def gini_heterogeneity_index (self, normalized=None):
    gini = 1-(self.frequencies()**2).sum()/self.num_items()
    if normalized:
      max_gini = (self.num_labels()-1)/(self.num_labels())
      gini = gini/max_gini
    return gini
  
  @cached('_compiled')
  def gini_heterogeneity_index_normalized (self):
    return self.gini_heterogeneity_index(True)
  
  
  def compile (self):
    self._compiled = (True, time.time())
  
  def apply_normalization (self, fn):
    return PseudoAnnotationsData(fn(self.summary()))
  
  # aliases
  @alias('fleiss_observed_agreement')
  def observed_agreement (_IGNORED_):
    pass
  
  @alias('fleiss_expected_agreement')
  def expected_agreement (_IGNORED_):
    pass

  @alias('observed_agreement')
  def agreement (_IGNORED_):
    pass

  @alias('fleiss_kappa')
  def kappa (_IGNORED_):
    pass

  @alias('krippendorff_observed_disagreement')
  def observed_disagreement (_IGNORED_):
    pass

  @alias('krippendorff_expected_disagreement')
  def expected_disagreement (_IGNORED_):
    pass

  @alias('observed_disagreement')
  def disagreement (_IGNORED_):
    pass

  @alias('krippendorff_alpha')
  def alpha (_IGNORED_):
    pass

  @alias('gini_heterogeneity_index')
  def gini_index (_IGNORED_):
    pass

  @alias('gini_index')
  def gini (_IGNORED_):
    pass

  @alias('gini_heterogeneity_index_normalized')
  def gini_index_normalized (_IGNORED_):
    pass

  @alias('gini_index_normalized')
  def gini_normalized (_IGNORED_):
    pass

def remove (fn):
  @wraps(fn)
  def wrapper (self, *args, **kwargs):
    raise AttributeError("'{}' object has not attribute '{}'".format(self.__class__.__name__, fn.__name__))
  return wrapper

class PseudoAnnotationsData (AnnotationsData):
  @remove
  def add_data (_IGNORE_):
    pass
  
  @remove
  def select (_IGNORE_):
    pass
  
  @remove
  def exclude (_IGNORE_):
    pass
  
  @remove
  def elements (_IGNORE_):
    pass
  
  @remove
  def num_annotators (_IGNORE_):
    pass
  
  @staticmethod
  def from_annotations_data (ad):
    return PseudoAnnotationsData(ad.summary())
  
  def __init__ (self, summary):
    self._summary = (np.array(summary), 1)
    self._compiled = (True, 0)
  
  def select_item (self, item):
    return PseudoAnnotationsData(self.summary()[item])
    
  def select_label (self, label):
    return PseudoAnnotationsData(self.summary()[:,label])
  
  @cached('_compiled')
  def num (self, column=None):
    if column is None:
      return self.summary().sum()
    return self.summary().shape[column]
  
  @cached('_compiled')
  def summary (self):
    pass

  @cached('_compiled')
  def fleiss_observed_agreement (self):
    d = self.frequencies()
    return np.sum(d**2)/self.num_items()
  
  @cached('_compiled')
  def fleiss_expected_agreement (self):
    d = self.frequencies()
    return np.sum(d.sum(0)**2)/self.num_items()**2
  
  @cached('_compiled')
  def krippendorff_observed_disagreement (self, distance=None):
    if distance is None:
      distance = self.nominal_distance
    i = self.num_items()
    n = self.num_labels()
    d = self.frequencies()
    ret = np.zeros(i)
    for k1 in range(n):
      for k2 in range(n):
        ret += d[:,k1]*d[:,k2]*distance(k1,k2)
    return np.sum(ret)/i
  
  @cached('_compiled')
  def krippendorff_expected_disagreement (self, distance=None):
    if distance is None:
      distance = self.nominal_distance
    i = self.num_items()
    n = self.num_labels()
    totals = self.frequencies().sum(0)
    ret = 0
    for k1 in range(n):
      for k2 in range(n):
        ret += totals[k1]*totals[k2]*distance(k1,k2)
    return np.sum(ret)/i**2
