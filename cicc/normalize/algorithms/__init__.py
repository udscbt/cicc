import os
import importlib
from warnings import warn
from cicc.utils import SubmoduleImporter

algorithms_dir = os.path.dirname(os.path.abspath(__file__))
geometry_dir   = os.path.join(algorithms_dir, 'geometry')
brightness_dir = os.path.join(algorithms_dir, 'brightness')
default_algorithms = {
  'geometry': SubmoduleImporter(
    __name__+".geometry",
    geometry_dir
  ),
  'brightness': SubmoduleImporter(
    __name__+".brightness",
    brightness_dir
  ),
}

default_algorithms['geometry'].reload()
default_algorithms['geometry']['none'] = None
default_algorithms['brightness'].reload()

def correct_brightness (img, name):
  if name not in default_algorithms['brightness']:
    raise ValueError(
      "Unregistered brightness algorithm: '{}'".format(algorithm)
    )
  return default_algorithms['brightness'][name](img)
