import numpy as np
from .utils import *

class Radar:
  CIRCLE = 'circle'
  POLYGON = 'polygon'
  
  CENTER = 'center'
  EXTERNAL = 'external'
  INTERNAL = 'internal'
  
  OPTIONS = {
    'line_color' : [(255,0,0), (0,255,0), (0,0,255)],
    'line_stroke' : 3,
    'line_fill_color' : TRANSPARENT,
    'ghost_color' : None,
    'ghost_stroke' : None,
    'ghost_fill_color': None,
    'axis_color' : (0,0,0),
    'axis_stroke' : 1,
    'boundary_shape' : CIRCLE,
    'boundary_color' : None,
    'boundary_stroke' : 2,
    # ~ 'radial_positions' : None,
    # ~ 'radial_color' : None,
    # ~ 'radial_stroke' : 1,
    'background_color' : TRANSPARENT,
    'maximum' : 1,
    'label_stroke' : None,
    'label_color' : None,
    'label_box_stroke' : None,
    'label_box_color' : TRANSPARENT,
    'label_box_fill_color' : TRANSPARENT,
    'label_font' : 0,
    'label_size' : 1,
    'label_relsize' : None,
    'label_position' : EXTERNAL,
  }
  
  FALLBACK = {
    'line_fill_color' : 'line_color',
    'ghost_color' : 'line_color',
    'ghost_stroke' : 'line_stroke',
    'ghost_fill_color': 'line_fill_color',
    'boundary_color' : 'axis_color',
    'boundary_stroke' : 'axis_stroke',
    'radial_color' : 'boundary_color',
    'radial_stroke' : 'boundary_stroke',
    'label_stroke' : 'boundary_stroke',
    'label_color' : 'boundary_color',
    'label_box_stroke' :  'label_stroke',
    'label_box_color' : 'label_color',
    'label_box_fill_color' : 'background_color',
  }
  
  def __init__ (
    self,
    axes=5,
    persistent=0,
    **kwargs
  ):
    for k in kwargs:
      if k not in self.OPTIONS:
        raise TypeError("__init__() got an unexpected keyword argument '{}'".format(k))
    
    if not isinstance(axes, list):
      if isinstance(axes, int):
        axes = list(range(axes))
      else:
        raise TypeError("axes must be either a list or an int")
    
    self.axes = axes
    self.persistent = persistent
    self.config = self.OPTIONS.copy()
    self.config.update(kwargs)
    
    self.angles = np.arange(len(self.axes))*2*np.pi/len(self.axes)
    self.positions = np.array([np.cos(self.angles), np.sin(self.angles)]).transpose()
    
    self.data = {0:self.Curve(self)}
  
  def get_config (self, key=None):
    if key is None:
      return {k:self.get_config(k) for k in self.config}
    if self.config[key] is None and key in self.FALLBACK:
      return self.get_config(self.FALLBACK[key])
    return self.config[key]
  
  def set_config (self, key, value):
    self.config[key] = value
  
  def __setitem__ (self, key, value):
    self.set_config(key, value)
  
  def __getitem__ (self, key):
    return self.get_config(key)
  
  class Curve:
    def __init__ (self, radar):
      self.lines = [np.array([0 for axis in radar.axes], dtype=float) for ghost in range(radar.persistent+1)]
      self.current = -1
      self.next = 0
      self.radar = radar
    
    def update_persistent (self):
      while self.radar.persistent+1 < len(self.lines):
        if self.next >= len(self.lines):
          self.next = 0
          self.current = -1
        self.lines.pop(self.next)
      while self.radar.persistent+1 > len(self.lines):
        self.lines.insert(self.next, 0)
    
    def shift (self):
      self.current = self.next
      self.next += 1
      self.next %= len(self.lines)
    
    def set (self, axis, value, shift=True):
      self.update_persistent()
      
      self.lines[self.next][axis] = value
      
      if shift:
        self.shift()
    
    def get (self, axis=None, lag=0):
      self.update_persistent()
      line = self.lines[(self.current-lag)%len(self.lines)]
      if axis is None:
        return line
      return line[axis]
  
  def set (self, curve, axis=None, value=None, shift=True):
    """
      set(curve, axis, value)
      set(curve, values)
      set(axis, value)
      set(values)
    """
    if value is None:
      try:
        axis = iter(axis)
      except TypeError:
        self.set(0, curve, axis, shift)
      else:
        for a, v in zip(self.axes, axis):
          self.set(curve, a, v, False)
        if shift:
          self.data[curve].shift()
      finally:
        return
    
    if axis not in self.axes:
      raise ValueError ("Axis {} not defined".format(axis, self.axes))
    
    if curve not in self.data:
      self.data[curve] = self.Curve(self)
    
    self.data[curve].set(self.axes.index(axis), value, shift)
  
  def get (self, curve=None, axis=None, lag=0):
    """
      get(curve, axis, lag)
      get(curve, axis)
      get(curve)
      get()
    """
    if curve is None:
      curve = 0
    return self.data[curve].get(axis, lag)
  
  def cv2_draw (self, img, center, radius, show_labels=False):
    import cv2
    
    maximum = self['maximum']
    if maximum is None:
      maximum = max([
        value
        for curve in self.data.values()
        for line in curve.lines
        for value in line
      ])
    
    scale = radius/maximum
    original_image = img.copy()
    mask = np.zeros_like(img, dtype=float)
    
    npcenter = center[::-1]
    
    bs = self['boundary_stroke']
    bc, bca = split_alpha(self['boundary_color'])
    bg, bga = split_alpha(self['background_color'])
    
    pts = (self.positions*radius+center).astype(np.int32)
    if self['boundary_shape'] == Radar.CIRCLE:
      Cv2Alpha.do(bga, cv2.circle, img, center, radius, bg, -1)
      Cv2Alpha.do(bca, cv2.circle, img, center, radius, bc, bs)
      cv2.circle(mask, center, radius, (1,1,1), -1)
    elif self['boundary_shape'] == Radar.POLYGON:
      Cv2Alpha.do(bga, cv2.fillPoly, img, [pts], bg)
      Cv2Alpha.do(bca, cv2.polylines, img, [pts], True, bc, bs)
      cv2.fillPoly(mask, [pts], (1,1,1))
    color, alpha = split_alpha(self['axis_color'])
    stroke = self['axis_stroke']
    
    queue = Cv2Alpha(img, alpha)
    for point in pts:
      queue.add_command(cv2.line, center, tuple(point), color, stroke)
    queue.apply()
    
    def ensure_list (x):
      try:
        x = list(x)
      except TypeError:
        x = [x]
      return x
    colors = ensure_list(self['line_color'])
    fillcolors = ensure_list(self['line_fill_color'])
    gcolors = ensure_list(self['ghost_color'])
    gfillcolors = ensure_list(self['ghost_fill_color'])
    strokes = ensure_list(self['line_stroke'])
    gstrokes = ensure_list(self['ghost_stroke'])
    curves = [
      (
        curve,
        colors[i%len(colors)],
        fillcolors[i%len(fillcolors)],
        strokes[i%len(strokes)],
        gcolors[i%len(gcolors)],
        gfillcolors[i%len(gfillcolors)],
        gstrokes[i%len(gstrokes)]
      )
      for i, curve in enumerate(self.data.values())
    ]
    for curve, color, fillcolor, stroke, gcolor, gfillcolor, gstroke in curves:
      for idx, line in enumerate(curve.lines):
        pts = (self.positions*scale*line[:,np.newaxis]+center).astype(np.int32)
        if idx == curve.current:
          continue
        else:
          beta = ((curve.current-idx)%(self.persistent+1))/(self.persistent+1)
          alpha = (1-beta)
          gcolor, gcoloralpha = split_alpha(gcolor)
          gfillcolor, gfillalpha = split_alpha(gfillcolor)
          Cv2Alpha.do(alpha*gfillalpha, cv2.fillPoly, img, [pts], gfillcolor)
          Cv2Alpha.do(alpha*gcoloralpha, cv2.polylines, img, [pts], True, gcolor, gstroke)
      line = curve.lines[curve.current]
      pts = (self.positions*scale*line[:,np.newaxis]+center).astype(np.int32)
      color, alpha = split_alpha(color)
      fillcolor, fillalpha = split_alpha(fillcolor)
      Cv2Alpha.do(fillalpha, cv2.fillPoly, img, [pts], fillcolor)
      Cv2Alpha.do(alpha, cv2.polylines, img, [pts], True, color, stroke)
    
    img[:] = (img*mask + original_image*(1-mask)).astype(np.uint8)
    
    stroke = self['label_stroke']
    color, alpha = split_alpha(self['label_color'])
    size = (
      self['label_size']
      if self['label_relsize'] is None
      else radius * self['label_relsize']
    )
    font = self['label_font']
    
    box_color, box_alpha = split_alpha(self['label_box_color'])
    box_stroke = self['label_box_stroke']
    fillcolor, fillalpha = split_alpha(self['label_box_fill_color'])
    
    pts = (self.positions*radius+center).astype(np.int32)
    if show_labels:
      if show_labels == True:
        labels = self.axes
      else:
        labels = show_labels
      queues = True
      for point, lbl in zip(pts, labels):
        if lbl is not None:
          x,y = point
          if self['label_position'] == self.EXTERNAL:
            if y > center[1]:
              anchor = 'top'
            elif y < center[1]:
              anchor = 'bottom'
            else:
              anchor = 'center'
            if x > center[0]:
              anchor += ' left'
            elif x < center[0]:
              anchor += ' right'
            else:
              anchor += ' center'
          elif self['label_position'] == self.INTERNAL:
            if y > center[1]:
              anchor = 'bottom'
            elif y < center[1]:
              anchor = 'top'
            else:
              anchor = 'center'
            if x > center[0]:
              anchor += ' right'
            elif x < center[0]:
              anchor += ' left'
            else:
              anchor += ' center'
          elif self['label_position'] == self.CENTER:
            anchor = 'center'
          else:
            anchor = self['label_position']
          
          queues = putText_ex (
            img, lbl, (x,y),
            self['label_font'],
            self['label_size'],
            self['label_color'],
            self['label_stroke'],
            anchor,
            self['label_box_color'],
            self['label_box_stroke'],
            self['label_box_fill_color'],
            queues
          )
      if queues is not True:
        for queue in queues:
          queue.apply()
    return img
