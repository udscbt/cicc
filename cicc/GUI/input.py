import time
import threading
import cv2
import numpy as np
from tkinter import *
from tkinter.ttk import *
from tkinter.filedialog import askopenfilename
from tkinter import messagebox
from .image import ImagePanel
from .webcam import Webcam
from ..normalize import landmarks

class InputPanel (ImagePanel):
  FACE_COLORS = [
    (0,255,0), # Green
    (0,255,255), # Yellow
    (0,0,255), # Red
    (255,0,255), # Magenta
    (255,0,0), # Blue
    (255,255,0), # Cyan
  ]
  FACE_INDEX = 0
  
  def face_color (self, i):
    return self.FACE_COLORS[i%len(self.FACE_COLORS)]
  
  def __init__ (
    self,
    *args,
    webcam_shape=None,
    webcam_maxshape=None,
    webcam_keep_ratio=False,
    webcam_fps=10,
    max_landmark_updates=5,
    use_landmarks=True,
    **kwargs
  ):
    self.webcam = Webcam(callback=self.webcam_loop_fn)
    self.webcam_timer = None
    
    self.webcam_fps = webcam_fps
    self.webcam_keep_ratio = webcam_keep_ratio
    self.webcam_shape = webcam_shape
    self.webcam_allow_enlarge = True
    if webcam_shape is None:
      self.webcam_shape = webcam_maxshape
      self.webcam_allow_enlarge = False
    if isinstance(self.webcam_shape, int):
      self.webcam_shape = (self.webcam_shape, self.webcam_shape)
    
    ImagePanel.__init__(self, *args, **kwargs)
    
    menubar = self.master.menubar
    
    self.imgmenu = Menu(menubar, tearoff=0)
    self.imgmenu.add_command(label="Empty", command=self.empty)
    self.imgmenu.add_command(label="Open file...", command=self.open)
    self.imgmenu.add_command(label="Grab from webcam", command=self.webcam_start)
    
    self.viewmenu = Menu(menubar, tearoff=0)
    
    if use_landmarks:
      self.computations_per_second = max_landmark_updates
      
      self.showface = BooleanVar()
      self.showland = BooleanVar()
    
      self.showface.trace('w', lambda *args: self.showimg(self.box))
      self.showland.trace('w', lambda *args: self.showimg(self.box))
      self.viewmenu.add_checkbutton(label="Show faces", variable=self.showface)
      self.viewmenu.add_checkbutton(label="Show landmarks", variable=self.showland)
      self.imagepanel.bind('<1>', self.selectface)
    
      self.faces = {}
      self.add_augmentation(self.compute_landmarks, False)
      self.add_augmentation(self.add_landmarks, True)
    
    menubar.add_cascade(label="Image", menu=self.imgmenu)
    menubar.add_cascade(label="View", menu=self.viewmenu)
    
    self.empty()
    
  def destroy (self, *args, **kwargs):
    self.webcam_stop(hard=True)
    super().destroy(*args, **kwargs)
  
  lastupdate=time.time()
  def compute_landmarks (self, IP):
    now = time.time()
    if (
      self.computations_per_second == 0 or
      now-self.lastupdate < 1/self.computations_per_second
    ):
      return IP.image
    self.lasttime = now
    img = IP.image
    faces = landmarks.get_face(img)
    self.landmarks = {}
    if len(faces) > 0:
      indexes = landmarks.get_faces_indexes(faces, self.faces, counter=self.FACE_INDEX)
      self.faces = {i:face for i,face in zip(indexes, faces)}
      self.FACE_INDEX = max(self.FACE_INDEX-1, *self.faces.keys())+1
      for i, face in self.faces.items():
        landmarks.EXTERNALLY_FOUND.clear()
        lms = landmarks.detect(img,face)
        self.landmarks[i] = lms
    else:
      self.faces = {}
    return img
    
  def add_landmarks (self, IP, img, scale):
    if self.showface.get():
      for i, face in self.faces.items():
        face = (
          face.left(),
          face.top(),
          face.right(),
          face.bottom()
        )
        p1, p2 = self.panelcoords(face, scale)
        cv2.rectangle(
          img,
          p1,
          p2,
          self.face_color(i),
          2
        )
    if self.showland.get():
      for lms in self.landmarks.values():
        for point in self.panelcoords(lms, scale):
          cv2.circle(
            img,
            point,
            2,
            (255,0,0),
            -1
          )
    return img
  
  def selectface (self, e):
    if self.showface.get():
      x,y = np.array(self.imagecoords((e.x, e.y))[0])
      for face in self.faces.values():
        fx, fy, fX, fY = face.left(), face.top(), face.right(), face.bottom()
        if (
          x > fx and x < fX and
          y > fy and y < fY
        ):
          self.showimg([fx,fy,fX,fY])
          break
  
  def empty (self):
    self.webcam_stop()
    self.setimg(None)
  
  def open (self):
    self.webcam_stop()
    filename = askopenfilename(parent=self)
    if filename == '':
      return
    img = cv2.imread(filename)
    if img is None:
      messagebox.showerror(
        "Wrong file",
        "The file {} could not be opened as image".format(filename),
        parent=self
      )
    else:
      self.setfull()
      self.setimg(img)
  
  def webcam_start (self):
    self.setfull()
    self.webcam.start()
    self.webcam_loop()
    self.imgmenu.entryconfig(
      2,
      label="Stop webcam",
      command=self.webcam_stop
    )
  
  def webcam_stop (self, hard=False):
    if self.webcam_timer is not None:
      self.after_cancel(self.webcam_timer)
      self.webcam_timer = None
    self.webcam.stop()
    if hard:
      self.webcam.hard_stop()
      # ~ self.webcam.wait_stop()
    else:
      self.imgmenu.entryconfig(
        2,
        label="Grab from webcam",
        command=self.webcam_start
      )
  
  def webcam_loop_fn (self, webcam_image):
    # Return immediately and do tkinter stuff in mainloop
    self.after(1, lambda wi=webcam_image: self._webcam_loop_fn(wi))
  
  def _webcam_loop_fn (self, webcam_image):
    if self.webcam_shape is not None:
      H,W = webcam_image.shape[:2]
      w,h = self.webcam_shape
      wratio = w/W
      hratio = h/H
      if self.webcam_keep_ratio:
        wratio = hratio = min(wratio, hratio)
      if not self.webcam_allow_enlarge:
        wratio = min(wratio, 1)
        hratio = min(hratio, 1)
      if wratio != 1 or hratio != 1:
        webcam_image = cv2.resize(webcam_image, (int(W*wratio), int(H*hratio)))
    self.setimg(webcam_image, self.box)
  
  lastwebcam=time.time()
  def webcam_loop (self):
    if self.webcam.active:      
      self.webcam.update()
      needed = 1000/self.webcam_fps
      now = time.time()
      used = 1000*(now-self.lastwebcam)
      self.lastwebcam=now
      missing = max(int(needed-used),1)
      self.webcam_timer = self.after(
        missing,
        self.webcam_loop
      )
