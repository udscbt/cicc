import time
import keras
import numpy as np
from cicc.dataset import BatchGenerator
from . import AnnotationsData, cached

class Filelist:
    def __init__ (self, files, targets, labels, normalize):
        self.files = files
        self.rawtargets = targets
        self.labels = labels
        self.normalize = normalize
    
    @property
    def rawtargets (self):
        return self._rawtargets
    
    @rawtargets.setter
    def rawtargets (self, targets):
        self._targetcontrol = (True, time.time())
        self._rawtargets = targets
    
    @property
    @cached('_targetcontrol')
    def targets (self):
        if self.normalize is None: return self.rawtargets
        else: return self.normalize(self.rawtargets)
    
    @staticmethod
    def SUM_TO_ONE (data):
        return data/data.sum(-1)[...,np.newaxis]
    
    @staticmethod
    def MAX_TO_ONE (data):
        return data/data.max(-1)[...,np.newaxis]
    
    @staticmethod
    def BETWEEN_ZERO_AND_ONE (data):
        data = data-data.min(-1)[...,np.newaxis]
        return data/data.max(-1)[...,np.newaxis]
    
    @staticmethod
    def SOFTMAX (data):
        e = np.exp(data)
        return e/e.sum(-1)[...,np.newaxis]
    
    @staticmethod
    def SOFTMAX2 (data):
        return Filelist.SOFTMAX(Filelist.SUM_TO_ONE(data))
    
    @staticmethod
    def read (path, normalize=None):
        with open(path, 'r') as f:
            content = f.read()
        filelist = np.array([
            row.split(",")
            for row in content.split("\n")
            if row.strip() != ""
               and not row.strip().startswith("#")
        ])
        try:
            filelist[0,1:].astype(float)
            labels = np.arange(filelist.shape[1]-1)
        except ValueError:
            labels = filelist[0,1:]
            filelist = filelist[1:]
        files = filelist[:,0]
        targets = filelist[:,1:].astype(float)
        return Filelist(files, targets, labels, normalize)
    
    @staticmethod
    def from_annotations_data (ann_data, normalize=None):
        files = ann_data.items()
        targets = ann_data.summary()
        labels = ann_data.labels()
        return Filelist(files, targets, labels, normalize)
    
    def write (self, path):
        table = [
            ["Path", *self.labels],
            *np.concatenate([self.files.reshape(-1,1), self.targets], 1)
        ]
        with open(path, 'w') as f:
            f.write("\n".join([",".join(list(map(str(row)))) for row in table]))
    
    def use_labels (self, labels, normalize=None):
        self.rawtargets = self.rawtargets[
            :,
            [
                self.labels.index(lbl)
                for lbl
                in labels
            ]
        ]
        self.labels = labels
    
    def focus_on (self, labels, normalize=None):
        self.rawtargets[
            :,
            [
                idx
                for idx,lbl
                in self.labels
                if lbl not in labels
            ]
        ] = 0
        

class BatchGenerator_multi (BatchGenerator):

    def __init__(self, verbose=False):
        BatchGenerator.__init__(self, verbose)
    
    def clear(self):
        self._classes = None
        self.filelist = []
        self.totalsamples = 0

    def initialize(self, filelist, normalize=None):
        self.print("Initializing generator")
        self.load_dir(filelist, normalize)
        self.reset()

    def load_dir(self, filelist, normalize=None):
        self.clear()
        
        self.print("Loading file list")
        if isinstance(filelist, Filelist):
            self.filelist = filelist
        elif isinstance(filelist, AnnotationsData):
            self.filelist = Filelist.from_annotations_data(filelist, normalize)
        else:
            if os.path.abspath(filelist) != filelist: # relative path
                filelist = self.getPath(filelist)
            self.filelist = Filelist.read(filelist, normalize)
        
        if self.labels is not None:
            self.filelist.use_labels(self.labels)
        self._classes = self.filelist.labels
        self.print("{} class(es) found".format(len(self._classes)))

        if self.used is not None:
            self.filelist.focus_on(self.used)
        
        self.filelist.files = np.array([self.getPath(path) for path in self.filelist.files])
        if self.weights is not None:
            self.weightlist = np.array([
                self.weights(self, path, target)
                for path, target
                in zip(self.filelist.files, self.filelist.targets)
            ])
        else:
            self.weightlist = np.ones_like(self.filelist.files, dtype=float)
        self.totalsamples = len(self.filelist.files)
        self.print("Total: {} image(s)".format(self.totalsamples))

    def getNumSamples(self):
        return self.totalsamples


class FullDatasetGenerator_multi (BatchGenerator_multi, keras.utils.Sequence):

    def __init__(self, verbose=False):
        BatchGenerator_multi.__init__(self, verbose)

    def reset(self):
        BatchGenerator_multi.reset(self)
        self.print("Reordering samples order")
        self._idx = 0
        self.indices = np.arange(self.getNumSamples())

    def randomize(self):
        self.reset()
        self.print("Randomizing samples order")
        shuffle(self.indices)

    def __len__(self):
        fixed_len = BatchGenerator_multi.__len__(self)
        if fixed_len is None:
            return int(np.ceil(self.getNumSamples() / self.getBatchSize()))
        else:
            return fixed_len

    def next_filename(self):
        try:
            self._idx, batch = self.get_filename_from(self._idx)
        except IndexError:
            raise StopIteration

        return batch

    def get_filename_from(self, idx):
        numsamples = self.getNumSamples()
        if self.numsamples is not None:
            numsamples = min(numsamples, self.numsamples)
        
        if idx >= numsamples:
            raise IndexError(
                "Trying to access image {}".format(idx) +
                " but only {} are available".format(self.getNumSamples()))

        indices = self.indices[idx:idx+self.batch_size]
        batch_in = self.filelist.files[indices]
        batch_out = self.filelist.targets[indices]
        batch_weights = self.weightlist[indices]
        if self.use_weights:
            return (idx+len(indices), (batch_in, batch_out, batch_weights))
        else:
            return (idx+len(indices), (batch_in, batch_out))
    
    def get_from(self, idx):
        idx, batch = self.get_filename_from(idx)
        return idx, self.__next__(batch)

    def __getitem__(self, index):
        _, batch = self.get_from(index * self.batch_size)
        return batch
