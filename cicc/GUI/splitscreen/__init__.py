import os
from ..localization import STRINGS, load_lang_file

load_lang_file (
  os.path.join(
    os.path.dirname(__file__),
    'default.lang'
  )
)

from .splitscreen import SplitFrame
from .selectionbar import SelectionBar
from .common import *
from .layout import *
