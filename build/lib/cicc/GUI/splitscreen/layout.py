import os
from .splitscreen import *

class Profile:
  PROFILES_DIR = "profiles"
  DEFAULT_PATH = os.path.join(PROFILES_DIR, "default.prf")
  
  def __init__ (self, name=None):
    self.lines = []
    self.screens = []
    self.name = name
    
    self.bar = False
    self.bg = "white"
    self.line_width = 1
    self.line_color = "black"
    self.sline_width = 1
    self.sline_color = "gray"
  
  def __str__ (self):
    if self.name is None:
      return "Custom profile"
    else:
      return self.name
  
  def fromFrame (self, frame):
    self.bar = SplitFrame.SplitScreen.SEPARATED_BAR
    self.bg = SplitFrame.SplitScreen.BG_COLOR
    self.line_width = SplitFrame.RealLine.WIDTH
    self.line_color = SplitFrame.RealLine.LINE_COLOR
    self.sline_width = SplitFrame.SplitScreen.SLINE_BD
    self.sline_color = SplitFrame.SplitScreen.SLINE_COLOR
    
    self.lines = []
    for line in frame.lines:
      if line.base is not None:
        s = frame.lines.index(line.base[0])
        e = frame.lines.index(line.base[1])
      else:
        s, e = -1, -1
      self.lines.append({
        'pos'   : line.position,
        'start' : s,
        'end'   : e,
        'vert'  : line.vertical
      })
    self.screens = []
    for screen in frame.screens:
      ty = screen.content.__class__.__name__
      l, t, r, b = [frame.lines.index(virtual.real) for virtual in screen.boundary]
      self.screens.append({
        'type'   : ty,
        'left'   : l,
        'top'    : t,
        'right'  : r,
        'bottom' : b
      })
  
  def save (self, path):
    f = open(path, "w")
    f.write("#Name\n{}\n".format(str(self)))
    f.write("#Appearance settings\n")
    f.write("#Separated bar\tBackground\tLine width\tLine color\tTemp line width\tTemp line color\n")
    f.write("{}\t{}\t{}\t{}\t{}\t{}\n".format(
      1 if self.bar else 0,
      self.bg,
      self.line_width,
      self.line_color,
      self.sline_width,
      self.sline_color
    ))
    f.write("#Lines number\n")
    f.write("{}\n".format(len(self.lines)))
    f.write("#Lines definition\n")
    f.write("#Position\tStart\tEnd\tVertical\n")
    for line in self.lines:
      f.write("{}\t{}\t{}\t{}\n".format(
        line['pos'],
        line['start'],
        line['end'],
        1 if line['vert'] else 0
      ))
    f.write("#Screens number\n")
    f.write("{}\n".format(len(self.screens)))
    f.write("#Screens definition\n")
    f.write("#Type\tLeft\tTop\tRight\tBottom\n")
    for screen in self.screens:
      f.write("{}\t{}\t{}\t{}\t{}\n".format(
        screen['type'],
        screen['left'],
        screen['top'],
        screen['right'],
        screen['bottom']
      ))
    f.write("#END")
    f.close()
  
  def load (self, path):
    f = open(path, "r")
    self.lines = []
    self.screens = []
    lines = [line for line in f.read().split("\n") if not line.startswith("#")]
    f.close()
    self.name = lines[0]
    lines = lines[1:]
    settings = lines[0].split("\t")
    self.bar = int(settings[0]) == 1
    self.bg = settings[1]
    self.line_width = int(settings[2])
    self.line_color = settings[3]
    self.sline_width = int(settings[4])
    self.sline_color = settings[5]
    lines = lines[1:]
    nlines = int(lines[0])
    lines = lines[1:]
    self.lines = [{} for x in range(nlines)]
    for line, linedef in zip(self.lines, lines):
      pos, s, e, v = linedef.split("\t")
      line['pos'] = float(pos)
      line['start'] = int(s)
      line['end'] = int(e)
      line['vert'] = (int(v) == 1)
    nscreens = int(lines[nlines])
    lines = lines[nlines+1:]
    self.screens = [{} for x in range(nscreens)]
    for screen, screendef in zip(self.screens, lines):
      ty, l, t, r, b = screendef.split("\t")
      screen['type'] = ty
      screen['left'] = int(l)
      screen['top'] = int(t)
      screen['right'] = int(r)
      screen['bottom'] = int(b)
  
  def applyToFrame (self, frame):
    for c in frame.winfo_children():
      c.destroy()
    
    SplitFrame.SplitScreen.SEPARATED_BAR = self.bar
    SplitFrame.SplitScreen.BG_COLOR = self.bg
    SplitFrame.RealLine.WIDTH = self.line_width
    SplitFrame.RealLine.LINE_COLOR = self.line_color
    SplitFrame.SplitScreen.SLINE_BD = self.sline_width
    SplitFrame.SplitScreen.SLINE_COLOR = self.sline_color
    
    lines = [
      frame.RealLine(
        frame,
        line['vert'],
        line['pos'],
        None
      )
      for line in self.lines
    ]
    for real, fake in zip(lines, self.lines):
      real.virtual = [[], []]
      if fake['start'] > -1 and fake['end'] > -1:
        real.base = [lines[fake['start']], lines[fake['end']]]
        real.reposition()
    for screen in self.screens:
      s = frame.SplitScreen(master=frame)
      s.setBoundary(
        0,
        frame.VirtualLine(
          real=lines[screen['left']],
          side=0
        )
      )
      s.setBoundary(
        1,
        frame.VirtualLine(
          real=lines[screen['top']],
          side=0
        )
      )
      s.setBoundary(
        2,
        frame.VirtualLine(
          real=lines[screen['right']],
          side=1
        )
      )
      s.setBoundary(
        3,
        frame.VirtualLine(
          real=lines[screen['bottom']],
          side=1
        )
      )
      for name, cls in s.selector.SCREENS.items():
        if cls.__name__ == screen['type']:
          s.selector.var.set(name)
    frame.resize()
  
  @staticmethod
  def findAvailable (path=None):
    if path is None:
      path = Profile.PROFILES_DIR
    files = [os.path.join(path, x) for x in os.listdir(path) if x.lower().endswith(".prf")]
    p = Profile()
    ret = {}
    for f in files:
      try:
        p.load(f)
      except:
        continue
      ret[str(p)] = f
    return ret
