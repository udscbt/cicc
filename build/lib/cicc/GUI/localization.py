import os
import warnings
import re

class Language:
  comments = re.compile(r'(?<!\\)#(.*?)\n')
  strings = {}
  
  def __init__ (self, path=None):
    if path is not None:
      self.update(path)
  
  def remove_comments (self, s):
    def sub(match):
      if match.group(1) == '':
        return ''
      else:
        return '\n'
    return self.comments.sub(sub, s)  
  
  def warn (self, real_file, real_line, msg, warn_type=Warning):
    warn_copy = warnings.showwarning
    def _showwarning (message, category, filename, lineno, file=None, line=None):
      return warn_copy(message, category, real_file, real_line, file, line)
    warnings.showwarning = _showwarning
    warnings.warn("Language: "+msg, warn_type)
    warnings.showwarning = warn_copy
  
  def update (self, path):
    with open(path, 'r') as f:
      content = f.read()
    
    content = self.remove_comments(content)
    
    for i, line in enumerate(content.split("\n")):
      if line == "":
        continue
      
      k,*v = line.split("=")
      if len(v) == 0: # no equal sign present
        self.warn(
          path, i,
          "Line with missing definition",
          SyntaxWarning
        )
        continue
      k = k.strip()
      if k == '':
        self.warn(
          path, i+1,
          "Definition with missing name",
          SyntaxWarning
        )
        continue
      v = "".join(v)
      v = v.strip()
      
      setattr(self, k, v)
      self.strings[k] = v

from tkinter import StringVar
class LanguageTkVar (StringVar):
  pass

class LanguageTk:
  comments = re.compile(r'(?<!\\)#(.*?)\n')
  strings = {}
  
  def __init__ (self, root, path=None):
    self.root = root
    if path is not None:
      self.update(path)
  
  def remove_comments (self, s):
    def sub(match):
      if match.group(1) == '':
        return ''
      else:
        return '\n'
    return self.comments.sub(sub, s)  
  
  def warn (self, real_file, real_line, msg, warn_type=Warning):
    warn_copy = warnings.showwarning
    def _showwarning (message, category, filename, lineno, file=None, line=None):
      return warn_copy(message, category, real_file, real_line, file, line)
    warnings.showwarning = _showwarning
    warnings.warn("Language: "+msg, warn_type)
    warnings.showwarning = warn_copy
  
  def update (self, path):
    with open(path, 'r') as f:
      content = f.read()
    
    content = self.remove_comments(content)
    
    for i, line in enumerate(content.split("\n")):
      if line == "":
        continue
      
      k,*v = line.split("=")
      if len(v) == 0: # no equal sign present
        self.warn(
          path, i,
          "Line with missing definition",
          SyntaxWarning
        )
        continue
      k = k.strip()
      if k == '':
        self.warn(
          path, i+1,
          "Definition with missing name",
          SyntaxWarning
        )
        continue
      v = "".join(v)
      v = v.strip()
      
      if k not in self.strings:
        self.strings[k] = LanguageTkVar(self.root)
      self.strings[k].set(v)
  
  def __getattr__ (self, k):
    if k in self.strings:
      return self.strings[k]
    else:
      return self.__getattribute__(k)
  
  def __getitem__ (self, k):
    return self.strings[k].get()

STRINGS = Language()

def load_lang_file (path):
  STRINGS.update(path)
