import sys
import os
import shutil
import argparse
import re
import importlib
import time
import traceback
import numpy as np
from numpy.random import permutation as permute
from numpy.random import shuffle
import cv2
import keras

#pylint: disable=C0103,C0111


class Dataset:

    def __init__(self):
        self.disk = ""
        self.basedir = []
        self.labels = None
        # Platform
        if sys.platform == 'win32':
            self.winzozz = True

    def getDisk(self, disk=None):
        if disk is None:
            return self.disk
        try:
            label, path = disk
            if sys.platform == 'win32':
                return label
            else:
                return path
        except (TypeError, ValueError):
            return disk

    def setDisk(self, *disk):
        if len(disk) == 1:
            disk = disk[0]
        self.disk = self.getDisk(disk)

    @staticmethod
    def splitPath(path):
        ret = []
        if isinstance(path, list):
            for p in path:
                ret += Dataset.splitPath(p)
        else:
            ret += re.split(r'\\|/', path)
        return ret

    def setBaseDir(self, basedir):
        self.basedir = self.splitPath(basedir)

    def getPath(self, relative=None):
        if relative is None:
            return os.path.join(self.disk, *self.basedir)
        path = os.path.join(*self.splitPath(relative))
        if os.path.abspath(path) == path: # absolute path
            return path
        return os.path.join(self.getPath(), path)

    def setLabels(self, labels):
        self.labels = labels

    def getLabels(self):
        return self.labels

    def getSubdataset(self,
                      folder,
                      dataset_class=None,
                      cls_args=(),
                      cls_kwargs={}):
        if dataset_class is None:
            ds = Dataset()
        else:
            ds = dataset_class(*cls_args, **cls_kwargs)

        ds.setDisk(self.getDisk())
        ds.setBaseDir([*self.basedir, folder])
        ds.setLabels(self.getLabels())
        return ds


class DatasetGenerator(Dataset):

    def __init__(self):
        Dataset.__init__(self)

        # Arg parser
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument('-D', '--debug', action="store_true")
        self.parser.add_argument('--clean', action="store_true")
        self.parser.add_argument('-R', '--resume')
        self.parser.add_argument('-S', '--skip-one', action="store_true")
        self.parser.add_argument('--verbose', action="store_true")

        self.destination = ""

    def print(self, *args, **kwargs):
        if self.debug or self.verbose:
            print(*args, **kwargs)

    def parseArgs(self):
        args = self.parser.parse_args()
        self.debug = args.debug
        self.clean = args.clean
        self.resume = args.resume
        self.skip = args.skip_one
        self.verbose = args.verbose
        return args

    def setDestination(self, path, newdisk=None):
        self.destination = os.path.join(
            self.getDisk(newdisk), *self.splitPath(path))

    def setDatasets(self, datasets):
        self.datasets = []
        self.weights = []
        for ds in datasets:
            if isinstance(ds, str):
                self.datasets.append(ds)
                self.weights.append(1)
            elif isinstance(ds, tuple) and len(ds) == 2:
                ds, w = ds
                self.datasets.append(ds)
                self.weights.append(w)
            else:
                raise Exception("Wrong datasets specification")
        self.weights = np.array(self.weights)
        self.weights = self.weights / self.weights.sum()

    def setupDirtree(self):
        if not self.debug:
            # Directory tree setup
            for dataset in self.datasets:
                absdataset = os.path.join(self.destination, dataset)
                if os.path.exists(absdataset):
                    if self.clean:
                        shutil.rmtree(absdataset)
                        while os.path.exists(absdataset):
                            pass
                if not os.path.exists(absdataset):
                    os.mkdir(absdataset)
                for emotion in self.labels:
                    if not os.path.exists(os.path.join(absdataset, emotion)):
                        os.mkdir(os.path.join(absdataset, emotion))

    class State:
        defaults = {
            'filename': "",
            "outfile": "",
            "idx": 0,
            "indices": None,
            "count": 0,
            "total": 0,
            "ds": 0,
            "skipped": None
        }

        def __init__(self):
            for k, v in self.defaults.items():
                setattr(self, k, v if v is not None else [])

        def load(self, logfile):
            module = importlib.import_module(logfile)
            for k, v in module.__dict__.items():
                if k.startswith('_'):
                    continue
                setattr(self, k, v if v is not None else [])

        def save(self, logfile, exc=None):
            f = open(logfile + ".py", "w")

            if exc is not None:
                f.write("#{}\n".format("\n#".join(exc.split("\n"))))

            for k in self.defaults:
                f.write("{} = {}\n".format(k, repr(getattr(self, k))))

            f.close()

    def generate(self, filelist, normalization=None, randomize=False):
        if self.debug:
            print(r"""
/----------------------\
| Debug mode activated |
\----------------------/
""")

        state = self.State()

        state.total = len(filelist)
        if randomize:
            state.indices = np.random.permutation(state.total)
        else:
            state.indices = np.arange(state.total)
        state.indices = list(state.indices)

        if self.resume is not None:
            state.load(self.resume)
            state.indices = state.indices[state.indices.index(state.idx):]

        weights = self.weights * state.total

        for idx in state.indices:
            state.idx = idx

            if self.skip:
                state.skipped.append(state.filename)
                self.skip = False
                continue
            try:
                state.count += 1

                sample = filelist[state.idx]
                state.filename = os.path.join(self.disk, *self.basedir,
                                              sample['filename'])
                emotion = self.labels[sample['label']]

                state.outfile = os.path.join(self.destination,
                                             self.datasets[state.ds], emotion,
                                             os.path.basename(state.filename))

                self.print(
                    "\r[{}/{}] Creating {}".format(state.count, state.total,
                                                   state.outfile),
                    end="")

                img = cv2.imread(state.filename)

                if self.debug:
                    cv2.imshow('Original', img)

                if normalization is not None:
                    ret, img = normalization(img, sample)
                    if not ret:
                        state.count -= 1
                        state.total -= 1
                        self.print("\r[{}/{}] Skipping {}".format(
                            state.count, state.total, state.filename))
                        state.skipped.append(state.filename)
                        continue

                if self.debug:
                    cv2.imshow('Normalized', img)
                    while cv2.waitKey(1) & 0xFF != ord('q'):
                        pass
                else:
                    cv2.imwrite(state.outfile, img)
            except:
                exc = traceback.format_exc()
                print(exc)
                logfile = "{}_log".format(str(time.time()).replace('.', '_'))
                state.save(logfile, exc)
                break

        if self.debug:
            cv2.destroyAllWindows()


class BatchGenerator(Dataset):

    def __init__(self, verbose=False):
        Dataset.__init__(self)
        self._classes = None
        self.used = None
        self.shape = (1, 1, 1)
        self.batch_size = 1
        self.verbose = verbose
        self.steps = None
        self.numsamples = None
        self.weights = None
        self.use_weights = False
        self.clear()
    
    def setWeightFunction(self, fn):
        self.weights = fn
    
    def getWeightFunction(self):
        return self.weights

    def print(self, *args, **kwargs):
        if self.verbose:
            print(*args, **kwargs)

    def reset(self):
        pass
    
    def clear (self):
        self._classes = None
        self.filelist = {}
        self.totalsamples = 0
        self.samples = {}

    def setInputShape(self, shape):
        self.shape = shape

    def getInputShape(self):
        return self.shape

    def setBatchSize(self, batch):
        self.batch_size = batch

    def getBatchSize(self):
        return self.batch_size

    def setLabels(self, labels):
        self.labels = labels
        self._classes = labels

    def getLabels(self):
        return self._classes

    def getTarget(self, label):
        return keras.utils.to_categorical(
            self.getClassIndex(label), self.getOutputSize())

    def setUsedLabels(self, labels):
        self.used = labels

    def getUsedLabels(self):
        if self.used is None:
            return self.getLabels()
        else:
            return self.used

    def getOutputSize(self):
        return len(self.getLabels())

    def initialize(self):
        self.print("Initializing generator")
        self.load_dir()
        self.reset()

    BREAK = 'break'
    RESET = 'reset'

    def load_dir(self, progress_callback=None):
        self.clear()
        
        self.print("Loading file list")
        if self.labels is None:
            classes = []
            for f in os.listdir(self.getPath()):
                if os.path.isdir(self.getPath(f)):
                    classes.append(f)
        else:
            classes = self.labels
        self._classes = classes
        self.print("{} class(es) found".format(len(classes)))

        for c, cls in enumerate(self.getUsedLabels()):
            imgs = []
            files = os.listdir(self.getPath(cls))
            for i, img in enumerate(files):
                imgs.append(self.getPath([cls, img]))
                if progress_callback is not None:
                    ret = progress_callback(
                        self,
                        c, cls, len(self.getUsedLabels()),
                        i, img, len(files)
                    )
                    if ret == self.BREAK:
                        return
                    elif ret == self.RESET:
                        self.clear()
                        return
            if len(imgs) == 0:
                raise Exception("No image found for class {}.\n".format(cls) +
                                "Could not create generator.")
            if self.weights is not None:
                self.filelist[cls] = [
                    (img, self.weights(self, img, cls))
                    for img in imgs
                ]
            else:
                self.filelist[cls] = [
                    (img, 1)
                    for img in imgs
                ]
            self.samples[cls] = len(imgs)
            self.totalsamples += self.samples[cls]
            self.print("[{}] {} image(s)".format(cls, len(imgs)))

        self.print("Total: {} image(s)".format(self.totalsamples))

    def getClassName(self, idx):
        return self.getLabels()[idx]

    def getClassIndex(self, cls):
        return self.getLabels().index(cls)

    def getNumSamples(self, cls=None):
        if cls is None:
            return self.totalsamples
        elif isinstance(cls, int):
            return self.getNumSamples(self.getClassName(cls))
        else:
            return self.samples.get(cls, 0)

    def setSteps(self, steps):
        self.steps = steps
    
    def setSamples(self, samples):
        self.numsamples = samples
    
    def load_image (self, path):
        img = cv2.imread(
            path, cv2.IMREAD_GRAYSCALE
            if self.shape[2] == 1 else cv2.IMREAD_COLOR)
        if self.shape[:2] != img.shape:
            img = cv2.resize(img, (self.shape[1], self.shape[0]))
        img = np.atleast_3d(img)
        return img
    
    def iter_filenames (self):
        if len(self) is None:
            while True:
                try:
                    yield self.next_filename()
                except StopIteration:
                    break
        else:
            for i in range(len(self)):
                try:
                    yield self.next_filename()
                except StopIteration:
                    break
    
    def __iter__(self):
        for batch in self.iter_filenames():
            yield self.__next__(batch)

    def next_filename (self):
        raise NotImplementedError

    def __next__(self, filename_batch=None):
        if filename_batch is None:
            filename_batch = self.next_filename()
        imgs = np.array([self.load_image(path) for path in filename_batch[0]])
        return (imgs, *filename_batch[1:])

    def __len__(self):
        if self.steps is not None:
            return self.steps
        if self.numsamples is not None:
            return int(np.ceil(self.numsamples / self.getBatchSize()))
        return None


class WellSampledGenerator(BatchGenerator):

    def __init__(self, verbose=False):
        BatchGenerator.__init__(self, verbose)

    def randomize(self, cls=None):
        if cls is None:
            BatchGenerator.reset(self)
            self.print("Randomizing samples order")
            self.orders = {}
            for cls in self.getUsedLabels():
                self.randomize(cls)
        else:
            self.orders[cls] = iter(permute(self.getNumSamples(cls)))

    def reset(self):
        BatchGenerator.reset(self)
        self.print("Reordering samples order")
        self.orders = {}
        for cls in self.getUsedLabels():
            self.orders[cls] = iter(range(self.getNumSamples(cls)))

    def next_filename (self):
        batch_in = []
        batch_out = []
        batch_weights = []
        class_order = self.getUsedLabels().copy()
        shuffle(class_order)
        idx = 0
        while idx < self.batch_size:
            for cls in class_order:
                try:
                    index = next(self.orders[cls])
                except StopIteration:
                    self.randomize(cls)
                    index = next(self.orders[cls])
                f, w = self.filelist[cls][index]
                batch_in.append(f)
                batch_out.append(self.getTarget(cls))
                batch_weights.append(w)
                idx += 1
                if idx >= self.batch_size:
                    break
        batch_in = np.array(batch_in)
        batch_out = np.array(batch_out)
        batch_weights = np.array(batch_weights)
        if self.use_weights:
            return (batch_in, batch_out, batch_weights)
        else:
            return (batch_in, batch_out)


class FullDatasetGenerator(BatchGenerator, keras.utils.Sequence):

    def __init__(self, verbose=False):
        BatchGenerator.__init__(self, verbose)

    def reset(self):
        BatchGenerator.reset(self)
        self.print("Reordering samples order")
        self._idx = 0
        self.indices = [(cls, idx)
                        for cls in self.getUsedLabels()
                        for idx in range(self.getNumSamples(cls))]

    def randomize(self):
        self.reset()
        self.print("Randomizing samples order")
        shuffle(self.indices)

    def __len__(self):
        fixed_len = BatchGenerator.__len__(self)
        if fixed_len is None:
            return int(np.ceil(self.getNumSamples() / self.getBatchSize()))
        else:
            return fixed_len

    def next_filename(self):
        try:
            self._idx, batch = self.get_filename_from(self._idx)
        except IndexError:
            raise StopIteration

        return batch

    def get_filename_from(self, idx):
        numsamples = self.getNumSamples()
        if self.numsamples is not None:
            numsamples = min(numsamples, self.numsamples)
        
        if idx >= numsamples:
            raise IndexError(
                "Trying to access image {}".format(idx) +
                " but only {} are available".format(self.getNumSamples()))

        batch_in = []
        batch_out = []
        batch_weights = []
        for i in range(self.batch_size):
            cls, index = self.indices[idx]
            f,w = self.filelist[cls][index]
            batch_in.append(f)
            batch_out.append(self.getTarget(cls))
            batch_weights.append(w)
            idx += 1
            if idx >= numsamples:
                break
        batch_in = np.array(batch_in)
        batch_out = np.array(batch_out)
        batch_weights = np.array(batch_weights)
        if self.use_weights:
            return (idx, (batch_in, batch_out, batch_weights))
        else:
            return (idx, (batch_in, batch_out))
    
    def get_from(self, idx):
        idx, batch = self.get_filename_from(idx)
        return idx, self.__next__(batch)

    def __getitem__(self, index):
        _, batch = self.get_from(index * self.batch_size)
        return batch
