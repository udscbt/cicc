import tkinter as tk

class SavedAttribute:
  def __init__ (self, default=None):
    self.saved = default
    self.values = {}
  
  def __get__ (self, obj, objtype=None):
    if obj is None:
      return self.saved
    elif obj not in self.values:
      try:
        saved = self.saved.copy()
      except:
        saved = self.saved
      self.__set__(obj, saved)
    return self.values[obj]
  
  def __set__ (self, obj, value):
    self.values[obj] = value
    self.saved = value

class SharedAttribute:
  def __init__ (self, default=None):
    self.value = default
  
  def __get__ (self, obj, objtype=None):
    return self.value
  
  def __set__ (self, obj, value):
    self.value = value

class CollapsibleList (tk.Frame):
  BTN_WIDTH = 20
  LST_WIDTH = 200
  
  def __init__ (self, master, *args, **kwargs):
    tk.Frame.__init__(self, master)
    self.btn = tk.Button(self, text=">", command=self.expandcollapse)
    self.lst = tk.Listbox(self, *args, **kwargs)
    
    self.expanded = False
    
    self.btn.place(anchor="nw", x=0, y=0, width=self.BTN_WIDTH, relheight=1)
    self.expandcollapse()
  
  def expandcollapse (self, e=None):
    self.expanded = not self.expanded
    if self.expanded:
      self.btn['text'] = '>'
      self.lst.place(anchor="nw", x=self.BTN_WIDTH, y=0, width=self.LST_WIDTH, relheight=1)
      self['width'] = self.BTN_WIDTH+self.LST_WIDTH
    else:
      self.btn['text'] = '<'
      self.lst.place(anchor="nw", x=self.BTN_WIDTH, y=0, width=0, relheight=1)
      self['width'] = self.BTN_WIDTH
  
  def listMethod (f):
    def decorated (self, *args, **kwargs):
      return getattr(self.lst, f.__name__)(*args, **kwargs)
    decorated.__name__ = f.__name__
    return decorated
  
  @listMethod
  def insert ():
    pass
  
  @listMethod
  def delete ():
    pass
  
  @listMethod
  def select_set ():
    pass
  
  @listMethod
  def see ():
    pass
  
  @listMethod
  def nearest ():
    pass
  
  @listMethod
  def get ():
    pass
  
  @listMethod
  def size ():
    pass
  
  @listMethod
  def curselection ():
    pass
  
  @listMethod
  def bind ():
    pass
