from .. import Layer, Value, add_unique_name_generator, available_layers

from keras.layers import Concatenate as layer

class ToBeMerged (Value):
  def __init__ (self, layer, input):
    if layer._lastoutput is None:
      layer._lastoutput = Merged(layer)
    
    Value.__init__(
      self,
      layer,
      input,
      "{}_output{}".format(layer.name,len(layer._lastoutput.inputs))
    )
    layer._lastoutput.inputs.append(self)
    self.outputs = [layer._lastoutput]
    layer.updateLabel()

class Merged (Value):
  def __init__ (self, layer):
    Value.__init__(
      self,
      layer
    )
    self.inputs = []
  
  def tokeras (self):
    self.keras = self.layer.keras([
      v.input.tokeras()
      for v in self.inputs
    ])
    return self.keras

@add_unique_name_generator('concat_')
class Concatenate (Layer):
  CLASSNAME = 'concat'
  def __init__ (self, *args, **kwargs):
    Layer.__init__(self, *args, **kwargs)
  
  def updateLabel (self):
    label = [self.escapeLabel(self.get_label())]
    if self._lastoutput is not None:
      inputs = []
      for out in self._lastoutput.inputs:
        inputs.append("<{}> {}".format(out.nodename, out.input.name))
      inputs = " | ".join(inputs)
      
      label.append(
        "{{ {{ {} }} | <{}> {} }}".format(
          inputs,
          self._lastoutput.nodename,
          self._lastoutput.name
        )
      )
    self.node.set_label(" | ".join(label))
  
  @property
  def outputs (self):
    if self._lastoutput is None:
      return []
    else:
      return [self._lastoutput]
  
  @outputs.setter
  def outputs (self, value):
    pass
  
  def connect (self, input):
    ToBeMerged(self, input)
    return self._lastoutput
  
  def updateROI (self):
    Concatenate._outputs = Concatenate.outputs
    Concatenate.outputs = self._lastoutput.inputs+self._outputs
    Layer.updateROI(self)
    Concatenate.outputs = Concatenate._outputs
    del Concatenate._outputs
  

available_layers[layer] = Concatenate
