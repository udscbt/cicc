import sys
import os
sys.path.append(
  os.path.join(
    os.path.dirname(__file__),
    os.path.pardir,
    os.path.pardir
  )
)

from tkinter import *
from cicc.GUI.splitscreen import (
  SplitFrame,
  SelectionBar,
  SharedAttribute,
  SavedAttribute,
  Profile
)
from cicc.GUI.splitscreen.voidscreen import VoidScreen
from cicc.GUI.layout import GridLayout

root = Tk()

class ExampleScreen (Frame):
  IDX = 0
  counter = 0
  def __init__ (self, master):
    super().__init__(master)
    
    self.idx = self.IDX + 1
    ExampleScreen.IDX = self.idx
    
    layout = GridLayout("""
      "This is a static label" |
      -------------------------+ pnlNamed{This is a named panel}
            pnlUnnamed{}       |
                               |
    """, row_weights=[1, 3], column_weights=[3,2])
    layout.apply(self)
    
    layout2 = GridLayout("""
      lblShouldBeDefinedElsewhere
      ---------------------------
      btnShouldBeDefinedElsewhere
    """)
    
    self.pnlUnnamed.lblShouldBeDefinedElsewhere = Label(self)
    self.pnlUnnamed.btnShouldBeDefinedElsewhere = Button(self, text="CLICCAMI", command=self.click)
    layout2.apply(self.pnlUnnamed)
    self.showlbl()
    
  def showlbl (self):
      self.pnlUnnamed.lblShouldBeDefinedElsewhere['text'] = "Index: {}, Counter: {}".format(self.idx, self.counter)
  
  def click (self):
    self.counter += 1
    self.showlbl()


class ExampleScreen2 (ExampleScreen):
  counter = SavedAttribute(0)
  
  def __init__ (self, master):
    super().__init__(master)

class ExampleScreen3 (ExampleScreen):
  counter = SharedAttribute(0)
  
  def __init__ (self, master):
    super().__init__(master)
  
  def showlbl(self):
    super().showlbl()
    self.after(10, self.showlbl)
    
SelectionBar.addScreen("Empty", VoidScreen)                   # Add screen
SelectionBar.addScreen("Example", ExampleScreen)              # types to
SelectionBar.addScreen("Example with memory", ExampleScreen2) # selection bar
SelectionBar.addScreen("Example with sharing", ExampleScreen3)

main = SplitFrame(root)
main.pack(fill=BOTH, expand=True)

# Default layout
p = Profile()
Profile.DEFAULT_PATH = os.path.join(
  os.path.dirname(__file__),
  "profiles",
  "default.prf"
)
# ~ try:
p.load(Profile.DEFAULT_PATH)
p.applyToFrame(main)
# ~ except:
  # ~ pass

root.mainloop()
