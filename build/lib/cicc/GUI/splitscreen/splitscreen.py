import tkinter as tk
from . import STRINGS
from .selectionbar import *

GLOBAL_DEBUG = False

def _debug_cb(callback):
  def _callback(s, e, *args, **kwargs):
    try:
      w = e.widget
      t = e.type
    except:
      w = None
      t = None
    if GLOBAL_DEBUG:
      print(w, t, callback.__name__)
      print(e)
      print(args)
      print(kwargs)
    callback(s, e, *args, **kwargs)
  return _callback

if GLOBAL_DEBUG:
  debug_cb = _debug_cb
else:
  debug_cb = lambda x: x

class SplitFrame (tk.Frame):
  # Common functions decorator
  def for_all_screens(func):
    def _func(self, *args, **kwargs):
      for s in self.screens:
        s.__getattribute__(func.__name__)(*args, **kwargs)
      func(self, *args, **kwargs)
    return _func

  BG_COLOR = 'white' # background color of frame (normally covered)
  # Constructor
  # content : class of screen to be shown
  def __init__(self, *args, **kwargs):
    tk.Frame.__init__(self, *args, **kwargs) # super constructor
    self['bg'] = self.BG_COLOR
    # Add border lines
    self.lines = []
    self.RealLine(self, True, 0, None),  # Left
    self.RealLine(self, False, 0, None), # Top
    self.RealLine(self, True, 1, None),  # Right
    self.RealLine(self, False, 1, None)  # Bottom
    # Create main screen
    self.screens = []
    self.SplitScreen(master=self)
    # Place main screen between border lines
    for i in range(4):
      self.screens[0].setBoundary(i, self.lines[i].virtual[int(i/2)][0])
    # Update positions
    self.resize()
  
  @for_all_screens
  def appearanceChanged (self):
    for l in self.lines:
      l.appearanceChanged()
  
  # Enable splitting globally
  # vertical : direction of splitting
  @for_all_screens
  def startSplit(self, vertical):
    pass
  
  # Disable splitting globally
  @for_all_screens
  def stopSplit(self):
    pass
  
  # Disable joining globally
  @for_all_screens
  def stopJoin(self):
    pass
  
  # Resize after changes
  @for_all_screens
  def resize(self):
    # Reposition split lines and move them on top
    for l in self.lines:
      l.reposition()
      l.lift()

  # Internal splittable screen
  class SplitScreen (tk.Frame):
    SLINE_BD = 2         # border width of temporary splitline
    SLINE_COLOR = 'gray' # color of temporary splitline
    BG_COLOR = '#222'    # background color of screen (normally covered)
    
    SEPARATED_BAR = False
    
    # Constructor
    def __init__(self, *args, **kwargs):
      tk.Frame.__init__(self, *args, **kwargs) # super constructor
      self.master.screens.append(self) # add to master frame
      # Temporary splitline
      self.splitLine = tk.Frame(self)
      self.splitLine.horizontal = False # default
      self.splitLine.bindtags(
        (self,) +
        self.splitLine.bindtags()
      ) # use event handlers of SplitScreen
      # Selection bar
      self.selector = SelectionBar(self)
      #~ self.rowconfigure(1, weight=1)
      self.rowconfigure(2, weight=1)
      self.columnconfigure(1, weight=1)
      # Content of screen
      self['bg'] = self.BG_COLOR
      self.content = None
      self.selector.changeScreen()
      # Boundary virtual lines (to be defined)
      self.boundary = [None for i in range(4)]
      
      self.maximized = False
      
      self.appearanceChanged()
    
    def appearanceChanged (self):
      self['bg'] = self.BG_COLOR
      self.splitLine['bg'] = self.SLINE_COLOR
      self.selector.place_forget()
      self.content.place_forget()
      self.selector.grid_forget()
      self.content.grid_forget()
      if self.SEPARATED_BAR:
        self.selector.grid(row=1, column=1, sticky="news")
        self.content.grid(row=2, column=1, sticky="news")
      else:
        self.selector.place(anchor="nw", x=0, y=0)
        self.content.place(x=0, relwidth=1, y=0, relheight=1)
    
    # Set/change boundary virtual line on one side
    def setBoundary(self, index, line):
      # Destroy old boundary
      if self.boundary[index] is not None:
        self.boundary[index].destroy()
      # Remove line from old screen
      if line.screen is not None:
        for i, v in enumerate(line.screen.boundary):
          if v == line:
            line.screen.boundary[i] = None
            break
      # Set this as screen associated to the line
      line.screen = self
      # Set line as boundary
      self.boundary[index] = line
    
    # Bind events of SplitScreen to all descendants
    def bindFamily(self, child):
      try:
        child.bindtags((self,)+child.bindtags())
      except:
        pass
      try:
        children = child.children.values()
      except:
        children = []
      for c in children:
        self.bindFamily(c)
    
    # Set content of screen
    # content : class of screen
    def setContent(self, content):
      if self.content is not None:
        self.content.destroy()
      if content is not None:
        self.content = content(self)
        if self.SEPARATED_BAR:
          self.content.grid(row=2, column=1, sticky="news")
        else:
          self.content.place(x=0, relwidth=1, y=0, relheight=1)
        self.bindFamily(self.content)
      else:
        self.content = None
      self.selector.lift() # put selector on top
    
    # Enable splitting
    def startSplit(self, vertical):      
      # Show splitline
      self.splitLine.lift()
      # Position splitline
      if vertical:
        self.splitLine.horizontal = True
        self.splitLine.place(
          anchor=tk.CENTER,
          relx=0, y=0,
          relwidth=2, relheight=0,
          height=self.SLINE_BD
        )
      else:
        self.splitLine.horizontal = False
        self.splitLine.place(
          anchor=tk.CENTER,
          x=0, rely=0,
          relwidth=0, relheight=2,
          width=self.SLINE_BD
        )
      # Event bindings:
      # Move splitline during splitting
      self.bind_class(self, "<Motion>", self.onMove)
      # Cancel splitting on right click
      self.bind_class(self, "<3>", self.onRightClick)
      # Change split orientation on middle click
      self.bind_class(self, "<2>", self.onMiddleClick_split)
      # Perform split on left click
      self.bind_class(self, "<1>", self.onLeftClick_split)
      # Show splitline on mouse enter
      self.bind("<Enter>", self.onMove)
      # Hide splitline on mouse leave
      self.bind("<Leave>", self.onLeave)
    
    # Move splitline during splitting
    @debug_cb
    def onMove(self, e):
      # Reposition splitline
      if (self.splitLine.horizontal):
        pos = (e.y_root-self.winfo_rooty())
        self.splitLine.place(y=pos)
      else:
        pos = (e.x_root-self.winfo_rootx())
        self.splitLine.place(x=pos)
      # Stop event propagation
      return "break"
    
    # Perform split on left click
    @debug_cb
    def onLeftClick_split(self, e):
      self.master.stopSplit() # stop splitting on all screens
      if self.splitLine.horizontal:
        ver = True
        # position
        pos = (e.y_root-self.master.winfo_rooty())/self.master.winfo_height()
      else:
        ver = False
        # position
        pos = (e.x_root-self.master.winfo_rootx())/self.master.winfo_width()
      self.split(ver, pos)
      # Stop event propagation
      return "break"
    
    def split (self, horizontal, position):
      # Create new screen
      screen = SplitFrame.SplitScreen(self.master)
      # Create boundaries for new screen updating old ones:
      # Set parameters dependent on horizontal or vertical split
      if horizontal:
        i = 3 # splitline side
        j = 1 # opposite side
        # adjacent sides
        h = 0
        k = 2
        # position
        pos = position
      else:
        i = 2 # splitline side
        j = 0 # opposite side
        # adjacent sides
        h = 1
        k = 3
        # position
        pos = position
      # Create new real line
      line = SplitFrame.RealLine(
        self.master,
        not horizontal,
        pos,
        [self.boundary[h].real, self.boundary[k].real]
      )
      # The new screen takes old screen's splitline-side border
      screen.setBoundary(i, self.boundary[i])
      # The two sides of the new real line are
      # opposite borders for the two screens
      self.setBoundary(i, line.virtual[0][0])
      screen.setBoundary(j, line.virtual[1][0])
      # Generate new virtual lines for adjacent sides of new screen
      screen.setBoundary(
        h,
        SplitFrame.VirtualLine(
          real=self.boundary[h].real,
          side=self.boundary[h].side
        )
      )
      screen.setBoundary(
        k,
        SplitFrame.VirtualLine(
          real=self.boundary[k].real,
          side=self.boundary[k].side
        )
      )
      # Update positions
      self.master.resize()
      # Return two screens
      return self, screen
    
    @debug_cb
    def onMiddleClick_split(self, e):
      self.master.stopSplit() # stop splitting on all screens
      self.master.startSplit(not self.splitLine.horizontal) # restart splitting with different orientation
      # Stop event propagation
      return "break"
    
    # Enable joining
    def startJoin(self, line, screen):
      # Change cursor
      if line.vertical:
        if self.winfo_x() < screen.winfo_x():
          self.config(cursor="left_side")
        else:
          self.config(cursor="right_side")
      else:
        if self.winfo_y() < screen.winfo_y():
          self.config(cursor="top_side")
        else:
          self.config(cursor="bottom_side")
      # Event bindings:
      # Cancel joining on right click
      self.bind_class(self, "<3>", self.onRightClick)
      # Perform join on left click
      self.bind_class(self, "<1>", lambda e: self.onLeftClick_join(e, line))
    
    # Join this screen to screen opposite to the selected line
    def join(self, line):
      # Find corresponding boundary
      for i,v in enumerate(self.boundary):
        if v.real == line:
          # Delete this screen by collapsing the line
          self.remove(i)
          break
    
    # Swap position of this screen with another screen
    def swap(self, screen):
      # Swap positions by swapping boundaries
      v = self.boundary
      self.boundary = screen.boundary
      screen.boundary = v
      # Update positions
      self.master.resize()
    
    # Perform join on left click
    @debug_cb
    def onLeftClick_join(self, e, line):
      self.master.stopJoin() # stop joining on all screens
      self.join(line) # join screens
      # Stop event propagation
      return "break"
    
    # Cancel splitting and joining on right click
    @debug_cb
    def onRightClick(self, e):
      self.master.stopJoin() # stop joining on all screens
      self.master.stopSplit() # stop splitting on all screens
      # Stop event propagation
      return "break"
    
    # Stop splitting
    def stopSplit(self):
      self.onLeave(None) # hide splitline
      # Disable event handlers
      self.unbind_class(self, "<Motion>")
      self.unbind_class(self, "<3>")
      self.unbind_class(self, "<1>")
      self.unbind_class(self, "<Enter>")
      self.unbind("<Leave>")
    
    # Stop joining
    def stopJoin(self):
      self.config(cursor="arrow") # reset cursor
      # Disable event handlers
      self.unbind_class(self, "<3>")
      self.unbind_class(self, "<1>")
    
    # Hide splitline on mouse leave
    @debug_cb
    def onLeave(self, e):
      if (self.splitLine.horizontal):
        self.splitLine.place(y=-10)
      else:
        self.splitLine.place(x=-10)
    
    # Update position and size of screen by using its boundaries
    def resize(self):
      if self.maximized:
        self.place(relx=0, rely=0, relwidth=1, relheight=1)
      else:
        try:
          x = self.boundary[0].real.position
          y = self.boundary[1].real.position
          w = self.boundary[2].real.position - x
          if w <= 0:
            self.boundary[0].real.position = self.boundary[0].real.oldpos
            self.boundary[2].real.position = self.boundary[2].real.oldpos
            self.master.resize()
            return
          h = self.boundary[3].real.position - y
          if h <= 0:
            self.boundary[1].real.position = self.boundary[1].real.oldpos
            self.boundary[3].real.position = self.boundary[3].real.oldpos
            self.master.resize()
            return
          self.place(relx=x, rely=y, relwidth=w, relheight=h)
        except:
          pass
    
    # Destroy screen
    def destroy(self):
      self.master.screens.remove(self) # remove from parent frame
      for v in self.boundary:
        if v is not None:
          v.destroy() # destroy boundaries
      # Destroy widget (after a little time to avoid errors)
      self.after(100, lambda: tk.Frame.destroy(self))
    
    # Close screen and enlarge the adjacent ones
    # boundary : side to use for collapsing
    def remove(self, boundary=None):
      if boundary == None: # find suitable side
        for i, v in enumerate(self.boundary):
          # A suitable side must fully occupy a real line
          if v.real.base is not None and len(v.real.virtual[v.side]) == 1:
            return self.remove(i)
        return False
      
      # Define sides
      i = boundary # side used for collapsing
      j = (i+2)%4  # opposite side
      # adjacent sides
      h = (i+1)%4
      k = (i+3)%4
      # Virtual line used for collapsing
      v = self.boundary[i]
      # Real line used for collapsing
      line = v.real
      # Move virtual lines on other side to opposite real line
      for virtual in line.virtual[1-v.side]:
        virtual.real = self.boundary[j].real
        self.boundary[j].real.virtual[self.boundary[j].side].append(virtual)
      # Remove virtual lines from old real line
      line.virtual[1-v.side] = []
      # Update extremes of real lines intersecting the old one
      for l in self.master.lines:
        if l.base is not None:
          if l.base[0] == line:
            l.base[0] = self.boundary[j].real
          if l.base[1] == line:
            l.base[1] = self.boundary[j].real
      # Destroy old line
      line.destroy()
      # Update positions
      self.master.resize()
      # Destroy screen
      self.destroy()
      return True
  
  class RealLine (tk.Frame):
    WIDTH = 6            # line width
    LINE_COLOR = 'black' # line color
    # Constructor
    # parent : parent SplitFrame
    # vertical : whether the line is vertical or not
    # position : position relative to full size of frame
    # base : extremes of the line (None if it's part of the fram borders)
    def __init__(self, parent, vertical, position, base=None):
      parent.lines.append(self) # add to parent
      # Create virtual lines on both sides
      # (only one side if the line is a border)
      self.virtual = [[], []]
      SplitFrame.VirtualLine(real=self, side=int(position))
      if base is not None:
        SplitFrame.VirtualLine(real=self, side=1-int(position))
      
      tk.Frame.__init__(self, parent)
      self.vertical = vertical
      self.base = base
      self.oldpos = self.position = position
      # Place line in the correct position
      self.reposition()
      # Event bindings:
      # Move line by dragging
      self.bind("<B1-Motion>", self.onDrag)
      # Open menu on right click
      self.bind("<Button-3>", self.onClick)
      
      self.appearanceChanged()
    
    def appearanceChanged (self):
      self['bg'] = self.LINE_COLOR
      self.reposition()
    
    # Update position of line
    def reposition(self):
      self.oldpos = self.position
      # Extreme points
      if self.base is not None:
        start  = self.base[0].position
        end    = self.base[1].position
        if start > end:
          tmp = start
          start = end
          end = tmp
        length = end - start
      else:
        start = 0
        length = 1
      # Position line
      if self.vertical:
        self.place(
          anchor=tk.N,
          relx=self.position, width=self.WIDTH,
          rely=start, relheight=length
        )
      else:
        self.place(
          anchor=tk.W,
          relx=start, relwidth=length,
          rely=self.position, height=self.WIDTH
        )
      # Set cursor for movable or unmovable lines
      if self.vertical:
        if self.base is not None:
          self.config(cursor="sb_h_double_arrow")
        elif self.position > 0.5:
          self.config(cursor="right_tee")
        else:
          self.config(cursor="left_tee")
      else:
        if self.base is not None:
          self.config(cursor="sb_v_double_arrow")
        elif self.position > 0.5:
          self.config(cursor="bottom_tee")
        else:
          self.config(cursor="top_tee")
    
    # Move line by dragging
    @debug_cb
    def onDrag(self, e):
      if self.base is None:
        for i in range(4):
          if self == self.master.lines[i]:
            break
        j = (i+2)%4
        h = (i+1)%4
        k = (i+3)%4
        screen = SplitFrame.SplitScreen(self.master)
        line = SplitFrame.RealLine(self.master, self.vertical, int(i/2), None)
        self.master.lines[-1] = self
        self.master.lines[i] = line
        screen.setBoundary(i, line.virtual[int(i/2)][0])
        screen.setBoundary(j, SplitFrame.VirtualLine(self, int(j/2)))
        screen.setBoundary(h, SplitFrame.VirtualLine(self.master.lines[h], int(h/2)))
        screen.setBoundary(k, SplitFrame.VirtualLine(self.master.lines[k], int(k/2)))
        self.base = [self.master.lines[h], self.master.lines[k]]
      # Set line position to mouse position
      if self.vertical:
        self.position = (
          e.x_root - self.master.winfo_rootx()
        ) / self.master.winfo_width()
      else:
        self.position = (
          e.y_root - self.master.winfo_rooty()
        ) / self.master.winfo_height()
      # Avoid going outside borders
      if self.position > 1:
        self.position = 1
      elif self.position < 0:
        self.position = 0
      # Update position
      self.master.resize()
    
    # Whether the adjacent screens can be joined together
    def joinable(self):
      return (
        self.base is not None and
        len(self.virtual[0]) == 1 and
        len(self.virtual[1]) == 1
      )
    
    # Open menu on right click
    @debug_cb
    def onClick(self, e):
      self.master.stopSplit() # stop splitting on all screens
      self.master.stopJoin()  # stop joining on all screens
      # Create new menu dialog
      dialog = self.SplitJoinDialog(master=self)
      x = e.x_root - self.master.winfo_rootx()
      y = e.y_root - self.master.winfo_rooty()
      dialog.place(anchor="center", x=x, y=y)
      # Reposition dialog if it goes outside the borders
      def _overfull_check():
        anchor = ""
        if y+dialog.winfo_height() > self.master.winfo_height():
          anchor = "s"
          y_ = y + 2 # avoid immediate Leave event
        else:
          anchor = "n"
          y_ = y
        if x+dialog.winfo_width() > self.master.winfo_width():
          anchor = anchor + "e"
          x_ = x + 2 # avoid immediate Leave event
        else:
          anchor = anchor + "w"
          x_ = x
        dialog.place(anchor=anchor, x=x_, y=y_)
      # Wait for the dialog to appear (width and height defined)
      # before the check
      self.after(1, _overfull_check)
    
    # Enable join on the adjacent screens
    def startJoin(self):
      screen1 = self.virtual[0][0].screen
      screen2 = self.virtual[1][0].screen
      screen1.startJoin(self, screen2)
      screen2.startJoin(self, screen1)
    
    # Swap the adjacent screens
    def swap(self):
      screen1 = self.virtual[0][0].screen
      screen2 = self.virtual[1][0].screen
      screen1.swap(screen2)
    
    # Destroy line
    def destroy(self):
      self.master.lines.remove(self) # remove line from parent
      # Destroy widget (after a little time to avoid errors)
      self.after(50, lambda: tk.Frame.destroy(self))
    
    # Menu dialog
    class SplitJoinDialog (tk.Frame):
      BG_COLOR = 'gray' # background color
      # Constructor
      # master : parent RealLine
      def __init__(self, master, *args, **kwargs):
        tk.Frame.__init__(self, master.master, *args, **kwargs)
        self['bg'] = self.BG_COLOR
        # Create command buttons
        btnSplit = tk.Button(self, text="Split")
        btnSplit.grid(row=1, sticky='news')
        btnSplit.bind("<1>", lambda e: master.master.startSplit(master.vertical))
        btnJoin = tk.Button(self, text="Join")
        btnJoin.grid(row=2, sticky='news')
        if master.joinable():
          btnJoin.bind("<1>", lambda e: master.startJoin())
        else:
          btnJoin['state'] = 'disabled'
        btnSwitch = tk.Button(self, text="Swap")
        btnSwitch.grid(row=3, sticky='news')
        if master.joinable():
          btnSwitch.bind("<1>", lambda e: master.swap())
        else:
          btnSwitch['state'] = 'disabled'
        # Destroy dialog when out of focus
        self.bind("<Leave>", self.onLeave)
      
      #~ def onEnter(self,e ):
        #~ try:
          #~ self.after_cancel(self.leaveTimer)
        #~ except:
          #~ pass
      
      # Destroy dialog when out of focus
      def onLeave(self, e):
        self.leaveTimer = self.after(100, lambda: self.destroy())
  
  class VirtualLine:
    # Constructor
    # real : support RealLine
    # side : on which side of the RealLine this VirtualLine is
    def __init__(self, real, side):
      self.real = real
      self.side = side
      self.real.virtual[self.side].append(self) # add to real line
      self.screen = None
    
    # Destroy virtual line
    def destroy(self):
      # Remove from real line (if not already removed)
      try:
        self.real.virtual[self.side].remove(self)
      except:
        pass
