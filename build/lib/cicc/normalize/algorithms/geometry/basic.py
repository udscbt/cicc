from functools import wraps

try:
  import cicc.normalize.algorithms as alg
except AttributeError:
  from ... import algorithms as alg

def generic_resize_factory (resize_function):
  @wraps(resize_function)
  def algorithm (IN, img, highres=None):
    import cv2
    import numpy as np
    
    if highres is not None:
      img = highres
        
    if IN.input_shape is not None:
      img = resize_function(img, IN.input_shape[1::-1])
      if IN.input_shape[2] == 1 and img.ndim == 3 and img.shape[2] == 3:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
      elif IN.input_shape[2] == 3 and (
        img.ndim == 2 or img.shape[2] == 1
      ):
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    
    if img.ndim == 2:
      img = img[:,:,np.newaxis]
    
    return [img]
  alg.default_algorithms['geometry'][resize_function.__name__] = algorithm
  return algorithm

@generic_resize_factory
def stretch (img, shape):
  import cv2
  return cv2.resize(img, shape)

@generic_resize_factory
def extend (img, shape):
  import cv2
  h,w = img.shape[:2]
  W,H = shape
  ratio = max(W/w, H/h)
  neww, newh = int(ratio*w), int(ratio*h)
  return cv2.resize(img, (neww, newh))[newh-H:, :W]

@generic_resize_factory
def fit (img, shape):
  import cv2
  import numpy as np
  h,w = img.shape[:2]
  d = 1 if img.ndim == 2 else img.shape[2]
  W,H = shape
  ratio = min(W/w, H/h)
  neww, newh = int(ratio*w), int(ratio*h)
  img = cv2.resize(img, (neww, newh))
  newimg = np.zeros((H,W,d), dtype=np.uint8)
  x,y = (W-neww)//2, (H-newh)//2
  newimg[y:y+newh,x:x+neww] = img
  return newimg
