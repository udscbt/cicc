import keras
import keras.backend as K
from . import patches

@patches(keras.activations, 'sumtoone')
def sumtoone (x):
  return x / K.sum(x, axis=-1, keepdims=1)
