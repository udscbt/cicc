# Tkinter
from tkinter import *
from tkinter.ttk import *
import warnings

def _make_no_collision_var (varname):
  def var (*append):
    return "__".join([
      "__{}__{}".format(
        __name__,
        varname
      ),
      *map(str, append)
    ]).replace('.','_').replace(' ', '_')
  var.__name__ = varname
  globals()[varname] = var

DEFAULT = 0
LONG_PRESS = 1
CLICK = 2
CLICK_AND_PRESS = 3
DOUBLE_CLICK = 4
TRIPLE_CLICK = 6

_make_no_collision_var('click_event_flag')
_make_no_collision_var('click_handler')
_make_no_collision_var('maximum_clicks')
_make_no_collision_var('click_timer')
_make_no_collision_var('click_delay')
_make_no_collision_var('click_count')

def setupClickEvents (widget, button=1, delay=500, max_clicks=0):
  flag = click_event_flag(button)
  click = lambda n: click_handler(button, n)
  maxck = maximum_clicks(button)
  timer = click_timer(button)
  d_lay = click_delay(button)
  count = click_count(button)
  
  setattr(widget, flag, True)
  setattr(widget, click(0), lambda e: None)
  setattr(widget, maxck, max_clicks)
  setattr(widget, timer, None)
  setattr(widget, d_lay, delay)
  setattr(widget, count, 0)
  def onPress (e):
    setattr(widget, count, getattr(widget, count)+1)
    if getattr(widget, timer) is not None:
      widget.after_cancel(getattr(widget, timer))
    setattr(
      widget,
      timer,
      widget.after(
        getattr(widget, d_lay),
        lambda e=e: executeCommand(e)
      )
    )
  
  def executeCommand (e):
    n_clicks = getattr(widget, count)
    if hasattr(widget, click(n_clicks)):
      getattr(widget, click(n_clicks))(e)
    else:
      getattr(widget, click(0))(e) # Default handler
    
    setattr(widget, timer, None)
    setattr(widget, count, 0) # count = 0
  
  def onRelease (e):
    # Do nothing if not waiting for event
    if getattr(widget, timer) is None:
      return
    
    # count += 1
    setattr(widget, count, getattr(widget, count)+1)
    # Stop early if max number of clicks reached
    if getattr(widget, count) == 2*getattr(widget, maxck):
      widget.after_cancel(getattr(widget, timer))
      executeCommand(e)
  
  widget.bind('<ButtonPress-{}>'.format(button), onPress)
  widget.bind('<ButtonRelease-{}>'.format(button), onRelease)

def changeClickDelay (widget, delay=500, button=1):
  flag = click_event_flag(button)
  d_lay = click_delay(button)
  if not hasattr(widget, flag):
    raise Exception(
      "Call {}.setupClickEvents on the widget "
      "before using this function".format(__name__)
    )
  setattr(widget, d_lay, delay)

def bindClickEvents (
  widget,
  handlers,
  button=1
):
  flag_ = click_event_flag(button)
  click_ = lambda n: click_handler(button, n)
  if not hasattr(widget, flag_):
    setupClickEvents(widget, button)
  for n, handler in handlers.items():
    setattr(widget, click_(n), handler)

# General 
def rgb_to_hex (value):
  if value is None:
    return None
  if isinstance(value, int):
    value = (value, value, value)
  if (
    isinstance(value, tuple) and
    len(value) == 3
  ):
    return '#{:02x}{:02x}{:02x}'.format(*value)
  else:
    raise ValueError(
      'Not a valid RGB tuple or grayscale value: {}'.format(value)
    )

def hex_to_rgb (value):
  if value is None:
    return None
  if not value.startswith('#'):
    raise ValueError(
      'Not a valid hex color: {}'.format(value)
    )
  if len(value) == 4:
    value = (
      value[1],
      value[2],
      value[3]
    )
  elif len(value) == 7:
    value = (
      value[1:3],
      value[3:5],
      value[5:7]
    )
  else:
    raise ValueError(
      'Not a valid hex color: {}'.format(value)
    )
  return tuple(
    int(x, 16)
    for x in value
  )

TRANSPARENT = -1

def split_alpha (color):
  if color is None or color == TRANSPARENT:
    alpha = 0
  elif len(color) == 4:
    alpha = color[-1]/255
    color = color[:-1]
  else:
    alpha = 1
  return color, alpha

# OpenCV
import cv2

class Cv2Alpha:
  def __init__ (self, original, alpha=1, immediate=True):
    self.alpha = alpha
    self.original = original
    self.immediate = immediate
    if self.immediate and self.alpha != 1 and self.alpha != 0:
      self.copy = original.copy()
    else:
      self.queue = []
  
  def add_command (self, fn, *args, **kwargs):
    if self.immediate:
      self.exec_command(fn, *args, **kwargs)
    else:
      self.queue.append((fn, args, kwargs))
  
  def exec_command (self, fn, *args, **kwargs):
    if self.alpha == 1:
      fn(self.original, *args, **kwargs)
    elif self.alpha != 0:
      fn(self.copy, *args, **kwargs)
  
  def apply_postponed (self):
    self.copy = self.original.copy()
    for fn, args, kwargs in self.queue:
      self.add_command(fn, *args, **kwargs)
    self.apply()
  
  def apply (self):
    import cv2
    if not self.immediate:
      self.copy = self.original.copy()
      for fn, args, kwargs in self.queue:
        self.exec_command(fn, *args, **kwargs)
    if self.alpha != 1 and self.alpha != 0:
      cv2.addWeighted(self.copy, self.alpha, self.original, 1-self.alpha, 0, self.original)
  
  @classmethod
  def do (cls, alpha, fn, original, *args, **kwargs):
    obj = cls(original, alpha)
    obj.add_command(fn, *args, **kwargs)
    obj.apply()

TEXT_ANCHORS = [
  'bottom',
  'top',
  'left',
  'right',
  'bottom left',
  'bottom right',
  'top left',
  'top right',
  'center',
  'cv2',
]
for anchor in TEXT_ANCHORS:
  globals()['TEXT_ANCHOR_{}'.format(anchor.upper().replace(' ', '_'))] = anchor

TEXT_ALIGNMENTS = [
  'left',
  'right',
  'center',
]
for anchor in TEXT_ALIGNMENTS:
  globals()['TEXT_ALIGNMENT_{}'.format(anchor.upper().replace(' ', '_'))] = anchor


def getTextSize_ex (
  text,
  fontFace=0,
  fontScale=1,
  thickness=1,
  lead=1
):
  lines = text.split("\n")
  width, height = 0, 0
  for i, line in enumerate(lines):
    (w,h), baseline = cv2.getTextSize(text, fontFace, fontScale, thickness)
    width = max(width, w)
    vertical_space = h+baseline
    if i > 0:
      vertical_space *= lead
    height += vertical_space
  
  return (int(width), int(height)), int(baseline)

def format_multiline (
  text,
  fontFace=0,
  fontScale=1,
  thickness=1,
  lead=1,
  align=TEXT_ALIGNMENT_LEFT,
):
  lines = text.split("\n")
  width, height = 0, 0
  l,r,t,b = 0,0,0,0
  minl, maxr = 0,0
  orgs = []
  for i, line in enumerate(lines):
    (w,h), baseline = cv2.getTextSize(line, fontFace, fontScale, thickness)
    
    if align == TEXT_ALIGNMENT_LEFT:
      r = l+w
    elif align == TEXT_ALIGNMENT_RIGHT:
      l = r-w
    else:
      l = -w/2
      r = +w/2
    maxr = max(maxr, r)
    minl = min(minl, l)
    
    b = t+h
    orgs.append([int(l), int(b)])
    t += (h+baseline)*lead
  
  for i in range(len(orgs)):
    orgs[i][0] -= int(minl)
    orgs[i] = tuple(orgs[i])
  
  return orgs, (int(maxr-minl), int(b)), int(baseline)

def putText_multiline (
  img, text, org,
  fontFace=0,
  fontScale=1,
  color=(0,0,0),
  thickness=1,
  anchor=TEXT_ANCHOR_CV2,
  box_color=TRANSPARENT,
  box_stroke=0,
  box_fill=TRANSPARENT,
  lead=1,
  align=TEXT_ALIGNMENT_CENTER,
  queued=False
):
  orgs, (w,h), baseline = format_multiline(text, fontFace, fontScale, thickness, lead, align)
  bottom = h+baseline
  top = 0
  left = 0
  right = w
  x,y = org
  if anchor == TEXT_ANCHOR_CV2:
    y += h
  else:
    if anchor.endswith(TEXT_ANCHOR_LEFT):
      x += -left
    elif anchor.endswith(TEXT_ANCHOR_RIGHT):
      x += -right
    else:
      x += -(left+right)/2
      
    if anchor.startswith(TEXT_ANCHOR_BOTTOM):
      y += -bottom
    elif anchor.startswith(TEXT_ANCHOR_TOP):
      y += -top
    else:
      y += -(top+bottom)/2
    
    x = int(x)
    y = int(y)
  
  if queued is True or queued is False:
    queues = True
  else:
    queues = queued
  for org, line in zip(orgs, text.split("\n")):
    queues = putText_ex(
      img, line, (x+org[0], y+org[1]),
      fontFace,
      fontScale,
      color,
      thickness,
      TEXT_ANCHOR_CV2,
      box_color,
      box_stroke,
      box_fill,
      queues
    )
  
  if queued is False:
    if queues is not True:
      for queue in queues:
        queue.apply()
    return img
  else:
    return queues
  

def putText_ex (
  img, text, org,
  fontFace=0,
  fontScale=1,
  color=(0,0,0),
  thickness=1,
  anchor=TEXT_ANCHOR_CV2,
  box_color=TRANSPARENT,
  box_stroke=0,
  box_fill=TRANSPARENT,
  queued=False
):
  # ~ print(text, org, fontFace, fontScale, color, thickness, anchor, box_color, box_stroke, box_fill, queued)
  
  color, alpha = split_alpha(color)
  box_color, box_alpha = split_alpha(box_color)
  fillcolor, fillalpha = split_alpha(box_fill)
  try:
    queue_fill, queue_box, queue_text = queued
  except:
    queue_fill = Cv2Alpha(img, fillalpha, immediate=True)
    queue_box = Cv2Alpha(img, box_alpha, immediate=False)
    queue_text = Cv2Alpha(img, alpha, immediate=False)
  
  shape, baseline = cv2.getTextSize(text, fontFace, fontScale, thickness)
  bottom = +baseline
  top = -shape[1]
  left = 0
  right = shape[0]
  x,y = org
  if anchor != TEXT_ANCHOR_CV2:
    if anchor.endswith(TEXT_ANCHOR_LEFT):
      x += -left
    elif anchor.endswith(TEXT_ANCHOR_RIGHT):
      x += -right
    else:
      x += -(left+right)/2
      
    if anchor.startswith(TEXT_ANCHOR_BOTTOM):
      y += -bottom
    elif anchor.startswith(TEXT_ANCHOR_TOP):
      y += -top
    else:
      y += -(top+bottom)/2
    
    x = int(x)
    y = int(y)
  
  queue_fill.add_command(cv2.rectangle, (x+left,y+top), (x+right,y+bottom), fillcolor, -1)
  queue_box.add_command(cv2.rectangle, (x+left,y+top), (x+right,y+bottom), box_color, box_stroke)
  queue_text.add_command(cv2.putText, text, (x,y), fontFace, fontScale, color, thickness)
  
  if queued is False:
    queue_fill.apply()
    queue_box.apply()
    queue_text.apply()
    return img
  else:
    return (queue_fill, queue_box, queue_text)
