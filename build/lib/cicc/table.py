# -*- coding: utf-8 -*-
import numpy as np

box_characters = u"""
┌┬─┐
├┼─┤
││ │
└┴─┘
"""

try:
  print("{}\b ".format(box_characters[1]), end="")
except UnicodeEncodeError:
  box_characters = r"""
/+-\
++-+
|| |
\+-/
"""

TOP_LEFT     = box_characters[1]
TOP          = box_characters[2]
HORIZONTAL   = box_characters[3]
TOP_RIGHT    = box_characters[4]
LEFT         = box_characters[6]
CENTER       = box_characters[7]
RIGHT        = box_characters[9]
VERTICAL     = box_characters[11]
BOTTOM_LEFT  = box_characters[16]
BOTTOM       = box_characters[17]
BOTTOM_RIGHT = box_characters[19]


def pretty_table (table, **kwargs):
  def format_element (element):
    classes = element.__class__.__mro__
    for cls in classes:
      key = '{}_format'.format(cls.__name__)
      if key in kwargs:
        formatter = kwargs[key]
        if hasattr(formatter, '__call__'):
          return formatter(element)
        else:
          return formatter.format(element)
    return str(element)
  
  table = [[format_element(element) for element in row] for row in table]
  widths = np.array([
    [
      len(element)+2
      for element in row
    ]
    for row in table
  ]).max(axis=0)
  lines = [
    HORIZONTAL*width
    for width in widths
  ]
  
  top_line = (
    TOP_LEFT +
    TOP.join(lines) +
    TOP_RIGHT +
    "\n"
  )
  bottom_line = (
    BOTTOM_LEFT +
    BOTTOM.join(lines) +
    BOTTOM_RIGHT
  )
  center_line = (
    LEFT +
    CENTER.join(lines) +
    RIGHT +
    "\n"
  )
  
  return (
    top_line +
    center_line.join([
      (
        VERTICAL +
        VERTICAL.join([
          element.center(width)
          for element, width in zip(row, widths)
        ]) +
        VERTICAL +
        "\n"
      )
      for row in table
    ]) +
    bottom_line
  )
