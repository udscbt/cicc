import dlib
import numpy
import os
import sys

class ExternalLandmarks:
  def __init__ (self):
    self.clear()
  
  def set (self, landmarks):
    self.landmarks = landmarks
  
  def clear (self):
    self.landmarks = None

EXTERNALLY_FOUND = ExternalLandmarks()

_dir = os.path.dirname(os.path.abspath(__file__))
_module = sys.modules[__name__]

_segments = [
  ('jaw', 16),
  ('rbrow', 21),
  ('lbrow', 26),
  ('nose', 35),
  ('reye', 41),
  ('leye', 47),
  ('mouth', 67)
]
segments = {}
_idx = 0

for _name, _last in _segments:
  segments[_name] = range(_idx, _last+1)
  _idx = _last+1
  _module.__dict__[_name.upper()] = segments[_name]

get_face = dlib.get_frontal_face_detector()
try:
  dlib_detect = dlib.shape_predictor(
    os.path.join(
      _dir,
      "shape_predictor_68_face_landmarks.dat"
    )
  )
  DLIB_DETECTOR_AVAILABLE = True
except RuntimeError as e:
  dlib_error = e
  def dlib_detect (*args, **kwarg):
    raise dlib_error
  DLIB_DETECTOR_AVAILABLE = False

def overlapping (face1, face2):
  l1,r1,t1,b1 = face1.left(), face1.right(), face1.top(), face1.bottom()
  
  l2,r2,t2,b2 = face2.left(), face2.right(), face2.top(), face2.bottom()
  
  if (
    max(l1,r1) < min(l2,r2) or
    max(l2,r2) < min(l1,r1) or
    max(t1,b1) < min(t2,b2) or
    max(t2,b2) < min(t1,b1)
  ):
    return 0
  
  xs = sorted([l1,r1,l2,r2])
  ys = sorted([t1,b1,t2,b2])
  
  intersection = (xs[2]-xs[1])*(ys[2]-ys[1])
  union = (r1-l1)*(b1-t1)+(r2-l2)*(b2-t2)-intersection
  
  return intersection/union
  
def find_face (face, cmp_faces, threshold=0.1):
  if not isinstance(cmp_faces, dict):
    cmp_faces = {i:cmp_face for i,cmp_face in enumerate(cmp_faces)}
  maxoverlapping = 0
  bestfit = None
  for i, cmp_face in cmp_faces.items():
    overlap = overlapping(face, cmp_face)
    if overlap > maxoverlapping:
      maxoverlapping = overlap
      bestfit = i
  if maxoverlapping < threshold:
    return None
  return bestfit

def get_faces_indexes (faces, old_faces, counter=0):
  if len(faces) == 0:
    return []
  
  if not isinstance(old_faces, dict):
    old_faces = {i:old_face for i,old_face in enumerate(old_faces)}
  faces = {i:face for i,face in enumerate(faces)}
  indexes = [None for face in faces]
  for old_idx, old_face in old_faces.items():
    idx = find_face(old_face, faces) # Find new position
    if idx is not None:
      # Copy index of old face
      faces.pop(idx)
      indexes[idx] = old_idx
  for idx, old_idx in enumerate(indexes):
    if old_idx is None:
      while counter in indexes:
        counter += 1
      indexes[idx] = counter
    
  return indexes

def detect (img, box):
  global EXTERNALLY_FOUND
  if EXTERNALLY_FOUND.landmarks is not None:
    return EXTERNALLY_FOUND.landmarks
  landmarks = dlib_detect (img, box)
  landmarks = numpy.array([(p.x, p.y) for p in landmarks.parts()])
  return landmarks

def get_segment (segment, landmarks=None, imgface=(None, None)):
  global segments
  if isinstance(segment, str):
    if segment not in segments.keys():
      raise KeyError(
        "{}: Unrecognized segment '{}'".format(
          __name__,
          segment
        )
      )
    segment = segments[segment]
  if landmarks is None:
    landmarks = detect(*imgface)
  return landmarks[segment]

def get_segment_center (segment, landmarks=None, imgface=(None, None)):
  segment = get_segment(segment, landmarks, imgface)
  return segment.mean(axis=0)

def get_triangulation (landmarks=None, imgface=(None, None), triangles=None):
  global triangulation
  if triangles is None:
    triangles = triangulation
  if landmarks is None:
    landmarks = detect(*imgface)
  tri = []
  for row in triangles:
    try:
      triangle = []
      for point in row:
        triangle.append(landmarks[point])
      tri.append(triangle)
    except:
      pass
  return numpy.array(tri)

triangulation = numpy.array([[38,40,37],
[35,30,29],
[38,37,20],
[18,37,36],
[33,32,30],
[54,64,53],
[30,32,31],
[59,48,60],
[19,79,20],
[40,31,41],
[36,37,41],
[21,39,38],
[35,34,30],
[51,33,52],
[40,29,31],
[57,58,66],
[36,17,18],
[35,52,34],
[2,73,1],
[65,66,62],
[58,67,66],
[53,63,52],
[61,67,49],
[53,65,63],
[56,66,65],
[55,10,9],
[4,73,3],
[64,54,55],
[69,74,70],
[25,79,78],
[18,17,72],
[43,42,22],
[23,79,24],
[46,54,35],
[14,68,13],
[16,26,78],
[0,73,17],
[1,0,36],
[73,72,17],
[79,19,72],
[19,37,18],
[1,36,41],
[0,17,36],
[37,19,20],
[18,72,19],
[21,38,20],
[39,40,38],
[79,23,20],
[28,29,39],
[41,31,2],
[59,67,58],
[29,30,31],
[34,33,30],
[21,27,39],
[28,42,29],
[52,33,34],
[62,66,67],
[3,73,2],
[48,4,3],
[73,0,1],
[41,2,1],
[31,3,2],
[37,40,41],
[39,29,40],
[57,7,58],
[31,48,3],
[5,4,48],
[70,73,4],
[32,49,31],
[60,49,59],
[7,69,6],
[10,75,9],
[59,5,48],
[70,4,5],
[7,6,58],
[70,5,6],
[31,49,48],
[49,67,59],
[6,59,58],
[6,5,59],
[8,7,57],
[69,70,6],
[12,71,11],
[75,74,69],
[48,49,60],
[32,33,50],
[49,50,61],
[49,32,50],
[51,61,50],
[62,67,61],
[33,51,50],
[63,65,62],
[51,62,61],
[51,52,63],
[51,63,62],
[52,35,53],
[47,46,35],
[54,10,55],
[56,57,66],
[56,8,57],
[69,8,9],
[69,7,8],
[53,55,65],
[9,8,56],
[9,75,69],
[56,55,9],
[11,71,10],
[77,71,13],
[13,71,12],
[10,76,75],
[10,71,76],
[65,55,56],
[10,54,11],
[53,64,55],
[53,35,54],
[12,54,13],
[12,11,54],
[54,14,13],
[68,77,13],
[35,42,47],
[16,77,68],
[45,16,15],
[26,25,78],
[15,68,14],
[15,16,68],
[22,42,27],
[42,35,29],
[27,42,28],
[44,25,45],
[44,47,43],
[46,14,54],
[45,46,44],
[45,14,46],
[39,27,28],
[22,23,43],
[21,22,27],
[21,20,23],
[43,23,24],
[22,21,23],
[44,43,24],
[47,42,43],
[25,44,24],
[46,47,44],
[16,45,26],
[15,14,45],
[45,25,26],
[24,79,25],
[77,78,71],
[77,16,78]])

triangulation_ext = numpy.array([
[0, 1, 28],
[1, 2, 28],
[2, 3, 28],
[3, 4, 28],
[4, 5, 28],
[5, 6, 28],
[6, 7, 28],
[7, 8, 28],
[8, 9, 28],
[9, 10, 28],
[10, 11, 28],
[11, 12, 28],
[12, 13, 28],
[13, 14, 28],
[14, 15, 28],
[15, 16, 28],
[0, 17, 28],
[16, 26, 28],
[17, 18, 28],
[18, 19, 28],
[19, 20, 28],
[20, 21, 28],
[21, 22, 28],
[22, 23, 28],
[23, 24, 28],
[24, 25, 28],
[25, 26, 28]
])
