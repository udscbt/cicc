import os
icons_dir = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'icons')
ICONS = {}
for f in os.listdir(icons_dir):
  name = os.path.splitext(f)[0].upper()
  path = os.path.join(icons_dir, f)
  ICONS[name] = path
