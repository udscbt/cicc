import tkinter as tk

def collapsible (widget):
  class CollapsibleWidget (tk.Frame):
    BTN_WIDTH = 20
    WDG_WIDTH = 200
    
    btn = None
    wdg = None
    expanded = None
    
    def __init__ (self, master, *args, **kwargs):
      tk.Frame.__init__(self, master)
      self.btn = tk.Button(self, text=">", command=self.expandcollapse)
      self.wdg = widget(self, *args, **kwargs)
      
      self.expanded = False
      
      self.btn.place(anchor="nw", x=0, y=0, width=self.BTN_WIDTH, relheight=1)
      self.expandcollapse()
    
    def expandcollapse (self, e=None):
      self.expanded = not self.expanded
      if self.expanded:
        self.btn['text'] = '>'
        self.wdg.place(anchor="nw", x=self.BTN_WIDTH, y=0, width=self.WDG_WIDTH, relheight=1)
        self['width'] = self.BTN_WIDTH+self.WDG_WIDTH
      else:
        self.btn['text'] = '<'
        self.lst.place(anchor="nw", x=self.BTN_WIDTH, y=0, width=0, relheight=1)
        self['width'] = self.BTN_WIDTH
    
    def __getattr__ (self, k):
      return self.wdg.__getattr__(k)
