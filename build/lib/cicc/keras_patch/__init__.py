def patches(obj, attr):
    if hasattr(obj, attr):
        to_patch = getattr(obj, attr)
        name = to_patch.__qualname__
        origin = to_patch.__module__
        doc = to_patch.__doc__
    else:
        if isinstance(obj, type):
            name = obj.__name__ + attr
            origin = obj.__module__
        else:
            name = attr
            origin = obj.__name__
        doc = ""

    def patch(patch_with):
        patch_with.__doc__ = ("""
Patches {} from {}

""".format(name, origin) + (patch_with.__doc__ or "") + """

Original docstring:
{}
""".format(doc))
        setattr(obj, attr, patch_with)
        return patch_with

    return patch


from .multidim_metrics import *
from .additional_metrics import *
from .additional_activations import *
