from .. import Layer, Value, add_unique_name_generator, available_layers
from cicc.normalize.algorithms import default_algorithms

import keras

@add_unique_name_generator('input_')
class Input (Layer):
  CLASSNAME = 'Input'
  KERAS = keras.layers.InputLayer
  
  PARAMETERS = [
    (
      'Width', int, ('width',), ('updateWidget',)
    ),
    (
      'Height', int, ('height',), ('updateWidget',)
    ),
    (
      'Grayscale', bool, ('grayscale',), ('updateWidget',)
    ),
    (
      'Normalization algorithm (GUI only)',
      list(default_algorithms['geometry'].keys()),
      ('normalize',),
      ('updateWidget',)
    ),
  ]
  
  @property
  def shape (self):
    return (
      self.width,
      self.height,
      1 if self.grayscale else 3
    )
  
  def __init__ (self, *args, **kwargs):
    self.width = 0
    self.height = 0
    self.grayscale = True
    self.normalize = 'none'
    Layer.__init__(self, *args, **kwargs)
    self._lastoutput = Value(self, name=self.name)
  
  def fromkeras (self, keras):
    Layer.fromkeras(self, keras)
    _, self.width, self.height, depth = self.keras.output_shape
    if depth == 1:
      self.grayscale = True
    else:
      self.grayscale = False
  
  def keras_kwargs (self):
    kwargs = Layer.keras_kwargs(self)
    kwargs.update(
      input_shape=self.shape
    )
    return kwargs
  
  def tokeras (self):
    Layer.tokeras(self)
    self._lastoutput.keras = self.keras.output
  
  def updateWidget (self):
    if hasattr(self, 'widget') and self.widget is not None:
      self.widget.update()
  
  def connect (self, input):
    pass

available_layers[Input.KERAS] = Input
