from tkinter import *
from tkinter.ttk import *

# ~ def print_before (f):
  # ~ _f = f
  # ~ def decorated (self, *args, **kwargs):
    # ~ if _f.__name__ == '__init__':
      # ~ lbl = self.__class__.__name__
    # ~ else:
      # ~ lbl = repr(self)
    # ~ print(lbl, args, kwargs)
    # ~ return _f(self,*args,**kwargs)
  # ~ return decorated

# ~ Frame.grid = print_before(Frame.grid)
# ~ LabelFrame.grid = print_before(LabelFrame.grid)

# ~ class SplitLayout:
  # ~ SPACES = ' \t\n\r'
  # ~ NESTED = '()'
  # ~ STRING = '{}'
  # ~ VERT_SEP = '|'
  # ~ HOR_SEP = '-'
  # ~ SPECIAL = SPACES+NESTED+STRING+VERT_SEP+HOR_SEP
  # ~ LAYOUT_PREFIX = 'L'
  
  # ~ def __init__ (self, s, parent, pwidget=None):
    # ~ raise NotImplemented()
    
    # ~ index = 0
    # ~ self.layout = s
    # ~ self.parent = parent
    # ~ self.pwidget=pwidget or parent
    
    # ~ self.widget = None
    # ~ self.row = 0
    # ~ self.column = 0
    
    # ~ self.sublayouts = []
    # ~ self.widgets = []
    
    # ~ while index < len(s):
      # ~ index = self.parse_next(index)
  
  # ~ def parse_next (self, index):
    # ~ if self.layout[index] in self.SPACES:
      # ~ return index + 1
    
    # ~ if self.layout[index] == self.STRING[0]:
      # ~ return self.parse_string(index)
    
    # ~ if self.widget is None:
      # ~ self.widget = Frame(self.pwidget)
    
    # ~ if self.layout[index] == self.NESTED[0]:
      # ~ return self.parse_nested(index)
    
    # ~ if self.layout[index] == self.VERT_SEP:
      # ~ l = self.__class__(
        # ~ self.layout[index+1:],
        # ~ self.parent,
        # ~ self.widget
      # ~ )
      # ~ l.widget.grid(
        # ~ row=0,
        # ~ column=self.column+1,
        # ~ rowspan=self.row+1,
        # ~ sticky='news'
      # ~ )
      # ~ self.sublayouts.append(l)
      # ~ return len(self.layout)
    
    # ~ if self.layout[index] == self.HOR_SEP:
      # ~ l = self.__class__(
        # ~ self.layout[index+1:],
        # ~ self.parent,
        # ~ self.widget
      # ~ )
      # ~ l.widget.grid(
        # ~ column=0,
        # ~ row=self.row+1,
        # ~ columnspan=self.column+1,
        # ~ sticky='news'
      # ~ )
      # ~ self.sublayouts.append(l)
      # ~ return len(self.layout)
    
    # ~ ############
    # ~ return self.parse_name(index)
  
  # ~ def parse_nested (self, index):
    # ~ count = 0
    # ~ for idx in range(index, len(self.layout)):
      # ~ if self.layout[idx] == self.NESTED[0]:
        # ~ count += 1
      # ~ elif self.layout[idx] == self.NESTED[1]:
        # ~ count -= 1
      # ~ if count == 0:
        # ~ break
    # ~ l = self.__class__(
      # ~ self.layout[index+1:idx],
      # ~ self.parent,
      # ~ self.widget
    # ~ )
    # ~ l.widget.grid(
      # ~ row=self.row,
      # ~ column=self.column,
      # ~ sticky='news'
    # ~ )
    # ~ self.sublayouts.append(l)
    # ~ return idx + 1
  
  # ~ def parse_string (self, index):
    # ~ idx = index + 1
    # ~ while idx < len(self.layout):
      # ~ if self.layout[idx] == self.STRING[0]:
        # ~ if self.layout[idx+1] == self.STRING[0]:
          # ~ idx += 1
        # ~ else:
          # ~ raise ValueError ("Nested string")
      # ~ elif self.layout[idx] == self.STRING[1]:
        # ~ if idx < len(self.layout)-1 and self.layout[idx+1] == self.STRING[1]:
          # ~ idx += 1
        # ~ else:
          # ~ break
      # ~ idx += 1
    # ~ if self.widget is not None:
      # ~ raise ValueError ("String not label of LabelFrame")
    
    # ~ substring = self.layout[index+1:idx].replace(
      # ~ self.STRING[0]*2,
      # ~ self.STRING[0]
    # ~ ).replace(
      # ~ self.STRING[1]*2,
      # ~ self.STRING[1]
    # ~ )
    
    # ~ self.widget = LabelFrame(
      # ~ self.pwidget,
      # ~ text=substring
    # ~ )
    # ~ return idx + 1
  
  # ~ def parse_name (self, index):
    # ~ for idx in range(index+1, len(self.layout)):
      # ~ if self.layout[idx] in self.SPECIAL:
        # ~ break
    # ~ else:
      # ~ idx = len(self.layout)
    
    # ~ name = self.layout[index:idx]
    # ~ if name.startswith(self.LAYOUT_PREFIX):
      # ~ self.widget.layout = self
      # ~ setattr(self.parent, name, self.widget)
    # ~ else:
      # ~ self.widgets.append((
        # ~ self.layout[index:idx],
        # ~ self.row,
        # ~ self.column,
        # ~ 1,
        # ~ 1
      # ~ ))
    # ~ return idx
  
  # ~ def populate (self):
    # ~ for w, r, c, rs, cs in self.widgets:
      # ~ getattr(self.parent, w).grid(
        # ~ in_=self.widget,
        # ~ row=r,
        # ~ column=c,
        # ~ rowspan=rs,
        # ~ columnspan=cs,
        # ~ sticky='news'
      # ~ )
    
    # ~ for sublayout in self.sublayouts:
      # ~ sublayout.populate()

class LayoutError (SyntaxError):
    def __init__ (self, msg, ridx, cidx):
      SyntaxError.__init__(
        self,
        msg + " on line {}, column {}".format(ridx, cidx)
      )

class GridLayout:
  SPACES = ' \t\n\r'
  CROSS = '+'
  VSEP = '|'
  VERT_SEP = VSEP+CROSS
  HSEP = '-'
  HOR_SEP = HSEP+CROSS
  BREAK = SPACES+VERT_SEP+HOR_SEP
  
  LABEL_OPEN = '{'
  LABEL_CLOSE = '}'
  LABEL = LABEL_OPEN+LABEL_CLOSE
  
  LITERAL_OPEN = '"'
  LITERAL_CLOSE = '"'
  LITERAL = LITERAL_OPEN+LITERAL_CLOSE
  
  SPECIAL = BREAK+LABEL+LITERAL
  
  OPTIONS = [
    "spaces",
    "cross",
    "vsep",
    "hsep",
    "label_open",
    "label_close",
    "literal_open",
    "literal_close"
  ]
  
  numColumns = 0
  numRows = 0
  numCells = 0
  cells = []
  
  def __init__ (self, layout, row_weights=None, column_weights=None, **kwargs):
    for k in kwargs:
      if k not in self.OPTIONS:
        raise TypeError("__init__() got an unexpected keyword argument '{}'".format(k))
      setattr(self, k.upper(), kwargs[k])
    
    self.layout = layout.split("\n")
    self.widgets = []
    
    self._topremoved = 0
    self._leftremoved = 0
    self._remove_padding()
    self._height, self._width = self._remove_borders()
    self._get_split_positions()
    self._fill_gaps()
    self._make_cells()
    
    if isinstance(row_weights, list):
      self.row_weights = [None] * self.numRows
      for i in range(min(self.numRows, len(row_weights))):
        self.row_weights[i] = row_weights[i]
    elif isinstance(row_weights, dict):
      self.row_weights = [None] * self.numRows
      for k in row_weights:
        self.row_weights[k] = row_weights[k]
    else:
      self.row_weights = [row_weights]*self.numRows
      
    if isinstance(column_weights, list):
      self.column_weights = [None] * self.numColumns
      for i in range(min(self.numColumns, len(column_weights))):
        self.column_weights[i] = column_weights[i]
    elif isinstance(column_weights, dict):
      self.column_weights = [None] * self.numColumns
      for k in column_weights:
        self.column_weights[k] = column_weights[k]
    else:
      self.column_weights = [column_weights]*self.numColumns
  
  def get_layout (self):
    s = ""
    s += '+'+('-'*self._width)+'+\n'
    for row in self.layout:
      s += '|'+row+'|\n'
    s += '+'+('-'*self._width)+'+'
    return s
  
  def _remove_padding (self):
    for ridx, row in enumerate(self.layout):
      if len([x for x in row if x not in self.SPACES]):
        break
    self.layout = self.layout[ridx:]
    self._topremoved += ridx
    
    for ridx, row in enumerate(reversed(self.layout)):
      if len([x for x in row if x not in self.SPACES]):
        break
    if ridx != 0:
      self.layout = self.layout[:-ridx]
    
    left_pad = None
    for row in self.layout:
      for cidx, char in enumerate(row):
        if char not in self.SPACES:
          break
      if left_pad is None or cidx < left_pad:
        left_pad = cidx
    for ridx, row in enumerate(self.layout):
      self.layout[ridx] = row[left_pad:]
    self._leftremoved += left_pad
    
    right_limit = 0
    for row in self.layout:
      for cidx, char in enumerate(reversed(row)):
        if char not in self.SPACES:
          break
      newlimit = len(row)-cidx
      if newlimit > right_limit:
        right_limit = newlimit
    for ridx, row in enumerate(self.layout):
      self.layout[ridx] = row[:right_limit]
  
  def _remove_top (self):
    for char in self.layout[0]:
      if char in self.HOR_SEP:
        self.layout = self.layout[1:]
        self._topremoved += 1
        return True
    return False
  
  def _remove_left (self):
    for row in self.layout:
      if row[0] in self.VERT_SEP:
        self.layout = [row[1:] for row in self.layout]
        self._leftremoved += 1
        return True
    return False
  
  def _remove_bottom (self):
    for char in self.layout[-1]:
      if char in self.HOR_SEP:
        self.layout = self.layout[:-1]
        return True
    return False
  
  def _get_maxc (self):
    maxc = 0
    for row in self.layout:
      if len(row)-1 > maxc:
        maxc = len(row)-1
    return maxc
  
  def _remove_right (self, maxc=None):
    if maxc is None:
      maxc = self._get_maxc()
    for row in self.layout:
      if len(row) > maxc and row[maxc] in self.VERT_SEP:
        self.layout = [row[:maxc] for row in self.layout]
        return True
    return False
        
  def _remove_borders (self):
    while self._remove_top():
      pass
    while self._remove_left():
      pass
    while self._remove_bottom():
      pass
    maxc = self._get_maxc()
    while self._remove_right(maxc):
      maxc -= 1
    return len(self.layout), maxc+1
  
  def _get_split_positions (self):
    columns = []
    rows = []
    
    for ridx, row in enumerate(self.layout):
      for cidx, char in enumerate(row):
        if char in self.VERT_SEP:
          columns.append(cidx)
        if char in self.HOR_SEP:
          rows.append(ridx)
    
    self._columns = sorted(set(columns+[-1,self._width]))
    self._rows = sorted(set(rows+[-1,self._height]))
    self.numColumns = len(columns)
    self.numRows = len(rows)
  
  def _fill_gaps (self):
    self.layout = [
      row + " "*(self._width-len(row))
      for row in self.layout
    ]
  
  def _make_cells (self):
    self._labels = [
      [None for cidx in range(self._width)]
      for ridx in range(self._height)
    ]
    cell = 0
    self._cells = []
    
    for ridx in range(self._height):
      for cidx in range(self._width):
        if self._labels[ridx][cidx] is not None:
          continue
        self._cells.append(
          self._search_cell(
            ridx,
            cidx,
            cell,
            self._labels
          )
        )
        cell += 1
    
    self.cells = []
    for cell in self._cells:
      l,t,r,b,wdt,lbl = cell
      # ~ if lbl != '': WHY?
      l = self._columns.index(l-1)
      t = self._rows.index(t-1)
      r = self._columns.index(r)
      b = self._rows.index(b)
      self.cells.append({
        'row' : t,
        'rowspan' : b-t,
        'column' : l,
        'columnspan' : r-l,
        'widget' : wdt,
        'label' : lbl
      })
      self.numCells = len(self.cells)
  
  def _search_cell (self, ridx, cidx, cell, labels):
    left = cidx
    right = None
    top = ridx
    bottom = None
    
    # Get width of cell from first line
    for cidx in range(left, self._width):
      char = self.layout[top][cidx]
      if char in self.VERT_SEP:
        labels[top][cidx] = char
        right = cidx
        break
      elif char in self.HOR_SEP:
        # There shouldn't be any row separator in the first line
        self.raise_layout_error("Unexpected row separator", top, cidx)
    else:
      right = self._width
    
    cell_widget = ""
    widget_found = False
    cell_label = None
    label_type = None
    # Search for the row separator
    # and the cell label
    for ridx in range(top, self._height):
      row = self.layout[ridx]
      # Check for column separator after it has been found
      if right < self._width:
        if row[right] in self.VERT_SEP:
          labels[ridx][right] = row[right]
        else:
          self.raise_layout_error("Missing column separator", ridx, right)
      for cidx in range(left, right):
        # The position shouldn't be already visited
        if labels[ridx][cidx] is not None:
          self.raise_layout_error("Cell overflow", ridx, cidx)
        labels[ridx][cidx] = cell
        
        char = row[cidx]
        # Parse labels differently
        if not widget_found and label_type is not None:
          if char == self.LABEL_CLOSE[label_type]:
            # Ignore first close symbol in double-close
            if (
              cidx < self._width -1 and
              row[cidx+1] == self.LABEL_CLOSE[label_type]
            ):
              continue
            # Get second close symbol in double-close as literal
            elif (
              cidx > 0 and
              row[cidx-1] == self.LABEL_CLOSE[label_type]
            ):
              cell_label += char
            # Close label
            else:
              widget_found = True
          else:
            cell_label += char
          # Prevent wrong parsing
          continue
        
        # Parse literals differently
        if (
          cell_widget == "" and widget_found # only possible without error if a literal was found
          and label_type is not None
        ):
          if char == self.LITERAL_CLOSE[label_type]:
            # Ignore first close symbol in double-close
            if (
              cidx < self._width -1 and
              row[cidx+1] == self.LITERAL_CLOSE[label_type]
            ):
              continue
            # Get second close symbol in double-close as literal
            elif (
              cidx > 0 and
              row[cidx-1] == self.LITERAL_CLOSE[label_type]
            ):
              cell_label += char
            # Close label
            else:
              label_type = None
          else:
            cell_label += char
          # Prevent wrong parsing
          continue
        
        # Parse widget names differently
        if widget_found:
          if char not in self.BREAK:
            self.raise_layout_error(
              "Unexpected character ({})".format(char),
              ridx,
              cidx
            )
        else:
          # Widget name start not found yet
          if cell_widget == "":
            if char in self.LABEL:
              self.raise_layout_error(
                "Unexpected label delimiter",
                ridx,
                cidx
              )
            # Start parsing literal
            elif char in self.LITERAL_OPEN:
              widget_found = True
              label_type = self.LITERAL_OPEN.index(char)
              cell_label = ""
              continue
            elif char in self.LITERAL_CLOSE:
              self.raise_layout_error(
                "Unexpected literal terminator",
                ridx,
                cidx
              )
            # Start parsing the widget name
            elif char not in self.BREAK:
              cell_widget += char
              continue
          # Already parsing the widget name
          else:
            # Stop parsing the widget name
            if char in self.BREAK:
              widget_found = True
            # Start parsing the label
            elif char in self.LABEL_OPEN:
              label_type = self.LABEL_OPEN.index(char)
              cell_label = ""
              continue
            elif char in self.LABEL_CLOSE:
              self.raise_layout_error(
                "Unexpected label terminator",
                ridx,
                cidx
              )
            else:
              cell_widget += char
              continue            
        
        if char in self.HOR_SEP:
          # Row separators should start from the first column
          if cidx == left:
            bottom = ridx
          elif bottom is None:
            self.raise_layout_error("Unexpected row separator", ridx, cidx)
          labels[ridx][cidx] = char
        else:
          # Row separators should continue for the whole line
          if bottom is not None:
            self.raise_layout_error("Missing row separator", ridx, cidx)
          # There shouldn't be any column separator inside the cell
          if char in self.VERT_SEP:
            self.raise_layout_error("Unexpected column separator", ridx, cidx)
      if bottom is not None:
        # Check for cross in bottom-right corner
        if right < self._width:
          if row[right] in self.CROSS:
            labels[ridx][right] = row[right]
          else:
            self.raise_layout_error("Missing intersection symbol", ridx, right)
        break
    else:
      bottom = self._height
    
    return left, top, right, bottom, cell_widget, cell_label
  
  def apply (self, parent, in_=None):
    if in_ is None:
      in_ = parent
    
    for r,w in enumerate(self.row_weights):
      if w is not None:
        in_.rowconfigure(r, weight=w)
    
    for c,w in enumerate(self.column_weights):
      if w is not None:
        in_.columnconfigure(c, weight=w)
    
    for cell in self.cells:
      kwargs = cell.copy()
      label = kwargs.pop('label')
      widget = kwargs.pop('widget')
      if widget == "":
        widget = Label(parent, text=label)
      else:
        if label is not None:
          if label == "":
            frame = Frame(parent)
          else:
            frame = LabelFrame(parent, text=label)
          setattr(parent, widget, frame)
        widget = getattr(parent, widget)
      widget.grid(
        **kwargs,
        sticky='news',
        in_=in_
      )
  
  def raise_layout_error (self, msg, ridx, cidx):
    raise LayoutError(
      msg,
      self._topremoved+ridx,
      self._leftremoved+cidx
    )
  
  def _debug_print_labels (self):
    print("\n".join(["".join([str(x) if x is not None else " " for x in row]) for row in self._labels]))

# Example GridLayout
"""
pnlInput | (
  {Automatic predictor}
  [2,3]
  lblNorm & optNorm >
  lblNet  & etyNet & btnNet >
  btnLoad >
  -
  {Suggested labels}
  pnlLabels
)
"""
