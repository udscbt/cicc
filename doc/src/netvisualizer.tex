\documentclass[12pt]{article}

\usepackage[
  a4paper,
  bindingoffset=0cm,
  left=3cm,
  right=2.5cm,
  top=3cm,
  bottom=3cm,%
  footskip=.25in,
  head height = 14pt]{geometry}

\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{url}
\usepackage{hyperref}

\newcommand\terminal[1]{%
  \colorbox{black!90!white}{%
    \textcolor{white!90!black}{%
      \texttt{> #1}%
    }%
  }%
}

\title{CICC Network Visualization Tool (\texttt{cicc-nvt})}
\author{Fabio Paini}

\begin{document}
\maketitle

\begin{figure}[!ht]
  \centering
  \includegraphics[width=\textwidth]{images/netvisualizer_running.png}
\end{figure}

\clearpage

\tableofcontents

\section{Installation}
The following steps explain how to install the CICC module as a Python package
and use it to run the CICC Network Visualization Tool.
\subsection{Get source code}
All the source code is hosted as a repository on GitLab at
\url{https://gitlab.com/udscbt/cicc} and can be retrieved either by using
\texttt{git} or by downloading a \texttt{.zip} version and extracting it.

\subsubsection{With \texttt{git}}
  \begin{itemize}
    \item SSH: \terminal{git clone git@gitlab.com:udscbt/cicc.git}
    \item HTTP: \terminal{git clone https://gitlab.com/udscbt/cicc.git}
  \end{itemize}

\subsubsection{As \texttt{.zip} file}
The full \texttt{.zip} version can be downloaded from
\url{https://gitlab.com/udscbt/cicc/-/archive/master/cicc-master.zip} and then
extracted to a temporary directory.
Instead of the whole folder, only the necessary files can be downloaded:
\begin{itemize}
  \item
    The Python wheel containing the CICC package:
    \url{https://gitlab.com/udscbt/cicc/raw/master/dist/cicc-0.1-py3-none-any.whl}
  \item
    The \texttt{netvisualizer} folder:
      \url{https://gitlab.com/udscbt/cicc/-/archive/master/cicc-master.zip?path=tools%2Fcicc-nvt}
\end{itemize}

\subsection{Install CICC}
The CICC package can be installed either directly from the source code (if the
whole repository was downloaded) or by using the pre-packaged wheel file.
In both cases, all the required dependencies will also be installed.
\subsubsection{From source}
Simply run the command

\terminal{python setup.py install}

\subsubsection{With Python wheel}
Run the command

\terminal{pip install <wheel file>}

\noindent with the correct path for
\texttt{<wheel file>} (when the full source code is downloaded, it is located
at \texttt{dist/cicc-0.1-py3-none-any.whl}; note that the file name can change
with future versions).

\subsection{Install Graphviz}
To display the neural network structure \texttt{cicc-nvt} internally calls
executables from the Graphviz graph visualization suite, it is therefore
necessary to install them before using this application.
The necessary software can be downloaded from
\url{https://www.graphviz.org/download/}.

\subsection{Test the installation}
Every dependency should now be installed in the system.
The Network Visualization Tool can be started by passing the path to the
\texttt{cicc-nvt} directory to the Python interpreter.
From the repository folder, the correct command is:

\terminal{python tools/cicc-nvt}

\section{Interface}
\subsection{Main window}
\begin{figure}[!h]
  \centering
  \includegraphics[width=\textwidth]{images/netvisualizer_mainwindow.png}
  \caption{Main window}
\end{figure}
This is the main application window, divided in a viewport on the left and a
control panel on the right.

The control panel contains a few buttons used to:
\begin{itemize}
  \item move the neural network to the center of the viewport
  \item automatically reposition the layers of the network using one of the
    available \href{https://www.graphviz.org/}{Graphviz layouts}
  \item load a neural network from an HDF5 model containing a Keras model
\end{itemize}

The viewport shows the loaded neural network with all its layers
and connections.
Each block represents a layer in the network with their respective output
tensors.
The layers can be dragged around and more informations can be displayed
by double clicking on it (see below).

By double clicking on a tensor, instead, its real-time value is displayed
(see below).

A tensor bordered in green is an input for the neural network, while a yellow
border indicates that it is an output.

\subsection{Layer information}
\begin{figure}[!h]
  \centering
  \includegraphics[height=0.4\textheight]{images/netvisualizer_layerinfo.png}
  \caption{Layer information window}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[width=0.5\textwidth]{images/netvisualizer_inputinfo.png}
  \caption{Input layer information window}
\end{figure}
By double clicking on the name of a layer in the viewport, a window containing
information about it is shown, varying a bit depending on the type of layer.

The displayed information are:
\begin{enumerate}
  \item Layer type
  \item Main parameters
  \item Tensors which are inputs for the layer (with their shape; can be
    displayed by double clicking)
  \item Tensors which are outputs for the layer (with their shape; can be
    displayed by double clicking)
  \item Shape of the weights tensors (can be displayed by double clicking)
  \item Normalization algorithm to be applied to the input data before being
    sent to the neural network (only for layers of type \textit{Input})
\end{enumerate}

The available normalization algorithms are:
\begin{itemize}
  \item \textit{stretch}: resizes the input image to the size of the input
    tensor without keeping the aspect ratio
  \item \textit{extend}: resizes the input image to completely cover the input
    tensor while keeping the aspect ratio; if the resultant image is too big,
    it is cropped to fit the input tensor
  \item \textit{fit}: resizes the input image to fit the input tensor without
    need of cropping while keeping the aspect ratio; the remaining space is
    filled with zeros
  \item \textit{crop face + <normalizer>}: detects faces from the input images
    and applies the above algorithms to the single cropped faces separately
  \item \textit{Paini}: uses the normalization algorithm described in
    \href{http://hdl.handle.net/10589/146282}{``Deep Learning for real-time emotion recognition from face images''};
    it uses facial landmarks, so to use it it is necessary to download the file
    \href{https://github.com/davisking/dlib-models/raw/master/shape\_predictor\_68\_face\_landmarks.dat.bz2}{\texttt{shape\_predictor\_68\_face\_landmarks.dat.bz2}}
    and extract it in the folder \texttt{cicc/normalize} (in the location where
    the CICC package was installed)
  \item \textit{none}: leaves the input image as is; will not work if the size
    of the input image is different from the size of the input tensor
\end{itemize}

\subsection{Get input}
\begin{figure}[!h]
  \centering
  \includegraphics[height=0.4\textheight]{images/netvisualizer_input.png}
  \caption{Input window}
\end{figure}

This window is opened by double clicking on a tensor that is an input of the
network (the ones bordered in green).

From the \textit{Image} menu it is possible to select the input source: either
a static image saved in the filesystem or a real-time acquisition from the
webcam.

The panel on the left shows the original image, while the one in the right
displays the input tensor given to the neural network.

\subsection{Display tensor value}
\begin{figure}[!h]
  \centering
  \includegraphics[height=0.4\textheight]{images/netvisualizer_output.png}
  \caption{Output window}
\end{figure}

This window is opened by double clicking on a tensor that is the output of
any layer in the network (all except for the input ones).

It shows the activation map of the layer one as a 2D image (thus only one filter
for a convolutional layer or one slice for a 3D output) as well as its minimum
and maximum value; the displayed filter/slice can be changed by clicking on the
arrows on the bottom of the window.

It can also show 1-dimensional outputs, but it is rarely useful.

\subsection{Display weights tensor}
\begin{figure}[!h]
  \centering
  \includegraphics[width=0.5\textwidth]{images/netvisualizer_weights.png}
  \caption{Weights window}
\end{figure}

This window is opened by double clicking on the weights row inside the
layer information window.

It is similar to the generic tensor display described above, but since the
kernel of a 2D convolution is a 3D tensor, it also allows to move through that
third dimension through the arrows on the bottom.
The arrows on the sides change the filter/neuron shown instead.

\end{document}
