import setuptools

setuptools.setup(
  name='cicc',
  version='0.1',
  author="Fabio Paini",
  author_email="udscbt.dev@gmail.com",
  description="Complete Interface for Complex Classifications",
  url="https://gitlab.com/udscbt/cicc",
  packages=setuptools.find_packages(),
  install_requires=[
    "pillow",
    "opencv-python",
    "dlib",
    "keras",
    "numpy",
    "scipy",
    "pydot", # netvisualizer
  ],
  classifiers=[
    "Programming Language :: Python :: 3",
    "Operating System :: OS Independent",
  ],
  include_package_data=True,
)
