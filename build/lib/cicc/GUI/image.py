import cv2
import numpy as np
from tkinter import *
from tkinter.ttk import *
from PIL import Image, ImageTk
from . import ICONS
from tkinter.filedialog import askopenfilename, asksaveasfilename

class ImagePanel (Frame):
  voidimage = np.zeros((500,500, 3), dtype=np.uint8)
  
  def __init__ (self, *args, voidimage=None, **kwargs):
    if voidimage is not None:
      self.voidimage = voidimage
    
    Frame.__init__(self, *args, **kwargs)
    
    self.callbacks = []
    self.showcallbacks = []
    
    self.box = None
    self.changingbox = False
    
    self.columnconfigure(0, weight=1)
    self.rowconfigure(0, weight=1)
    
    self.imagepanel = Label(self, borderwidth=0, anchor=CENTER)
    self.imagepanel.image = None
    self.imagepanel.grid(row=0, column=0, sticky='news')
    
    self.imageyscroll = Scrollbar(self, orient=VERTICAL, command=self.moveimgy)
    self.imageyscroll.grid(row=0, column=1, sticky='ns')
    
    self.imagexscroll = Scrollbar(self, orient=HORIZONTAL, command=self.moveimgx)
    self.imagexscroll.grid(row=1, column=0, sticky='ew')
    
    self.boxwidth = IntVar()
    self.boxheight = IntVar()
    self.boxfull = BooleanVar()
    
    self.zoompanel = Frame(self)
    self.zoompanel.grid(row=2, column=0, columnspan=2)
    self.zoompanel.columnconfigure(0, weight=2)
    self.zoompanel.columnconfigure(1, weight=2)
    self.zoompanel.columnconfigure(2, weight=1)
    self.zoompanel.columnconfigure(3, weight=1)
    
    Label(self.zoompanel, text="Width: ").grid(row=0, column=0)
    self.widthselect = Spinbox(self.zoompanel, from_=0, textvariable=self.boxwidth)
    self.widthselect.grid(row=1, column=0)
    
    Label(self.zoompanel, text="Height: ").grid(row=0, column=1)
    self.heightselect = Spinbox(self.zoompanel, from_=0, textvariable=self.boxheight)
    self.heightselect.grid(row=1, column=1)
    
    self.checkfull = Checkbutton(
      self.zoompanel,
      text="Full image",
      command=self.togglefull
    )
    self.checkfull.grid(row=0, column=2, columnspan=2)
    
    Button(
      self.zoompanel,
      text="ZOOM -",
      command=self.zoom_m
    ).grid(row=1, column=2)
    Button(
      self.zoompanel,
      text="ZOOM +",
      command=self.zoom_p
    ).grid(row=1, column=3)
    
    self.setimg(None)
    self.togglefull()
    self.togglefull()
    
    self.boxwidth.trace('w', self.changeboxsize)
    self.boxheight.trace('w', self.changeboxsize)
    self.bind('<Configure>', self.resized)
    self.imagepanel.bind('<ButtonRelease-3>', self.saveimage)
  
  def saveimage (self, event=None):
    path = asksaveasfilename(
      parent=self,
    )
    if path != '':
      cv2.imwrite(path, self.image)
  
  def togglefull (self):
    if self.box is None:
      self.setpartial()
    else:
      self.setfull()
  
  def setfull (self, updateimg=True):
    self.widthselect['state'] = 'disabled'
    self.heightselect['state'] = 'disabled'
    self.checkfull.state(['selected'])
    if updateimg:
      self.showimg(None)
  
  def setpartial (self, updateimg=True):
    self.widthselect['state'] = 'normal'
    self.heightselect['state'] = 'normal'
    self.checkfull.state(['!selected'])
    if updateimg:
      h,w = self.image.shape[:2]
      box = (0,0,w,h)
      self.showimg(box)
  
  def resized (self, e):
    self.showimg(self.box)
  
  def setimg (self, img, box=None):
    if img is None:
      img = self.voidimage
    else:
      img = img.copy()
    h,w = img.shape[:2]
    self.widthselect.config(to=w, increment=w//10)
    self.heightselect.config(to=h, increment=h//10)
    
    self.image=img
    
    for callback in self.callbacks:
      self.image = callback(self)
    
    if box is None:
      self.setfull()
    else:
      self.showimg(box)
  
  def moveimgx (self, cmd, value, unit=None):
    value = float(value)
    if cmd == 'moveto':
      self.moveimg(value, None)
    elif cmd == 'scroll':
      x,X = self.imagexscroll.get()
      if unit == 'units':
        self.moveimg(x+0.1*value, None)
      elif unit == 'pages':
        self.moveimg(x+0.2*value, None)
  
  def moveimgy (self, cmd, value, unit=None):
    value = float(value)
    if cmd == 'moveto':
      self.moveimg(None, value)
    elif cmd == 'scroll':
      y,Y = self.imageyscroll.get()
      if unit == 'units':
        self.moveimg(None, y+0.1*value)
      elif unit == 'pages':
        self.moveimg(None, y+0.2*value)
  
  def moveimg (self, newx, newy):
    if self.box is None:
      return
    
    H,W = self.image.shape[:2]
    x,y,X,Y = self.box
    w,h = X-x,Y-y
    if newx is not None:
      x = newx*W
    if newy is not None:
      y = newy*H
    
    if x < 0:
      x = 0
    if y < 0:
      y = 0
    
    X = x+w
    Y = y+h
    
    if X > W:
      X = W
      x = X-w
    
    if Y > H:
      Y = H
      y = Y-h
    
    box = (x,y,X,Y)
    if box != self.box:
      self.showimg(box)
  
  def changeboxsize (self, *args):
    self.setWH(self.boxwidth.get(), self.boxheight.get())
    # ~ box = self.box
    # ~ if box is None:
      # ~ box = 0,0,self.image.shape[1],self.image.shape[0]
    # ~ x,y,X,Y = box
    # ~ X_ = x+self.boxwidth.get()
    # ~ Y_ = y+self.boxheight.get()
    # ~ if (X,Y) != (X_,Y_):
      # ~ self.showimg((x,y,X_,Y_))
  
  def panelscale (self):
    H,W = self.image.shape[:2]
    x,y,X,Y = self.box or (0,0,W,H)
    scale = 1
    if self.imagepanel.image is not None:
      winW = self.imagepanel.winfo_width()
      winH = self.imagepanel.winfo_height()
      w,h = X-x,Y-y
      scaleX = winW/w
      scaleY = winH/h
      scale = min(scaleX, scaleY)
    return scale
  
  def widgetcoords (self, points, scale=None):
    winW = self.imagepanel.winfo_width()
    winH = self.imagepanel.winfo_height()
    H,W = self.image.shape[:2]
    x,y,X,Y = self.box or (0,0,W,H)
    w,h = X-x,Y-y
    if scale is None:
      scale = self.panelscale()
    
    points = np.array(points).reshape(-1,2)
    points = points - (x,y)
    points = points * scale
    points = points + (winW-w*scale, winH-h*scale)/2
    return [tuple(p) for p in points.astype(int)]
  
  def panelcoords (self, points, scale=None):
    H,W = self.image.shape[:2]
    x,y,X,Y = self.box or (0,0,W,H)
    if scale is None:
      scale = self.panelscale()
    
    points = np.array(points).reshape(-1,2)
    points = points - (x,y)
    points = points * scale
    return [tuple(p) for p in points.astype(int)]
  
  def imagecoords (self, points, scale=None):
    winW = self.imagepanel.winfo_width()
    winH = self.imagepanel.winfo_height()
    H,W = self.image.shape[:2]
    x,y,X,Y = self.box or (0,0,W,H)
    w,h = X-x,Y-y
    if scale is None:
      scale = self.panelscale()
    
    points = np.array(points).reshape(-1,2)
    points = points / scale
    points = points + (x,y)
    points = points - ((winW/scale-w)/2, (winH/scale-h)/2)
    return [tuple(p) for p in points.astype(int)]
  
  def setWH (self, w, h):
    H,W = self.image.shape[:2]
    x,y,X,Y = self.box or (0,0,W,H)
    x_,y_ = (X+x)/2,(Y+y)/2
    x,X = x_-w/2, x_+w/2
    y,Y = y_-h/2, y_+h/2
    self.showimg((x,y,X,Y))
  
  def zoom (self, value):
    H,W = self.image.shape[:2]
    x,y,X,Y = self.box or (0,0,W,H)
    w,h = X-x,Y-y
    w -= value*W
    h -= value*H
    self.setWH(w,h)
  
  def zoom_m (self):
    return self.zoom(-0.1)
  
  def zoom_p (self):
    return self.zoom(+0.1)
  
  def showimg (self, box=None):
    H,W = self.image.shape[:2]
    winW = self.imagepanel.winfo_width()
    winH = self.imagepanel.winfo_width()
    
    if self.changingbox:
      return
    self.changingbox = True
    if box is None:
      x,y,X,Y = 0,0,W,H
      w,h = W,H
      self.box = None
      self.setfull(False)
    else:
      x,y,X,Y = map(int, box)
      if x < 0:
        x = 0
      if y < 0:
        y = 0
      if X > W:
        X = W
      if Y > H:
        Y = H
      w,h = X-x,Y-y
      if w < 1 or h < 1:
        self.changingbox = False
        return self.showimg(self.box)
      self.box = (x,y,X,Y)
      self.setpartial(False)
    self.boxwidth.set(w)
    self.boxheight.set(h)
    img = self.image[y:Y,x:X]
    scale = self.panelscale()
    if scale != 1:
      neww,newh = (int(w*scale), int(h*scale))
      if neww == 0:
        neww = 1
      if newh == 0:
        newh = 1
      img = cv2.resize(
        img,
        (neww,newh),
        interpolation = cv2.INTER_NEAREST
      )
    
    for callback in self.showcallbacks:
      img = callback(self, img, scale)
    
    if img.ndim == 2 or img.shape[2] == 1:
      img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    else:
      img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = Image.fromarray(img)
    img = ImageTk.PhotoImage(img)
    self.imagepanel.configure(image=img)
    self.imagepanel.image = img
    
    self.boxwidth.set(w)
    self.boxheight.set(h)
    self.imagexscroll.set(x/W, X/W)
    self.imageyscroll.set(y/H, Y/H)
    
    self.changingbox = False
  
  def redraw (self):
    self.showimg(self.box)
  
  def add_augmentation (self, callback, onShow=False):
    if onShow:
      self.showcallbacks.append(callback)
    else:
      self.callbacks.append(callback)

class MultiImagePanel (ImagePanel):
  def __init__ (self, *args, **kwargs):
    self.index = IntVar()
    
    ImagePanel.__init__(self, *args, **kwargs)
    
    self.navpanel = Frame(self)
    self.navpanel.grid(row=3, column=0, columnspan=2, sticky='news')
    
    self.navpanel.columnconfigure(0, weight=1)
    self.navpanel.columnconfigure(2, weight=1)
    Button(
      self.navpanel,
      text='<',
      command=self.prev_image
    ).grid(row=1, column=0, sticky='news')
    self.lblImgs = Label(
      self.navpanel,
      text='0/0'
    )
    self.lblImgs.grid(row=1, column=1, sticky='ns')
    Button(
      self.navpanel,
      text='>',
      command=self.next_image
    ).grid(row=1, column=2, sticky='news')
    
    
    self.clear_images()
    self.index.trace('w', self.change_image)
  
  def clear_images (self, keep_index=False):
    self.images = []
    self.labels = []
    if not keep_index:
      self.index.set(-1)
  
  def add_image (self, img, update=True, show=False):
    try:
      lbl, img = img
    except (TypeError, ValueError):
      lbl = None
    self.labels.append(lbl)
    self.images.append(img)
    if show:
      self.index.set(len(self.images)-1)
    elif self.index.get() == -1:
      self.index.set(0)
    if update:
      self.change_image()
  
  def change_image (self, *args, **kwargs):
    index = self.index.get()
    if index == -1 or len(self.images) == 0:
      text = "No image"
      image = None
    else:
      text = (
        "{}/{}" if self.labels[index] is None else "[{}/{}] {}"
      ).format(
        self.index.get()+1,
        len(self.images),
        self.labels[index]
      )
      image = self.images[self.index.get()]
    self.lblImgs['text'] = text
    self.setimg(image)
  
  def next_image (self):
    if self.index.get() == -1:
      return
    index = self.index.get()+1
    if index == len(self.images):
      index = 0
    self.index.set(index)
  
  def prev_image (self):
    if self.index.get() == -1:
      return
    index = self.index.get()-1
    if index == -1:
      index = len(self.images)-1
    self.index.set(index)
  
  def get_index (self):
    return self.index.get()

def space_converter (f):
  def _f (self, points, *args, alwayslist=False, **kwargs):
    points = np.array(points).reshape(-1,2)
    ret = f(self, points, *args, **kwargs)
    ret = [tuple(p) for p in ret]
    if len(points) != 1 or alwayslist:
      return ret
    else:
      return ret[0]
  _f.__name__ = f.__name__
  _f.__doc__ = f.__doc__
  return _f

def compound_space_converter (*funcs):
  def decorator (f):
    def _f (self, points, *args, alwayslist=False, **kwargs):
      ret = points
      for f in funcs:
        ret = f(self, ret, *args, alwayslist=alwayslist, **kwargs)
      return ret
    _f.__name__ = f.__name__
    _f.__doc__ = f.__doc__
    return _f
  return decorator

class ImagePanelEx (Frame):
  def __init__ (self, *args, **kwargs):    
    Frame.__init__(self, *args, **kwargs)
    
    self.augmentations = []
    self.showaugmentations = []
    
    self.callbacks = []
    self.showcallbacks = []
    
    self.position = (0,0)
    self.image = None
    self.imagepos = None
    
    self.columnconfigure(0, weight=1)
    self.rowconfigure(0, weight=1)
    
    self.imagepanel = Canvas(self)
    self.imagepanel.grid(row=0, column=0, sticky='news')
    
    self.imageyscroll = Scrollbar(
      self,
      orient=VERTICAL,
      command=self.moveviewy
    )
    self.imageyscroll.grid(row=0, column=1, sticky='ns')
    
    self.imagexscroll = Scrollbar(
      self,
      orient=HORIZONTAL,
      command=self.moveviewx
    )
    self.imagexscroll.grid(row=1, column=0, sticky='ew')
    
    self.zoombtn = Label(
      self,
      border=0,
      anchor=CENTER
    )
    self.zoombtn.grid(row=1, column=1, sticky='news')
    
    self.zoombtn.image = PhotoImage(file=ICONS['ICO_ZOOM'])
    self.zoombtn['image'] = self.zoombtn.image
    
    self.zoom = DoubleVar()
    self.zoom.set(1)
    self.zoom.trace('w', self.changezoom)
    
    zooms = 2.0**np.arange(-3,3)
    self.zoombtn.menu = Menu(self, tearoff=False)
    self.zoombtn.bind('<1>', lambda e: (
      self.zoombtn.menu.post(e.x_root, e.y_root)
    ))
    
    for zoom in zooms:
      self.zoombtn.menu.add_radiobutton(
        label="{}%".format(zoom*100),
        variable=self.zoom,
        value=zoom
      )
    
    self.imagepanel.bind('<Configure>', self.resized)
  
  def setimg (self, img, pos=None):
    self.image = img
    if pos is not None:
      self.imagepos = pos
    elif self.imagepos is None:
      self.imagepos = (0,0)
    
    for callback in self.augmentations:
      self.image = callback(self)
    
    for callback in self.callbacks:
      callback(self)
    
    self.showimg()
  
  def changezoom (self, *args):
    self.showimg()
  
  def resized (self, e):
    self.showimg()
  
  def moveviewx (self, cmd, value, unit=None):
    return self.scroll(cmd, value, unit, 0)
  
  def moveviewy (self, cmd, value, unit=None):
    return self.scroll(cmd, value, unit, 1)
  
  def scroll (self, cmd, value, unit, axis):
    if self.image is None:
      return
    
    winSize = np.array([
      self.imagepanel.winfo_width(),
      self.imagepanel.winfo_height()
    ])
    imgSize = self.image.size
    
    value = float(value)
    if cmd == 'moveto':
      oldvalues = [self.imagexscroll.get(), self.imageyscroll.get()][axis]
      range = oldvalues[1]-oldvalues[0]
      if value < 0:
        value = 0
      if value > 1-range:
        value = 1-range
      img_zero = [0,0]
      img_zero[axis] = value*imgSize[axis]
      global_newzero = self.image2global(img_zero)
      global_oldzero = self.canvas2global((0,0))
      diff = [0,0]
      diff[axis] = global_newzero[axis]-global_oldzero[axis]
      self.position = np.array(self.position)+diff
    elif cmd == 'scroll':
      if unit == 'units':
        step = 10
      elif unit == 'pages':
        step = 100
      diff = [0,0]
      diff[axis] = value*step
      local_newpos = winSize/2+diff
      self.position = self.canvas2global(local_newpos)
    
    self.showimg()
  
  @space_converter
  def global2canvas (self, points):
    winSize = np.array([
      self.imagepanel.winfo_width(),
      self.imagepanel.winfo_height()
    ])
    
    global_diff = points-self.position
    screen_diff = self.zoom.get()*global_diff
    screen_pos = screen_diff+winSize/2
    
    return screen_pos
  
  @space_converter
  def canvas2global (self, points):
    winSize = np.array([
      self.imagepanel.winfo_width(),
      self.imagepanel.winfo_height()
    ])
    
    screen_diff = points-winSize/2
    global_diff = screen_diff/self.zoom.get()
    global_pos  = global_diff+self.position
    
    return global_pos
  
  @space_converter
  def global2image (self, points):
    if self.image is None:
      return points
    return points-self.imagepos
  
  @space_converter
  def image2global (self, points):
    if self.image is None:
      return points
    return points+self.imagepos
  
  @compound_space_converter (canvas2global, global2image)
  def canvas2image ( IGNORED ):
    pass
  
  @compound_space_converter (image2global, global2canvas)
  def image2canvas ( IGNORED ):
    pass
  
  def resetposition (self):
    if self.image is None:
      return
    
    W,H = self.image.size
    x,y = self.imagepos
    self.position = x+W/2,y+H/2
    self.showimg()
  
  def showimg (self):
    self.imagepanel.delete(ALL)
    if self.image is None:
      return
    
    img = self.image
    for callback in self.showaugmentations:
      img = callback(self)
    
    W,H = self.image.size
    winW = self.imagepanel.winfo_width()
    winH = self.imagepanel.winfo_height()
    
    zoom = self.zoom.get()
    
    if zoom != 1:
      img = self.image.resize((int(zoom*W), int(zoom*H)))
    
    self.imagepanel.img = img = ImageTk.PhotoImage(img)
    
    self.imagepanel.create_image(
      self.image2canvas((0,0)),
      image=img,
      anchor='nw'
    )
    
    minx,miny = self.canvas2image((0,0))
    maxx,maxy = self.canvas2image((winW,winH))
    minx,maxx = minx/W,maxx/W
    miny,maxy = miny/H,maxy/H
    
    self.imagexscroll.set(minx, maxx)
    self.imageyscroll.set(miny, maxy)
    
    for callback in self.showcallbacks:
      callback(self)
  
  def add_augmentation (self, callback, onShow=False):
    if onShow:
      self.showaugmentations.append(callback)
    else:
      self.augmentations.append(callback)
  
  def add_callback (self, callback, onShow=False):
    if onShow:
      self.showcallbacks.append(callback)
    else:
      self.callbacks.append(callback)
