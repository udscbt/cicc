import os
from cicc.workflow import Workflow, TRAINED_FULL
import warnings
import keras
import keras.backend as K
import numpy as np
from cicc.keras_patch import make_multidim
import cicc.keras_patch as keras_patch
import gc
from cicc.table import pretty_table


def clear_memory():
    while gc.collect() > 0:
        pass


@make_multidim
def TP_per_class(y_true, y_pred):
    correct_samples = K.equal(K.argmax(y_true), K.argmax(y_pred))
    TP_per_class = K.sum(K.prod([correct_samples, y_true]), 0)
    return TP_per_class


@make_multidim
def true_per_class(y_true, y_pred):
    return K.sum(y_true, 0)


class ConfusionMatrix:

    def __init__(self, matrix, labels=None, borders_included=False):
        matrix = np.array(matrix, dtype=float)
        if np.all(matrix % 1 == 0):
            matrix = matrix.astype(int)

        if matrix.ndim != 2:
            raise ValueError("Not a matrix")
        elif matrix.shape[0] != matrix.shape[1]:
            raise ValueError("Not a square matrix")

        num_classes = matrix.shape[0]
        if borders_included:
            num_classes -= 1

        if labels is None:
            labels = []
        self.labels = [
            i if i >= len(labels) else labels[i] for i in range(num_classes)
        ]

        if not borders_included:
            matrix = np.concatenate([matrix, matrix.sum(0)[np.newaxis]], 0)
            matrix = np.concatenate([matrix, matrix.sum(1)[:, np.newaxis]], 1)

        self.matrix = matrix

    def indexing(aggregator):
        def decorator(method):

          def wrapper(self, cls=None):
              if cls is None:
                  cls = [idx for idx,_ in enumerate(self.labels)]
              if isinstance(cls, list) or isinstance(cls, tuple):
                  return aggregator(
                      [wrapper(self, idx) for idx in cls])
              if isinstance(cls, int):
                  return method(self, cls)
              else:
                  idx = self.labels.index(cls)
                  return method(self, idx)

          wrapper.__name__ = method.__name__
          wrapper.__doc__ = method.__doc__
          return wrapper
        return decorator

    def short_zero(method):

        def wrapper(self, idx):
            if self.matrix[idx, idx] == 0:
                return 0
            else:
                return method(self, idx)

        wrapper.__name__ = method.__name__
        wrapper.__doc__ = method.__doc__
        return wrapper
    
    def relabel(self, relabel_def):
        if isinstance(relabel_def, dict):
            labels = list(relabel_def)
            conversion = [None for i in self.labels]
            other = None
            for k,v in relabel_def.items():
                if not isinstance(v, list):
                    v = [v]
                for label in v:
                    if label is None:
                        if other is None:
                            other = labels.index(k)
                        else:
                            raise ValueError("Only one label can be defined to catch all unassigned labels")
                    else:
                        if not isinstance(label, int) or label not in self.labels:
                            label = self.labels.index(label)
                        conversion[label] = labels.index(k)
            if other is not None:
                while None in conversion:
                    idx = conversion.index(None)
                    conversion[idx] = other
        elif isinstance(relabel_def, list):
            if len(relabel_def) != len(self.labels):
                raise ValueError("If relabel_def is provided as a list, it should contain the new label for each old label")
            labels = list(set(relabel_def))
            conversion = [labels.index(k) for k in relabel_def]
        else:
            raise ValueError("relabel_def must be either a dict or a list")
        
        N = len(labels)
        newmat = np.zeros((N,N))
        for i,l in enumerate(self.labels):
            for j,l in enumerate(self.labels):
                newi = conversion[i]
                newj = conversion[j]
                if newi is None or newj is None:
                    continue
                newmat[conversion[i],conversion[j]] += self.matrix[i,j]
        return ConfusionMatrix(newmat, labels)
        
        
    
    def focus(self, labels):
        if not isinstance(labels, list):
            labels = [labels]
        idx = [
            lbl if isinstance(lbl, int) else self.labels.index(lbl)
            for lbl in labels
        ] + [-1]
        newmat = self.matrix[idx][:, idx]

        bottom = newmat[-1] - newmat[:-1].sum(0)
        newmat = np.concatenate([newmat[:-1], bottom[np.newaxis], newmat[[-1]]],
                                0)

        right = newmat[:, -1] - newmat[:, :-1].sum(1)
        newmat = np.concatenate(
            [newmat[:, :-1], right[:, np.newaxis], newmat[:, [-1]]], 1)

        return ConfusionMatrix(newmat, labels + ["<other>"], borders_included=True)

    def accuracy(self, cls=None):
        matrix = self.matrix
        if cls is None:
            return matrix[:-1, :-1].trace() / self.total()
        else:
            return self.focus(cls).accuracy()

    @indexing(np.mean)
    @short_zero
    def precision(self, idx):
        return self.matrix[idx, idx] / self.total_predictions(idx)

    @indexing(np.mean)
    @short_zero
    def recall(self, idx):
        return self.matrix[idx, idx] / self.total_examples(idx)

    sensitivity = recall

    @indexing(np.mean)
    def specificity(self, idx):
        if len(self.labels) != 2:
            return self.focus(idx).precision('<other>')
        else:
            return self.precision(1 - idx)

    @indexing(np.mean)
    @short_zero
    def F1_score(self, idx):
        return (2 * self.matrix[idx, idx] /
                (self.total_predictions(idx) + self.total_examples(idx)))

    @indexing(np.sum)
    def total_predictions (self, idx):
        return self.matrix[-1, idx]
    
    @indexing(np.sum)
    def total_examples (self, idx):
        return self.matrix[idx,-1]
    
    def total (self):
      return self.matrix[-1,-1]

    def to_table(self):
        labels = self.labels + [""]
        table = [
            [""] + labels,
            *([lbl, *map(str, row)] for lbl, row in zip(labels, self.matrix))
        ]
        return table

    def __str__(self):
        return pretty_table(self.to_table())

    @staticmethod
    def load(path):
        with open(path, "r") as f:
            content = f.read()
        table = np.array([row.split(",") for row in content.split("\n")])
        labels = list(table[0, 1:-1])
        return ConfusionMatrix(table[1:, 1:], labels, borders_included=True)

    def save(self, path):
        with open(path, 'w') as f:
            f.write("\n".join([",".join(row) for row in self.to_table()]))


class Evaluator:
    metrics = [
        'accuracy'
        'precision',
        'recall',
    ]

    def CONFUSION_OFFLINE(self, model):
        matrix = np.zeros((self.getOutputSize(), self.getOutputSize()))
        for imgs, lbls in self:
            y_true = lbls
            y_pred = model.predict(imgs)
            for target, prediction in zip(y_true.argmax(1), y_pred.argmax(1)):
                matrix[target, prediction] += 1
        return (matrix, False)

    def CONFUSION_MULTIPLE_METRICS(self, model):
        metrics = keras_patch.confusion_matrix_metrics(self.getOutputSize())

        model.compile(
            optimizer='adadelta',  # ignored
            loss='categorical_crossentropy',  # ignored
            metrics=metrics)
        model.evaluate_generator(self)
        matrix = np.array([metric.score for metric in metrics]).reshape(
            self.getOutputSize(), self.getOutputSize())

        for metric in metrics:
            del metric
            clear_memory()
        return (matrix, False)

    def CONFUSION_SINGLE_METRIC(self, model):
        metric = keras_patch.ConfusionMatrix(self.getOutputSize())
        model.compile(
            optimizer='adadelta',  # ignored
            loss='categorical_crossentropy',  # ignored
            metrics=[metric])
        model.evaluate_generator(self)
        return (metric.score, False)

    # Make Evaluator a template-class
    def __new__(cls, GeneratorType, *args, **kwargs):
        # Create specialized class
        class Evaluator(cls, GeneratorType):
            # Avoid infinite loop
            def __new__(cls, *args, **kwargs):
                return object.__new__(cls)

            # Call constructor of base class and generator class
            def __init__(self, GeneratorType, *args, verbose=False, **kwargs):
                GeneratorType.__init__(self, verbose)
                cls.__init__(self, *args, verbose=verbose, **kwargs)

        # Return created object
        return Evaluator.__new__(Evaluator, *args, **kwargs)

    def __init__(self,
                 verbose=False,
                 confusion_algorithm=CONFUSION_SINGLE_METRIC):
        self.confusion_algorithm = confusion_algorithm

    def init_evalfile(self, filename, overwrite=False):
        if os.path.exists(filename) and not overwrite:
            with open(filename, 'r') as f:
                content = f.read()
                header = [x for x in content.split("\n") if x.startswith("#")]
                data = [
                    x for x in content.split("\n")
                    if not x.startswith("#") and x != ""
                ]
            evaluations = {}
            for row in data:
                row = row.split(",")
                k = (row[3])
                evaluations[k] = row
            return evaluations
        else:
            with open(filename, 'w') as f:
                fields = [
                    'Test', 'Try', '#', 'Path', 'global_loss', 'global_acc'
                ]
                counts = []
                for cls, name in enumerate(self.getLabels()):
                    fields += [name + '_loss', name + '_acc']
                    counts.append((name, self.getNumSamples(cls)))
                counts.append(('TOTAL', self.getNumSamples()))

                f.write("# {}\n".format(",".join(fields)))
                f.write("# Dataset: {}\n".format(self.getPath()))
                f.write("".join([
                    "# {} {}\n".format(name, samples)
                    for name, samples in counts
                ]))
            return {}

    def computeConfusionMatrix(self, model):
        self.print("Setting generator parameters to fit model")
        self.setInputShape(model.input_shape)
        _batch = self.getBatchSize()
        self.setBatchSize(min(model.maxbatch, _batch))
        try:
            keras_model = model.get_model()
            matrix, border = self.confusion_algorithm(self, keras_model)
            del keras_model
            clear_memory()

            return ConfusionMatrix(
                matrix, self.getLabels(), borders_included=border)
        finally:
            self.setBatchSize(_batch)

    def computeConfusionMatrices(self,
                                 workflow,
                                 to_files=None,
                                 overwrite=False,
                                 output=None,
                                 check_correct=None):
        if check_correct is None:
          def check_correct (_): True
        if output is None:
            matrices = {}
        else:
            matrices = output
        for test in workflow:
            self.print("= {} =".format(test['name']))

            if not os.path.exists(test.path):
                self.print("Test not yet trained")
                continue

            for try_ in test:
                self.print("== {}.{} ==".format(test['name'], try_['name']))

                for model in try_:
                    self.print("=== Model {} ===".format(model.name))

                    if model.is_trained() < TRAINED_FULL:
                        self.print("Model not yet trained")
                        continue

                    if to_files is not None:
                        output_path = os.path.join(to_files,
                                                   model.name + ".csv")

                        if not overwrite and os.path.exists(output_path):
                            self.print("Already computed")
                            mat = ConfusionMatrix.load(output_path)
                            if check_correct(mat):
                              matrices[model] = mat
                              continue
                            else:
                              self.print("Old confusion matrix contains errors")

                    self.reset()
                    mat = self.computeConfusionMatrix(model)
                    if not check_correct(mat):
                        self.print("Computed confusion matrix contains errors")
                        matrices[model] = None
                        continue
                    if to_files is not None:
                        matrices[model] = mat
                        matrices[model].save(output_path)
        if output is None:
            return matrices

    def evaluate(self, workflow, file=None, overwrite=False):
        if file is not None:
            evaluations = self.init_evalfile(file, overwrite)
        else:
            evaluations = {}

        for test in workflow:
            self.print("= {} =".format(test['name']))

            if not os.path.exists(test.path):
                self.print("Test not yet trained")
                continue

            for try_ in test:
                self.print("== {}.{} ==".format(test['name'], try_['name']))

                for model in try_:
                    self.print("== {} ===".format(model.name))

                    if not model.is_trained():
                        self.print("Model not yet trained")
                        continue

                    if model.modelfile not in evaluations:
                        evaluations[model.modelfile] = [
                            test['name'], try_['name'], model.repeat,
                            model.modelfile
                        ] + ["", ""] * len(self.getLabels())

                    keras_model = None
                    for i, cls in enumerate(["Global"] + self.getLabels()):
                        self.print("Class {}".format(cls))
                        loss = evaluations[model.modelfile][4 + 2 * i]
                        acc = evaluations[model.modelfile][5 + 2 * i]
                        if (loss != '' and loss != 'nan' and acc != '' and
                                acc != 'nan'):
                            loss = abs(float(loss))
                            acc = abs(float(acc))
                            if loss < 1000 and acc <= 1:
                                self.print("Model already evaluated")
                                continue

                        if keras_model is None:
                            self.print("Loading last version of model")
                            keras_model = keras.models.load_model(
                                model.modelfile)
                            self.print("Model loaded")

                        self.reset()
                        self.print("Evaluating network")
                        self.setInputShape(model.input_shape)
                        _batch = self.getBatchSize()
                        self.setBatchSize(min(model.maxbatch, _batch))
                        score = keras_model.evaluate_generator(self)
                        self.setBatchSize(_batch)

                        a, b = score
                        if abs(a) >= 1000:
                            raise Exception(
                                "Loss too high to be true: {}".format(score))
                        if abs(b) > 1:
                            raise Exception("Accuracy too high to be true: {}".
                                            format(score))
                        evaluations[model.modelfile][4 + 2 * i] = score[0]
                        evaluations[model.modelfile][5 + 2 * i] = score[1]
                        # ~ update_eval()
                        return (evaluations,
                                keras_model.predict_generator(self))
