import warnings # GUI error reporting
from time import time
from functools import wraps
from tkinter import * # Basic GUI elements
from tkinter.ttk import * # Stylized GUI elements
from tkinter import messagebox
from tkinter.filedialog import askopenfilename, asksaveasfilename
from PIL import Image # openCV to PIL (to Tkinter with CICC)

import numpy as np
import cv2
import pydot as pd

from cicc.GUI.image import (
  ImagePanel,     # Weights/output views
  ImagePanelEx,   # Viewport
  MultiImagePanel # Normalized input
)
from cicc.GUI.input import InputPanel # Input
from cicc.GUI.layout import GridLayout
from cicc.normalize import ImageNormalizer
from cicc.normalize.algorithms import default_algorithms
from cicc.GUI.utils import bindClickEvents, LONG_PRESS, CLICK, DOUBLE_CLICK, rgb_to_hex

import controller as controller_module
from layers import Layer, Value
from layers.input import Input

def shape2str (shape):
  return "({})".format(", ".join([str(dim) for dim in shape]))

def falseColorImage (img, colormap=cv2.COLORMAP_JET):
  # Convert array to 8-bit image
  img = img-img.min()
  img = img*255/img.max()
  img = img.astype(np.uint8)
  
  # Apply colormap
  img = cv2.applyColorMap(img, colormap)
  
  # Return result
  return img

class ModelViewer (Frame):
  class optional_instance:
    def __init__ (self, fn):
      self.fn = fn
    
    def __get__ (self, obj, objtype):
      @wraps(self.fn)
      def fn (*args, **kwargs):
        if obj is None:
          return self.fn(objtype, *args, **kwargs)
        else:
          return self.fn(obj, *args, **kwargs)
      return fn
  
  def mouse_handler (f):
    @wraps(f)
    def _f (self, e):
      x,y = self.canvas2graph(e.x, e.y)
      layer, output = self.controller.elementFromPosition(x,y)
      
      f(self,event=e,x=x,y=y,layer=layer,output=output)
    return _f
  
  # Graphic parameters
  BGCOLOR = (0x44, 0x44, 0x44)
  FGCOLOR = (0xff, 0xff, 0xff)
  FLCOLOR = (0x66, 0x66, 0x66)
  INCOLOR = (0x00, 0xff, 0x00)
  OUTCOLOR = (0xff, 0xff, 0x00)
  PAD = 5
  DPI = 100
  
  @optional_instance
  def pixel2point (self, x):
    return x*72.0/self.DPI
  
  @optional_instance
  def point2pixel (self, x):
    return x*self.DPI/72.0
  
  def canvas2graph (self, x, y):
    x,y = self.viewport.canvas2global((x,y))
    return (
      self.pixel2point(x)-self.PAD,
      -(self.pixel2point(y)-self.PAD)
    )
  
  def graph2canvas (self, x, y):
    x,y = (
      self.point2pixel(x+self.PAD),
      -self.point2pixel(y-self.PAD)
    )
    return self.viewport.global2canvas((x,y))
  
  def __init__ (self, *args, controller=None, **kwargs):
    # Initialize frame
    Frame.__init__(self, *args, **kwargs)
    
    # Link controller to view
    if controller is None:
      controller = controller_module.ModelIntegrator()
    self.controller = controller
    self.controller.view = self
    
    ### GUI start
    # Viewport
    self.viewport = ImagePanelEx(self)
    self.viewport.pack(fill=BOTH, expand=True, side=LEFT)
    # Update view without changing the graph when changing view but not content 
    self.viewport.add_callback(self.updateKeepGraph, True)
    
    # Manage viewport canvas independently
    self.canvas = self.viewport.imagepanel
    self.canvas['bg'] = rgb_to_hex(self.BGCOLOR)
    
    # Canvas mouse events
    bindClickEvents(
      self.canvas,
      {
        LONG_PRESS : self.handler_startMove,
        DOUBLE_CLICK : self.handler_showDetails,
      },
      button=1
    )
    self.canvas.bind(
      '<ButtonRelease-1>',
      self.handler_stopAll,
      add='+'
    )
    self.canvas.bind(
      '<Motion>',
      self.handler_motion
    )
    
    # Control panel
    self.controlpanel = ControlPanel(self)
    self.controlpanel.pack(fill=BOTH, side=RIGHT)
    ### GUI end
    
    # Parameters
    self.selected = None  # Current element
    self.moving = None    # Is current element being dragged?    
    
    self.inputs = [] # Input data
    
    # Apply appearance settings to graph
    self.setupGraph()
  
  def setupGraph (self):
    graph = self.controller.graph
    node = graph.get_node('node')[0]
    edge = graph.get_node('edge')[0]
    
    graph.set("splines", "spline")
    graph.set("bgcolor", rgb_to_hex(self.BGCOLOR))
    graph.set("dpi", str(self.DPI))
    graph.set("pad", str(self.PAD/72))
    graph.set("rankdir", 'LR')
    
    node.set("shape", 'record')
    node.set("color", rgb_to_hex(self.FGCOLOR))
    node.set("fontcolor", rgb_to_hex(self.FGCOLOR))
    node.set("fillcolor", rgb_to_hex(self.FLCOLOR))
    node.set("style", 'filled')
    
    edge.set("color", rgb_to_hex(self.FGCOLOR))
    edge.set("headport", 'w')
    edge.set("tailport", 'e')
    edge.set("arrowhead", 'dot')
    edge.set("arrowtail", 'none')
    edge.set("dir", 'both')
  
  @mouse_handler
  def handler_motion (self, event, x, y, layer, output):
    if self.moving is not None:
      self.moving = (event.x, event.y)
    else:
      element = output or layer
      if self.selected == element:
        return
      self.selected = element
    
    self.viewport.showimg()
  
  @mouse_handler
  def handler_stopAll (self, event, x, y, layer, output):
    if self.moving is not None:
      self.moving = None
      self.selected.x = x
      self.selected.y = y
      s = self.selected.node.get_style().replace('"','')
      self.selected.node.set_style('"'+",".join(s.split(",")[:-1])+'"')
      self.update()
    elif self.selected is None:
      return
    
    self.selected = None
    self.viewport.showimg()
  
  @mouse_handler
  def handler_startMove (self, event, x, y, layer, output):
    if layer is not None:
      self.selected = layer
      s = self.selected.node.get_style().replace('"','')
      self.selected.node.set_style('"'+s+',dashed"')
      self.moving = (event.x,event.y)
      self.update()
  
  @mouse_handler
  def handler_showDetails (self, event, x, y, layer, output):
    element = output or layer
    if element is not None:
      self.selected = element
      self.showDetails(element)
  
  def update (self):
    self.controller.update()
    
    # Bounding box
    x,_,_,y = map(
      lambda x: (
        self.point2pixel(float(x))
      ),
      self.controller.annotated.get_bb()[1:-1].split(",")
    )
    y = -y
    
    self.zero = (x,y)
    
    # Create image
    try:
      img = cv2.imdecode(
        np.frombuffer(
          self.controller.graph.create_png(
            prog=self.controller.COMMAND
          ),
          dtype=np.uint8
        ),
        cv2.IMREAD_COLOR
      )
    except FileNotFoundError:
      raise FileNotFoundError(
        "The required Graphviz executables were not found. "+
        "Install them from https://www.graphviz.org/"
      )
    
    # CV2 image to Tkinter image
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    self.img = Image.fromarray(img)
    
    # Position image
    self.viewport.setimg(self.img, self.zero)
    
  def updateKeepGraph (self, IPE=None):
    if self.selected is not None and self.moving is None:
      x,y,X,Y = self.selected.ROI
      x,y = self.graph2canvas(x,y)
      X,Y = self.graph2canvas(X,Y)
      self.canvas.create_rectangle(
        x,y,
        X,Y,
        outline=rgb_to_hex(self.FGCOLOR),
        width=5
      )
    
    if self.moving is not None:
      shape = np.array([self.selected.w, self.selected.h])
      shape = self.point2pixel(shape)
      shape = shape*self.viewport.zoom.get()
      pos = np.array(self.moving)
      x,y = pos-shape/2
      X,Y = pos+shape/2
      self.canvas.create_rectangle(
        x,y,
        X,Y,
        outline=rgb_to_hex(self.FGCOLOR),
        width=5
      )
    
    for layer in self.controller.inputs:
      x,y,X,Y = layer.ROI
      x,y = self.graph2canvas(x,y)
      X,Y = self.graph2canvas(X,Y)
      self.canvas.create_rectangle(
        x,y,
        X,Y,
        outline=rgb_to_hex(self.INCOLOR),
        width=3,
        dash=(3,3)
      )
    
    for layer in self.controller.outputs:
      x,y,X,Y = layer.ROI
      x,y = self.graph2canvas(x,y)
      X,Y = self.graph2canvas(X,Y)
      self.canvas.create_rectangle(
        x,y,
        X,Y,
        outline=rgb_to_hex(self.OUTCOLOR),
        width=3,
        dash=(3,3)
      )
  
  def showDetails (self, element):
    if isinstance(element, Layer):
      LayerWidget(self, element)
    elif isinstance(element.layer, Input):
      InputWidget(self, element)
    else:
      OutputWidget(self, element)
      
class LayerField:
  def __init__ (self, LW, t):
    self.view = LW.view
    self.layer = LW.layer
    self.t = t
    if t == str:
      self.var = StringVar()
    elif t == int:
      self.var = IntVar()
    elif t == float:
      self.var = DoubleVar()
    elif t == bool:
      self.var = BooleanVar()
    elif isinstance(t, list):
      self.var = StringVar()
    else:
      raise Exception("Not implemented")
    self.getter = None
    self.setter = None
    self.callback = None
    
    self._enableSet = False
  
  def enableSet (self):
    self.var.trace('w', self.onSet)
    self._enableSet = True
  
  def update (self):
    self._enableSet = False
    self.var.set(self.list2attr(self.getter))
    self._enableSet = True
  
  def onSet (self, *args, **kwargs):
    if self._enableSet:
      if self.setter is not None:
        self.list2attr(self.setter, self.var.get())
      if self.callback is not None:
        self.list2attr(self.callback)()
      self.view.updateKeepGraph()
  
  def list2attr (self, attrs, var=None):
    if var is not None:
      last = attrs[-1]
      attrs = attrs[:-1]
    value = self.layer
    for attr in attrs:
      if isinstance(attr, tuple):
        args, kwargs = attr
        value = value(*args, **kwargs)
      else:
        value = getattr(value, attr)
    if var is None:
      return value
    else:
      setattr(value, last, var)

class LayerMultifield (LayerField):
  def __init__ (self, LW, ts):
    self.layer = LW.layer
    self.fields = []
    for t in ts:
      self.fields.append(LayerField(LW, t))
    self.getter = None
    self.setter = None
    self.callback = None
    
    self._enableSet = False
  
  def enableSet (self):
    for field in self.fields:
      field.var.trace('w', self.onSet)
    self._enableSet = True
  
  def update (self):
    self._enableSet = False
    values = self.list2attr(self.getter)
    for field, value in zip(self.fields, values):
      field.var.set(value)
    self._enableSet = True
  
  def onSet (self, *args, **kwargs):
    if self._enableSet:
      if self.setter is not None:
        values = tuple(field.var.get() for field in self.fields)
        self.list2attr(self.setter, values)
      if self.callback is not None:
        self.list2attr(self.callback)()
      self.view.updateKeepGraph()

class LayerWidget (Toplevel):  
  def __init__ (self, view, layer, *args, **kwargs):
    self.fields = []
    
    Toplevel.__init__(self, *args, **kwargs)
    self.view = view
    self.layer = layer
    self.title(self.layer.get_label())
    
    self.rowconfigure(0, weight=1)
    self.columnconfigure(0, weight=1)
    self.canvas = Canvas(self)
    self.canvas.grid(row=0, column=0, sticky='news')
    self.scroll = Scrollbar(self, command=self.canvas.yview)
    self.scroll.grid(row=0, column=1, sticky='news')
    self.canvas.config(yscrollcommand=self.scroll.set)
    
    self.mainframe = Frame(self.canvas)
    self.canvas.win = self.canvas.create_window(
      (0,0),
      window=self.mainframe,
      anchor='nw'
    )
    self.canvas.bind('<Configure>', lambda e: (
      self.canvas.itemconfig(
        self.canvas.win,
        width=self.canvas.winfo_width()
      )
    ))
    self.mainframe.bind('<Configure>', lambda e: (
      self.canvas.config(scrollregion=self.canvas.bbox('all'))
    ))
    
    self.mainframe.columnconfigure(0, weight=1)
    self.mainframe.columnconfigure(1, weight=1)
    
    self.info = LabelFrame(self.mainframe, text='Informations')
    self.info.grid(row=0, column=0, columnspan=2, sticky='news')
    
    for i, (label, *attrs) in enumerate(self.layer.getInfo()):
      Label(
        self.info,
        text=label+":"
      ).grid(row=i, column=0, sticky='news')
      field = LayerField(self, str)
      field.getter = attrs
      Label(
        self.info,
        textvariable=field.var
      ).grid(row=i, column=1, sticky='news')
      
      self.fields.append(field)
    
    self.params = LabelFrame(self.mainframe, text='Layer parameters')
    self.params.grid(row=1, column=0, columnspan=2, sticky='news')
    
    for i, (label, t, *other) in enumerate(self.layer.getParams()):
      attrs = other[0]
      
      if isinstance(attrs[0], tuple):
        getter = attrs[0]
        setter = attrs[1]
      else:
        getter = attrs
        setter = attrs
      
      if isinstance(t, tuple):
        field = LayerMultifield(self, t)
        fields = field.fields
      else:
        field = LayerField(self, t)
        fields = [field]
      field.getter = getter
      field.setter = setter
      if len(other) == 2:
        field.callback = other[1]
      
      Label(
        self.params,
        text=label+":"
      ).grid(row=i, column=0, sticky='news')
      
      for j, f in enumerate(fields):
        if isinstance(t, list):
          widget = OptionMenu(
            self.params,
            f.var,
            t[0],
            *t
          )
        elif t == bool:
          widget = Checkbutton(
            self.params,
            variable=f.var
          )
        else:
          widget = Entry(
            self.params,
            textvariable=f.var
          )
        widget.grid(row=i, column=1+j, sticky='news')
        if getter[0] != 'normalize':
          widget['state'] = 'disabled'
      
      field.enableSet()
      self.fields.append(field)
    
    self.inputs = LabelFrame(self.mainframe, text='Inputs')
    self.inputs.grid(row=2, column=0, sticky='news')
    
    for i, value in enumerate(self.layer.inputs):
      if value is None:
        continue
      label = value.name
      if value.keras is not None:
        label += " {}".format(shape2str(value.keras.shape))
        
      lbl = Label(
        self.inputs,
        text=label
      )
      lbl.grid(row=i, column=0, sticky='news')
      lbl.bind('<Motion>', lambda e,value=value: (
        setattr(self.view, 'selected', value),
        self.view.viewport.showimg()
      ))
    
    self.outputs = LabelFrame(self.mainframe, text='Outputs')
    self.outputs.grid(row=2, column=1, sticky='news')
    
    for i, value in enumerate(self.layer.outputs):
      label = value.name
      if value.keras is not None:
        label += " {}".format(shape2str(value.keras.shape))
      lbl = Label(
        self.outputs,
        text=label
      )
      lbl.grid(row=i, column=0, sticky='news')
      lbl.bind('<Motion>', lambda e,value=value: (
        setattr(self.view, 'selected', value),
        self.view.viewport.showimg()
      ))
      lbl.bind(
        '<Double-Button-1>',
        lambda e,value=value,self=self: (
          self.view.showDetails(value)
        )
      )
    
    try:
      self.wb = LabelFrame(self.mainframe, text='Weights&Bias')
      
      Label(
        self.wb,
        text="Weights:"
      ).grid(row=0, column=0, sticky='news')
      self.weights = Label(
        self.wb,
        text=str(tuple(layer.keras.get_weights()[0].shape))
      )
      self.weights.grid(row=0, column=1, sticky='news')
      self.weights.bind(
        '<Double-Button-1>',
        lambda e,layer=layer: (
          WeightsWidget(self.view, layer)
        )
      )
      self.wb.grid(row=3, column=0, columnspan=2, sticky='news')
    except Exception as e:
      warnings.warn(str(e))
    
    for field in self.fields:
      field.update()

class WeightsWidget (Toplevel): 
  def __init__ (self, view, layer, *args, **kwargs):
    self.layer = layer
    self.view = view
    self.filter = 0
    self.numfilters = self.layer.keras.get_weights()[0].shape[-1]

    Toplevel.__init__(self, *args, **kwargs)
    self.title(self.layer.name)
    
    self.rowconfigure(1, weight=1)
    self.columnconfigure(1, weight=1)
    
    self.image = MultiImagePanel(self)
    self.image.grid(row=1, column=1, sticky='news')
    
    self.left = Button(
      self,
      text="<",
      command=self.prevFilter
    )
    self.left.grid(row=1, column=0, sticky='news')
    
    self.right = Button(
      self,
      text=">",
      command=self.nextFilter
    )
    self.right.grid(row=1, column=2, sticky='news')
    
    self.info = Label(self)
    self.info.grid(row=0, column=0, columnspan=3)
    
    self.update()
    
  def prevFilter (self):
    self.filter -= 1
    if self.filter < 0:
      self.filter += self.numfilters
    self.update()
  
  def nextFilter (self):
    self.filter += 1
    self.filter %= self.numfilters
    self.update()
  
  def update (self):
    self.image.clear_images(keep_index=True)
    
    weights, bias = self.layer.keras.get_weights()
    filter_index = [slice(None) for dim in weights.shape]
    filter_index[-1] = self.filter
    out = np.atleast_3d(weights[tuple(filter_index)])
    for depth in range(out.shape[-1]):
      self.image.add_image((
        "minvalue = {} ; maxvalue = {}".format(
          out[:,:,depth].min(),
          out[:,:,depth].max()
        ),
        falseColorImage(out[:,:,depth])
      ), update=False)
    
    self.info['text'] = (
      "Filter: {}/{} ; bias = {}".format(
        self.filter+1,
        self.numfilters,
        bias[self.filter]
      )
    )
    self.image.change_image()

class OutputWidget (Toplevel):
  visible = []
  
  def __init__ (self, view, value, *args, **kwargs):
    self.value = value
    self.input = 0
    self.visible.append(self)
    self.view = view

    Toplevel.__init__(self, *args, **kwargs)
    self.title(self.value.name)
    
    self.image = MultiImagePanel(self)
    self.image.pack(fill=BOTH, expand=True)
    
    self.update()
      
  def destroy (self, *args, **kwargs):
    self.visible.remove(self)
    super().destroy(*args, **kwargs)
  
  def update (self):
    self.image.clear_images(keep_index=True)
    
    error = False
    if self.value.keras_f is None:
      self.change_image()
      return
    if self.view.inputs == []:
      self.change_image()
      return
    try:
      outs = self.value.keras_f(self.view.inputs)
    except:
      self.image.add_image((
        "No valid input",
        np.zeros((1,1,1), dtype=np.uint8)
      ), show=True)
      return
    else:
      out = outs[0] # Only one output from one node
      out = out[self.input] # Take selected input
      if out.ndim == 1: # array of neurons
        self.image.add_image((
          "minvalue = {} ; maxvalue = {}".format(out.min(),out.max()),
          falseColorImage(out)
        ))
      else:
        for filter in range(out.shape[-1]):
          if out.ndim == 3: # 2D image * filters
            this_out = out[:,:,filter]
          elif out.ndim == 2: # 1D image * filters
            this_out = out[None, :,filter]
          self.image.add_image((
            "minvalue = {} ; maxvalue = {}".format(this_out.min(),this_out.max()),
            falseColorImage(this_out)
          ), update=False)
        self.image.change_image()
  
class InputWidget (Toplevel):
  def __init__ (self, view, value, *args, **kwargs):
    Toplevel.__init__(self, *args, **kwargs)
    self.view = view
    self.value = value
    self.layer = value.layer
    self.layer.widget = self
    self.title(self.value.name)
    
    self.menubar = Menu(self, tearoff=0)
    self['menu'] = self.menubar
    
    self.columnconfigure(0, weight=1)
    self.columnconfigure(1, weight=1)
    self.rowconfigure(1, weight=1)
    
    Label(
      self,
      text="Original image"
    ).grid(row=0, column=0, sticky='news')
    
    self.inimage = InputPanel(self, use_landmarks=False)
    self.inimage.grid(row=1, column=0, sticky='news')
    
    self.inimage.add_augmentation(lambda IP: (
      self.update() or IP.image
    ), False)
    
    # ~ Label(
      # ~ self,
      # ~ text="Normalized image"
    # ~ ).grid(row=0, column=1, sticky='news')
    
    # ~ self.outimage = MultiImagePanel(self)
    # ~ self.outimage.grid(row=1, column=1, sticky='news')
    
    if hasattr(self.value, 'value'):
      self.inimage.setimg(self.value.value)
  
  _lastupdate = time()
  UPDATE_EVERY = 0.5
  def update (self, *args):
    now = time()
    if now-self._lastupdate < self.UPDATE_EVERY:
      return
    self._lastupdate = now
    IN = ImageNormalizer(
      self.layer.shape,
      self.layer.normalize
    )
    imgs = IN.normalize(self.inimage.image)
    if len(imgs) > 0:
      # ~ self.outimage.clear_images()
      # ~ for img in imgs:
        # ~ self.outimage.add_image(img)
      
      self.value.value = self.inimage.image
      if self.value in self.view.controller.inputs:
        self.view.inputs = [np.array(imgs)]
      
        for widget in OutputWidget.visible:
          widget.update()
  
  def destroy (self, *args, **kwargs):
    self.value.layer.widget = None
    Toplevel.destroy(self, *args, **kwargs)

class ControlPanel (Frame):
  def __init__ (self, view, *args, **kwargs):
    Frame.__init__(self, view, *args, **kwargs)
    self.view = view
    
    layout = GridLayout("""
       btnCenter
      ---------------------+-----------
       "Automatic layout:" | optLayout
      ---------------------+-----------
       btnLoad
      ---------------------------------
       lblLoad
    """)
    
    self.btnCenter = Button(
      self,
      text="Center network",
      command=self.view.viewport.resetposition
    )
    
    
    self.layout = StringVar()
    self.optLayout = OptionMenu(
      self,
      self.layout,
      'fdp',
      *self.view.controller.LAYOUTS
    )
    self.layout.trace('w', lambda *args: (
        self.view.controller.reposition(layout=self.layout.get()),
        self.view.update()
      )
    )
    
    self.lblLoad = Label(self, text="No network loaded")
    self.btnLoad = Button(
      self,
      text="Load HDF5 file",
      command=self.load
    )
    
    layout.apply(self)
    
    style = Style()
  
  def load (self):
    f = askopenfilename(
      defaultextension='.h5',
      filetypes=[('HDF5', '*.h5'), ('Other', '*')]
    )
    if f is not None and f != '':
      try:
        self.view.controller.loadHDF5(f, layout=self.layout.get())
      except Exception as e:
        messagebox.showerror(
          "Can't open network file",
          "{}: {}".format(e.__class__.__qualname__, " ".join(e.args))
        )
        self.lblLoad['text'] = "No network loaded"
      else:
        self.lblLoad['text'] = "Net `{}` loaded".format(f)
        self.view.update()
