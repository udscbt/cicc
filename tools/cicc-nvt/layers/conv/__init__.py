from .. import (
  Layer, Value,
  add_unique_name_generator,
  available_layers,
  ACTIVATION_OPTS,
  INITIALIZER_OPTS,
  REGULARIZER_OPTS,
  CONSTRAINT_OPTS
)

import keras

@add_unique_name_generator('conv2d_')
class Conv (Layer):
  CLASSNAME = 'Convolution'
  KERAS = keras.layers.Convolution2D
  
  PARAMETERS = [
    (
      'Number of filters', int, ('filters',)
    ), (
      'Kernel size', (int, int), ('kernel_size',)
    ), (
      'Strides', (int, int), ('strides',)
    ), (
      'Padding type', ['valid', 'same'], ('padding',)
    ), (
      'Data format',
      ['channels_last', 'channels_first'],
      ('data_format',)
    ), (
      'Dilation rate', (int, int), ('dilation_rate',)
    ), (
      'Activation function',
      ACTIVATION_OPTS,
      ('activation',)
    ), (
      'Use bias', bool, ('use_bias',)
    ), (
      'Kernel initializer',
      INITIALIZER_OPTS,
      ('kernel_initializer',)
    ), (
      'Bias initializer',
      INITIALIZER_OPTS,
      ('bias_initializer',)
    ), (
      'Kernel regularizer', 
      REGULARIZER_OPTS,
      ('kernel_regularizer',)
    ), (
      'Bias regularizer',
      REGULARIZER_OPTS,
      ('bias_regularizer',)
    ), (
      'Activity regularizer',
      REGULARIZER_OPTS,
      ('activity_regularizer',)
    ), (
      'Kernel constraint',
      CONSTRAINT_OPTS,
      ('kernel_constraint',)
    ), (
      'Bias constraint',
      CONSTRAINT_OPTS,
      ('bias_constraint',)
    )
  ]
  
  ONE2ONE = [
    'name',
    'filters',
    'kernel_size',
    'strides',
    'padding',
    'data_format',
    'dilation_rate',
    'activation',
    'use_bias',
  ]
  
  NAMED_CLASS = [
    'activation',
    'kernel_initializer',
    'bias_initializer',
    'kernel_regularizer',
    'bias_regularizer',
    'activity_regularizer',
    'kernel_constraint',
    'bias_constraint'
  ]
  
  def __init__ (self, *args, **kwargs):
    self.filters = 1
    self.kernel = (1,1)
    if kwargs.get('keras', None) is None:
      kwargs['keras'] = keras.layers.Conv2D(1, (1,1))
    Layer.__init__(self, *args, **kwargs)

available_layers[Conv.KERAS] = Conv

