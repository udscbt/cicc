from tkinter import *
from tkinter.ttk import *
from tkinter.messagebox import showerror, askyesno
from collections import OrderedDict as OD
from . import STRINGS

class SelectionBar (Frame):
  SCREENS = OD({})
  @staticmethod
  def addScreen(name, _class):
    SelectionBar.SCREENS[name] = _class

  def __init__ (self, master, **kwargs):
    Frame.__init__(self, master)
    self.var = StringVar(master)
    self.screen = None
    self.var.set(self.screensList()[0])
    self.opt = OptionMenu(self, self.var, self.screensList()[0], *self.screensList())
    self.opt.pack(side="left")
    
    self.btnMax = Button(self, text='+', command=self.maxdemax)
    self.btnMax.pack(side="left")
    
    self.btnCls = Button(self, text='x', command=self.closeScreen)
    self.btnCls.pack(side="left")
    
    self.var.trace("w", self.changeScreen)
  
  def maxdemax (self, *args):
    self.master.maximized = not self.master.maximized
    self.btnMax['text'] = '-' if self.master.maximized else '+'
    self.master.resize()
    if self.master.maximized:
      self.master.lift()
    else:
      for virtual in self.master.boundary:
        virtual.real.lift()
  
  def screensList(self):
    l = list(self.SCREENS.keys())
    return l
  
  def closeScreen (self, *args):
    if len(self.master.master.screens) > 1:
      if askyesno(STRINGS.SELBAR_ASK_CLOSE_TITLE,STRINGS.SELBAR_ASK_CLOSE_MSG):
        self.master.remove()
        return
    else:
      showerror(STRINGS.SELBAR_CLOSE_ERROR_TITLE, STRINGS.SELBAR_CLOSE_ERROR_MSG)
  
  def changeScreen(self, *args):
    name = self.var.get()
    if name == self.screen:
      return
    try:
      if name == STRINGS.SELBAR_REMOVE_SCREEN:
        self.closeScreen()
      else:
        self.master.setContent(self.SCREENS[name])
        self.screen = self.var.get()
    except Exception as e:
      #~ raise e
      showerror(type(e).__name__, "{}: {}".format(type(e).__name__, str(e)))
    self.after(100, lambda: self.var.set(self.screen))
