\contentsline {section}{\numberline {1}Installation}{2}{section.1}% 
\contentsline {subsection}{\numberline {1.1}Get source code}{2}{subsection.1.1}% 
\contentsline {subsubsection}{\numberline {1.1.1}With \texttt {git}}{2}{subsubsection.1.1.1}% 
\contentsline {subsubsection}{\numberline {1.1.2}As \texttt {.zip} file}{2}{subsubsection.1.1.2}% 
\contentsline {subsection}{\numberline {1.2}Install CICC}{3}{subsection.1.2}% 
\contentsline {subsubsection}{\numberline {1.2.1}From source}{3}{subsubsection.1.2.1}% 
\contentsline {subsubsection}{\numberline {1.2.2}With Python wheel}{3}{subsubsection.1.2.2}% 
\contentsline {subsection}{\numberline {1.3}Install Graphviz}{3}{subsection.1.3}% 
\contentsline {subsection}{\numberline {1.4}Test the installation}{3}{subsection.1.4}% 
\contentsline {section}{\numberline {2}Interface}{3}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Main window}{3}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}Layer information}{4}{subsection.2.2}% 
\contentsline {subsection}{\numberline {2.3}Get input}{6}{subsection.2.3}% 
\contentsline {subsection}{\numberline {2.4}Display tensor value}{6}{subsection.2.4}% 
\contentsline {subsection}{\numberline {2.5}Display weights tensor}{7}{subsection.2.5}% 
