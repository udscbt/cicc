import numpy as np
import scipy.stats

def piece_indices (x, a, b, n=None):
    x = np.array(x)
    x.sort()
    # ~ if n is None:
        # ~ n = b-a
    
    # ~ points = np.linspace(a, b, n, endpoint=False)
    
    # ~ ret = []
    # ~ idx = 0
    # ~ for p in points:
        # ~ while idx < len(x):
            # ~ if x[idx] > p:
                # ~ break
            # ~ idx += 1
        # ~ if idx == 0:
            # ~ ret.append(idx)
        # ~ elif idx == len(x):
            # ~ ret.append(idx-1)
        # ~ else:
            # ~ d1 = x[idx]-p
            # ~ d2 = p-x[idx-1]
            # ~ ret.append(
                # ~ idx if d1 < d2 else idx-1
            # ~ )
    ret = np.arange(len(x))[(x >= a) * (x < b)]
    return ret

def piece (x, a, b, n=None):
    extended = []
    if a < 0:
        extended = [x[0]]*(-a)
        a = 0
    extended += list(x[a:b])
    if b > len(x):
        extended += [x[-1]]*(b-len(x))
        b = len(x)
    return np.array(x[a:b]), np.array(extended)

def piecewise (x, y, size, eval_on=None):
    if eval_on is None:
        eval_on = x
    counts = np.zeros(len(eval_on))
    newy   = np.zeros(len(eval_on))
    
    for i in x:
        a,b = int(i)-size, int(i)+size
        
        indices = piece_indices(x, a, b)
        xs = x[indices]
        ys = y[indices]
        m,q,*_ = scipy.stats.linregress(xs, ys)
        
        eval_indices = np.unique(piece_indices(eval_on, a, b))
        y_ = q+m*eval_on[eval_indices]
        
        counts[eval_indices] += 1
        newy[eval_indices] += y_
    
    return newy/counts
