from tkinter import *
from tkinter.ttk import *
from view import ModelViewer

root = Tk()
root.title("CICC - NetVisualizer")
Style().theme_use('clam')
vis = ModelViewer(root)
vis.update()
vis.pack(fill=BOTH, expand=True, side=LEFT)

root.mainloop()
