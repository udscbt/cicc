"""
Module to manage workflow (.json) files.

Workflow file
-------------
[
    {
        "name" : _NAME_,
        ...
    }
]
"""

import os
import json
import numpy as np
import keras
from keras.layers import Input, concatenate
from keras.layers import (Conv2D, MaxPooling2D, Dense, ReLU, Flatten, Dropout,
                          SpatialDropout2D)
from keras.models import Model as KerasModel
from keras.models import load_model
from time import time

# VERBOSITY_LEVELS
MINIMAL = 0
MEDIUM = 1
COMPLETE = 2

# TRAIN LEVEL
TRAINED_NO = 0
TRAINED_PARTIAL = 1
TRAINED_FULL = 2
TRAINED_OVER = 3

class HierarchicalStruct:
    def __init__ (self, parent=None, params=None, exclude=None):
        self.parent = parent
        self.children = []
        if params is not None:
            if exclude is not None:
                params = {k:v for k,v in params.items() if k not in exclude}
            self.params = params
        else:
            self.params = {}
        self.excluded = exclude
    
    def __getitem__ (self, key):
        if isinstance(key, int):
            return self.children[key]
        if self.parent is not None and key not in self.params:
            return self.parent[key]
        return self.params[key]
    
    def __setitem__ (self, key, value):
        self.params[key] = value
    
    def __contains__ (self, key):
        if key in self.params:
            return True
        if self.parent is not None and key in self.parent:
            return True
        return False
    
    def keys (self):
        if self.parent is None:
            return self.params.keys()
        return set([*self.params.keys(), *self.parent.keys()])
    
    def items (self):
        for key in self.keys():
            yield (key, self[key])
    
    def values (self):
        for key in self.keys():
            yield self[key]
    
    def isset (self, key):
        return key in self.params
    
    def isstandard (self, key):
        if self.isset(key):
            return self.excluded is None or key not in self.excluded
        return self.parent.isstandard(key)
    
    def isrequired (self, key):
        if not self.isset(key):
            return False
        if self.parent is None or key not in self.parent:
            return True
        return self[key] != self.parent[key]
    
    def to_dict (self, verbose=MEDIUM):
        if self.parent is None:
            obj = {}
        else:
            obj = HierarchicalStruct.to_dict(self.parent, verbose)
        
        for k, v in self.params.items():
            if not self.isstandard(k):
                continue
            if verbose == MINIMAL:
                if not self.isrequired(k):
                    continue
            if not self.isset(k):
                if verbose == COMPLETE:
                    obj[k] = self[k]
                else:
                    continue
            else:
                try:
                    obj[k] = v.to_dict(verbose=verbose)
                except:
                    obj[k] = v
        return obj
    
class Workflow (HierarchicalStruct):
    DEFAULTS = {"comment": "", "epochs": 100, "samples": 10000, "batch": 64, "optimizer": 'adadelta',
                   "loss":'categorical_crossentropy',
                   "metrics":['accuracy'],}
    
    def __init__(self, folder):
        HierarchicalStruct.__init__(self, params=self.DEFAULTS)
        self.folder = folder
        self.summary = {}
        self.clear()
    
    @property
    def children (self):
        return self.tests
    
    @children.setter
    def children (self, value):
        self.tests = value

    def clear(self):
        self.tests = []

    def decode(self, s, from_file=False):
        if from_file:
            with open(s, 'r') as f:
                s = f.read()
        workflow = json.loads(s)

        self.clear()
        for i, test in enumerate(workflow):
            self.tests.append(Test(self, test, i))

        return self

    def encode(self, indent=2, to_file=None, verbose=MEDIUM):
        workflow = []
        for test in self.tests:
            workflow.append(test.to_dict(verbose=verbose))
        workflow = json.dumps(workflow, indent=indent)
        if to_file is not None:
            with open(to_file, 'w') as f:
                f.write(workflow)
        else:
            return workflow

    def __iter__(self):
        return iter(self.tests)

    def iter_all(self):
        for test in self.tests:
            for try_ in test:
                for model in try_:
                    yield model

    def __len__(self):
        return len(self.tests)

    def is_trained(self):
        ret = None
        for test in self:
            trained = test.is_trained()
            if trained == TRAINED_PARTIAL:
                return TRAINED_PARTIAL
            elif trained == TRAINED_NO:
                if ret is None:
                    ret = TRAINED_NO
                elif ret == TRAINED_FULL:
                    return TRAINED_PARTIAL
            else:
                if ret is None:
                    ret = TRAINED_FULL
                elif ret == TRAINED_NO:
                    return TRAINED_PARTIAL
        return ret


class Test (HierarchicalStruct):

    def __init__(self, workflow, test, i):
        HierarchicalStruct.__init__(self, parent=workflow, params=test, exclude=['structure', 'tries', 'name'])
        self.workflow = workflow

        self.origname = test.get('name', None)
        self.defaultname = 'test{}'.format(i)
        self.params['name'] = self.origname or self.defaultname
        self.path = os.path.join(self.workflow.folder, self.params['name'])

        if not ('shape' in test and 'structure' in test):
            raise Exception('Test {} missing mandatory parameters'.format(
                self.params['name']))
        self.params['structure'] = Structure(self, test['structure'])

        tries = test.get('tries', 1)
        if isinstance(tries, list):
            self.params['tries'] = [
                Try(self, i, try_=try_) for i, try_ in enumerate(tries)
            ]
        else:
            self.params['tries'] = [Try(self, 0, repeat=tries)]
        
        self.children = self['tries']

    def __iter__(self):
        return iter(self['tries'])

    def __len__(self):
        return len(self['tries'])

    def __contains__(self, k):
        return k in self.params

    def to_dict(self, verbose=MEDIUM):
        test = HierarchicalStruct.to_dict(self, verbose)
        if self.origname == None:
            if verbose == COMPLETE:
                test['name'] = self['name']
        else:
            if (verbose != MINIMAL
                    or self.origname != self.defaultname):
                test['name'] = self['name']
        test['structure'] = self['structure'].to_dict()

        if (len(self['tries']) == 1
                and self['tries'][0].is_default(verbose=verbose)):
            tries = self['tries'][0]['#']
            if tries == 1 and verbose == MINIMAL:
                return test
        else:
            tries = []
            for try_ in self['tries']:
                tries.append(try_.to_dict(verbose=verbose))
        test['tries'] = tries

        return test

    def is_trained(self):
        ret = None
        for try_ in self:
            trained = try_.is_trained()
            if trained == TRAINED_PARTIAL:
                return TRAINED_PARTIAL
            elif trained == TRAINED_NO:
                if ret is None:
                    ret = TRAINED_NO
                elif ret == TRAINED_FULL:
                    return TRAINED_PARTIAL
            else:
                if ret is None:
                    ret = TRAINED_FULL
                elif ret == TRAINED_NO:
                    return TRAINED_PARTIAL
        return ret


class Structure:
    def __init__(self, test, structure):
        self.test = test
        self.original = structure
        self.levels = []
        for level in structure:
            if not isinstance(level, list):
                level = [level]

            lvl = []
            for layer in level:
                lvl.append(self.decode_layer(layer))
            self.levels.append(lvl)

    def to_dict(self, verbose=MEDIUM):
        return self.original

    def build(self):
        X = Input(self.test['shape'])
        out = X
        for level in self.levels:
            outs = []
            for layer in level:
                layer_type, args, kwargs = layer
                layer = layer_type(*args, **kwargs)
                outs.append(layer(out))
            if len(outs) == 1:
                out = outs[0]
            else:
                out = concatenate(outs)
        model = KerasModel(inputs=X, outputs=out)
        return model

    def decode_layer(self, s):
        t, params = s.split(";")
        if t == 'c':  # conv
            filters, kernel, stride, dilation, activation = params.split(",")
            return (Conv2D, [int(filters)],
                    dict(
                        kernel_size=int(kernel),
                        strides=int(stride),
                        padding="same",
                        dilation_rate=int(dilation),
                        activation=activation))
        elif t == 'm':  # maxpool
            pool, stride = params.split(",")
            return (MaxPooling2D, [],
                    dict(pool_size=int(pool), strides=int(stride)))
        elif t == 'd':  # dense
            neurons, activation = params.split(",")
            return (Dense, [int(neurons)], dict(activation=activation))
        elif t == 'r':  # relu
            return (ReLU, [], {})
        elif t == 'f':  # flatten
            return (Flatten, [], {})
        elif t == 'model':
            test, output, weights = params.split(",")
            return (ModelLayer,
                    [self.test.workflow.folder, test,
                     int(output), weights], {})
        elif t == 'D':  # dropout
            rate = params
            return (Dropout, [float(rate)], {})
        elif t == 'SD':  # spatial dropout
            # Not implemented in PlaidML
            rate = params
            return (Dropout, [float(rate)], {})
            # ~ rate = params
            # ~ return (
            # ~ SpatialDropout2D,
            # ~ [
            # ~ float(rate)
            # ~ ],
            # ~ {}
            # ~ )
        else:
            raise Exception("Unrecognized layer type: {}".format(t))


def ModelLayer(folder, test, output, weights):
    if '_' in test:
        test_folder, *suffix = test.split("_")
    else:
        test_folder = test
    test = os.path.join(folder, test_folder, test + ".h5")
    source = load_model(test)
    submodel = KerasModel(
        inputs=source.inputs, outputs=source.layers[output].output)
    if weights == 'train':
        pass
    elif weights == 'keep':
        for layer in submodel.layers:
            layer.trainable = False
    else:
        raise Exception('Unrecognized parameter for "model": {}'.format(
            weights) + '\nMust be either "train" or "keep".')

    submodel.workflow_params = (folder, test, output, weights)
    return submodel


class Try (HierarchicalStruct):
    def __init__(self, test, i, try_=None, repeat=1):
        HierarchicalStruct.__init__(self, parent=test, params=try_, exclude=['sequence', 'name'])
        self.test = test
        if try_ is None:
            try_ = {'#': repeat}

        self.origname = try_.get('name', None)
        self.defaultname = str(i) if i != 0 else ""
        self.params['name'] = self.origname or self.defaultname
        self.params['#'] = try_.get('#', repeat)

        seq = try_.get('sequence', None)
        if seq is None:
            seq = [{}]
        self.params['sequence'] = [Sequence(self, s) for s in seq]

        self.totalepochs = 0
        for seq in self.params['sequence']:
            seq.epoch_range = (self.totalepochs,
                               self.totalepochs + seq['epochs'])
            self.totalepochs = seq.epoch_range[1]
    
    @property
    def children (self):
        return [Model(self, i) for i in range(self['#'])]
    
    @children.setter
    def children (self, value):
        pass

    def __iter__(self):
        for i in range(self['#']):
            yield self[i]

    def __len__(self):
        return self['#']

    def __contains__(self, k):
        return k in self.params

    def suffix(self, repeat=0):
        suffix = "_{}".format(self['name']) if self['name'] != "" else ""
        ssuffix = "__{}".format(repeat) if repeat != 0 else ""
        return suffix + ssuffix

    def to_dict(self, verbose=MEDIUM):
        try_ = HierarchicalStruct.to_dict(self, verbose)
        if self.origname == None:
            if verbose == COMPLETE:
                try_['name'] = self['name']
        else:
            if (verbose != MINIMAL
                    or self.origname != self.defaultname):
                try_['name'] = self.origname

        if (len(self['sequence']) == 1
                and self['sequence'][0].is_default(verbose=verbose)):
            return try_
        else:
            sequence = []
            for seq in self['sequence']:
                sequence.append(seq.to_dict(verbose=verbose))
        try_['sequence'] = sequence

        return try_

    def is_default(self, verbose=MEDIUM):
        if verbose == COMPLETE:
            return False

        if self.origname is not None:
            return False

        for k in self.test.workflow.DEFAULTS:
            v = self.params[k]
            if self.params[k] is not None:
                if verbose is not MINIMAL or v != self.test[k]:
                    return False

        for s in self['sequence']:
            if not s.is_default(verbose=verbose):
                return False

        return True

    def is_trained(self):
        ret = None
        for model in self:
            trained = model.is_trained()
            if trained == TRAINED_PARTIAL:
                return TRAINED_PARTIAL
            elif trained == TRAINED_NO:
                if ret is None:
                    ret = TRAINED_NO
                elif ret == TRAINED_FULL:
                    return TRAINED_PARTIAL
            else:
                if ret is None:
                    ret = TRAINED_FULL
                elif ret == TRAINED_NO:
                    return TRAINED_PARTIAL
        return ret


class Sequence (HierarchicalStruct):
    def __init__(self, try_, s):
        HierarchicalStruct.__init__(self, parent=try_, params=s)
        self.try_ = try_
    
    def is_default(self, verbose=MEDIUM):
        if verbose == COMPLETE:
            return False

        for k in self.try_.test.workflow.DEFAULTS:
            v = self.params[k]
            if self.params[k] is not None:
                if verbose is not MINIMAL or v != self.try_[k]:
                    return False

        return True


class Model (HierarchicalStruct):
    def __init__(self, try_, repeat):
        HierarchicalStruct.__init__(self, parent=try_)
        self.repeat = repeat
        self.try_ = try_
        self.test = self.try_.test
        self.workflow = self.test.workflow

        self.epochs = self.try_.totalepochs
        self.input_shape = self.test['shape']
        self.minbatch = min([seq['batch'] for seq in self.try_['sequence']])
        self.maxbatch = max([seq['batch'] for seq in self.try_['sequence']])

        self.suffix = self.try_.suffix(self.repeat)

        self.name = self.test['name'] + self.suffix

        self.modelfile = os.path.join(self.test.path, self.name + ".h5")
        self.epochfile = os.path.join(self.test.path, "epochs" + self.suffix)
        self.historyfile = os.path.join(self.test.path,
                                        "history" + self.suffix + ".csv")

    def assure_folders_existance(f):
        def f_(self, *args, **kwargs):
            if not os.path.exists(self.workflow.folder):
                os.mkdir(self.workflow.folder)
            if not os.path.exists(self.test.path):
                os.mkdir(self.test.path)

            return f(self, *args, **kwargs)

        return f_

    def get_model(self):
        if os.path.exists(self.modelfile):
            return load_model(self.modelfile)
        else:
            return None

    def make_model(self,
                   *,
                   optimizer=None,
                   loss=None,
                   metrics=None,
                   save=False,
                   **kwargs):
        if optimizer is None:
            optimizer = self['optimizer']
        if loss is None:
            loss = self['loss']
        if metrics is None:
            metrics = self['metrics']
        model = self.test['structure'].build()
        model.compile(
            optimizer=optimizer, loss=loss, metrics=metrics, **kwargs)
        if save:
            model.save(self.modelfile)
        return model

    @assure_folders_existance
    def save_model(self, model):
        model.save(self.modelfile)

    def get_next_epoch(self):
        if os.path.exists(self.epochfile):
            with open(self.epochfile, "r") as f:
                return int(f.read()) + 1
        else:
            return 0

    @assure_folders_existance
    def set_last_epoch(self, epoch=None, logging=False):
        if epoch is None:
            epoch = self.get_next_epoch()
        if logging:
            with open(self.epochfile + "_bis.txt", "a") as f:
                f.write("{} {}\n".format(epoch, time()))
        with open(self.epochfile, 'w') as f:
            f.write(str(epoch))

    def get_sequence(self, epoch=None):
        if epoch is None:
            epoch = self.get_next_epoch()
        for seq in self.try_['sequence']:
            if (seq.epoch_range[0] <= epoch and seq.epoch_range[1] > epoch):
                return seq
        return None

    def get_history(self):
        if os.path.exists(self.historyfile):
            return History(self.historyfile)
        else:
            return History()

    @assure_folders_existance
    def history_callback(self):
        return keras.callbacks.CSVLogger(
            self.historyfile, separator=',', append=True)

    def is_trained(self):
        epoch = self.get_next_epoch()
        if epoch == 0:
            return TRAINED_NO
        elif epoch < self.epochs:
            return TRAINED_PARTIAL
        elif epoch == self.epochs:
            return TRAINED_FULL
        else:
            return TRAINED_OVER


class History:
    HEADER = ["epoch", "acc", "loss", "val_acc", "val_loss"]

    def __init__(self, path=None):
        if path is not None:
            with open(path, 'r') as f:
                data = f.read()
            data = [x.split(",") for x in data.split("\n") if x != ""][1:]
            self.data = np.array(data, dtype=float)
        else:
            self.data = np.array([])

    def __getitem__(self, k):
        if isinstance(k, list):
            keys = []
            for key in k:
                if key in self.HEADER:
                    keys.append(self.HEADER.index(k))
                else:
                    keys.append(k)
            return self.data[:, keys]
        elif k in self.HEADER:
            return self.data[:, self.HEADER.index(k)]
        elif isinstance(k, int):
            found = self.data[np.argwhere(self.data[:, 0] == k).flatten()]
            if len(found) == 0:
                return None
            elif len(found) == 1:
                return found[0]
            else:
                return found

    def __iter__(self):
        return iter(self.data)

    def __len__(self):
        return len(self.data)
    
    def save (self, path):
        with open(path, 'w') as f:
            f.write(",".join(self.HEADER))
            f.write("\n")
            f.write(
                "\n".join([
                    ",".join([str(x) for x in row])
                    for row in self.data
                ])
            )
