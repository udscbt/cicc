import tkinter as tk
from tkinter import *
from tkinter.ttk import *
import pydot as pd
from cicc.utils import SubmoduleImporter
import os

ACTIVATION_OPTS = [
  'none',
  'softmax',
  'elu',
  'selu',
  'softplus',
  'softsign',
  'relu',
  'tanh',
  'sigmoid',
  'hard_sigmoid',
  'exponential',
  'linear'
]

INITIALIZER_OPTS = [
  'none'
  'Zeros',
  'Ones',
  'Constant',
  'RandomNormal',
  'RandomUniform',
  'TruncatedNormal',
  'VarianceScaling',
  'Orthogonal',
  'Identity',
  'lecun_uniform',
  'glorot_normal',
  'glorot_uniform',
  'he_normal',
  'lecun_normal',
  'he_uniform',
]

REGULARIZER_OPTS = [
  'none',
  'l1',
  'l2',
  'l1_l2'
]

CONSTRAINT_OPTS = [
  'none',
  'MaxNorm',
  'NonNeg',
  'UnitNorm',
  'MinMaxNorm',
]

def cleanstr (s):
  return s.replace("\\\r\n", "").replace("\\\n", "")

def add_unique_name_generator (basename="", fname="getUniqueName"):
  def _decorator (cls):
    cls.NAME_UID = 0
    def _generate_name (self):
      name = basename+str(cls.NAME_UID)
      cls.NAME_UID += 1
      return name
    _generate_name.__name__ = fname
    setattr(cls, fname, _generate_name)
    return cls
  return _decorator

@add_unique_name_generator('output_')
class Value:
  def __init__ (self, layer, input=None, name=None, fromkeras=None):
    self.nodename = self.getUniqueName()
    if name is None:
      name = self.nodename
    self.name = name
    self.layer = layer
    self.input = input
    self.outputs = []
    
    self.keras = fromkeras
    self.keras_f = None
    
    self._ROI = [0,0,0,0]
    
    self.layer.outputs.append(self)
    self.layer.inputs.append(self.input)
    self.layer.updateLabel()
    
    self.isinput = False
    self.isoutput = False
    
    if self.input is not None:
      self.input.outputs.append(self)
      self.layer.model.graph.add_edge(pd.Edge(
        self.input.layer.nodename,
        self.layer.nodename,
        tailport='"{}:e"'.format(self.input.nodename),
        headport='"{}:w"'.format(self.nodename),
      ))
  
  @property
  def ROI (self):
    return self._ROI
  
  def tokeras (self):
    if self.input is not None:
      self.keras = self.layer.keras(self.input.tokeras())
    return self.keras
  
  def remove (self):
    for out in self.outputs:
      out.remove()
    if self.input is not None:
      self.input.outputs.remove(self)
      graph = self.layer.model.graph
      edges = graph.obj_dict['edges'][(
        self.input.layer.nodename,
        self.layer.nodename
      )]
      for i, edge in enumerate(edges):
        if (
          edge['attributes']['headport'] ==
          '"{}:w"'.format(self.nodename)
        ):
          del edges[i]
    self.layer.outputs.remove(self)
    self.layer.updateLabel()
  

@add_unique_name_generator('layer_')
class Layer:
  CLASSNAME = None
  INFO_FIELDS = 1
  
  KERAS = None
  ONE2ONE = ['name']
  NAMED_CLASS = []
  
  # INFO = '(' NAME ',' ATTRIBUTE_LIST ')'
  # NAME = str
  # ATTRIBUTE_LIST = ATTRIBUTE [ ',' ATTRIBUTE_LIST]
  # ATTRIBUTE = str | '(' ARGS ',' KWARGS ')'
  # ARGS = list
  # KWARGS = dict
  INFO = [
    ('Layer type', 'classname', ((), {})),
  ]
  # PARAMETERS = '(' NAME ',' TYPE ',' ATTRIBUTE_PAIR [ ',' CALLBACK ] ')'
  # NAME = str
  # TYPE = BASE_TYPE | TYPE_TUPLE | TYPE_LIST
  # TYPE_TUPLE = '(' TYPES ')'
  # TYPES = BASE_TYPE [, TYPES]
  # TYPE_LIST = '[' TYPES ']'
  # ATTRIBUTE_PAIR = '(' ATTRIBUTE_LIST ')' | '((' ATTRIBUTE_LIST '),(' ATTRIBUTE_LIST '))'
  # ATTRIBUTE_LIST = ATTRIBUTE [ ',' ATTRIBUTE_LIST]
  # ATTRIBUTE = str | '(' ARGS ',' KWARGS ')'
  # ARGS = list
  # KWARGS = dict
  # CALLBACK = '(' ATTRIBUTE_LIST ')'
  PARAMETERS = [
    ('Layer name', str, ('name',), ('updateName',)),
  ]
  
  def updateName (self, wait=True):
    self.updateLabel()
    view = self.model.view
    if view is not None:
      if wait:
        view.askUpdate(500)
      else:
        view.update()
  
  def updateROI (self):      
    if self.model.annotated is not None:
      name = self.nodename
      node = self.model.annotated.get_node(name)[0]
      
      x,y = map(
        float,
        node.get_pos()[1:-1].split(",")
      )
      self._x,self._y = x,y
      self._w = w = 72*float(node.get_width())
      self._h = h = 72*float(node.get_height())
      x,y,X,Y = x-w/2,y-h/2,x+w/2,y+h/2
      self._ROI = (x,y,X,Y)
      self.updatePos()
      
      try:
        rects = cleanstr(node.get_rects())[1:-1].split(" ")[self.INFO_FIELDS:]
        for v,r in zip(self.outputs, rects):
          v._ROI = list(map(float, r.split(",")))
      except:
        import traceback
        exc = traceback.format_exc()
        print(exc)
  
  def updatePos (self):
    self.node.set_pos("{},{}!".format(self.x, self.y))
  
  def connect (self, input):
    self._lastoutput = Value(self, input)
    return self._lastoutput
  
  @property
  def x (self):
    return self._x
  
  @x.setter
  def x (self, x):
    self._x = x
    self.updatePos()
  
  @property
  def y (self):
    return self._y
  
  @y.setter
  def y (self, y):
    self._y = y
    self.updatePos()
  
  @property
  def w (self):
    return self._w
  
  @property
  def h (self):
    return self._h
  
  @property
  def ROI (self):
    return self._ROI
  
  def __init__ (self, model, *args, x=0, y=0, name=None, fromkeras=None, **kwargs):
    self.keras = fromkeras
    
    self.nodename = self.getUniqueName()
    if name is None:
      if self.keras is None:
        name = self.nodename
      else:
        name = self.keras.name
    self.name = name
    self.model = model
    
    self._lastoutput = None
    self.outputs = []
    self.inputs = []
    self._x = 0
    self._y = 0
    self._w = 0
    self._h = 0
    self._ROI = [0,0,0,0]
    
    self.node = pd.Node(
      self.nodename
    )
    self.model.graph.add_node(self.node)
    self.updateLabel()
    
    self.x = x
    self.y = y
    
    self.model.layers.append(self)
    
    
    if self.keras is not None:
      self.fromkeras(self.keras)
  
  def keras_kwargs (self):
    strornone = lambda x: None if x == 'none' else x
    kwargs = {
      param: strornone(getattr(self, param))
      for param in self.ONE2ONE+self.NAMED_CLASS
    }
    return kwargs
  
  def fromkeras (self, keras):
    self.keras = keras
    for param in self.ONE2ONE:
      setattr(self, param, getattr(self.keras, param))
    for param in self.NAMED_CLASS:
      value = getattr(self.keras, param)
      if value is None:
        value = 'none'
      else:
        try:
          value = value.__name__
        except:
          value = value.__class__.__name__
      setattr(self, param, value)
  
  def tokeras (self):
    if self.KERAS is not None:
      self.keras = self.KERAS(
        **self.keras_kwargs()
      )
  
  def classname (self):
    if self.CLASSNAME is None:
      if self.keras is None:
        return "Layer"
      else:
        return self.keras.__class__.__name__
    else:
      return self.CLASSNAME
  
  def get_label (self):
    return '<{}> {}'.format(self.classname(), self.name)
  
  def escapeLabel (self, label):
    return label.replace("<", r"\<").replace(">", r"\>")
  
  def updateLabel (self):
    e = self.escapeLabel
    
    label = [e(self.get_label())]
    for out in self.outputs:
      label.append("<{}> {}".format(e(out.nodename), e(out.name)))
    self.node.set_label(" | ".join(label))
  
  def getInfo (self):
    INFO = []
    for cls in reversed(type(self).mro()):
      INFO += cls.__dict__.get('INFO', [])
    return INFO
  
  def getParams (self):
    PARAMETERS = []
    for cls in reversed(type(self).mro()):
      PARAMETERS += cls.__dict__.get('PARAMETERS', [])
    return PARAMETERS
  
  def remove (self):
    for out in self.outputs:
      out.remove()
    self.model.graph.del_node(self.nodename)
    edges = self.model.graph.obj_dict['edges']
    self.model.layers.remove(self)
    print("\n".join(["{} : {}".format(k, len(v)) for k,v in edges.items()]))
    print(self.nodename)

layers_dir = os.path.dirname(os.path.abspath(__file__))
available_layers = SubmoduleImporter(__name__, layers_dir)
available_layers.reload()

def add_layer (model, *args, fromkeras=None, **kwargs):
  cls = Layer
  if fromkeras is not None:
    if fromkeras.__class__ in available_layers:
      cls = available_layers[fromkeras.__class__]
  return cls(model, *args, fromkeras=fromkeras, **kwargs)
