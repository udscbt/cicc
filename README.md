# CICC

Complete Interface for Complex Classifications

Note: to use the cicc.normalize.landmarks module, you have to copy a dlib
shape predictor model in the cicc/normalize folder and name it
shape_predictor_68_face_landmarks.dat
A suitable model can be found at https://github.com/davisking/dlib-models, but
it can not be used for commercial purposes.
