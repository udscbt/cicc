import numpy as np
import cv2
import os, shutil
import sys
import argparse

########################################################################
# GENERIC UTILS
################

def get_disk_crossplatform(label, path):
  global disk
  if sys.platform == 'win32':
    return label
  else:
    return path

def make_path(disk, path):
  return os.path.join(disk, path.split("\\").split("/"))

class SubmoduleImporter (dict):
  import importlib
  import os
  
  def __init__ (self, package, path, allowsubpackages=True):
    dict.__init__(self)
    self.package = package
    self.path = path
    self.packages = allowsubpackages
    self.modules = []
    # ~ self.reload()
  
  def reload (self):
    modules = {}
    for f in self.os.listdir(self.path):
      if f.startswith('_'):
        continue
      if os.path.isdir(os.path.join(self.path, f)):
        if self.packages:
          f = f+".py"
        else:
          continue
      if f.endswith('.py'):
        modulename = os.path.splitext(f)[0]
        modulename = self.package+'.'+modulename
        if modulename in self.modules:
          module = self.importlib.reload(self.modules[modulename])
        else:
          module = self.importlib.import_module(modulename)
        modules[modulename] = module
    self.modules = modules

# ~ default_algorithms = {'geometry': {}, 'brightness': {}}
# ~ def add_algorithm(name, algorithm, type='geometry'):
  # ~ global default_algorithms
  # ~ d = default_algorithms[type]
  # ~ if name in d.keys():
    # ~ warn(
      # ~ "A {} algorithm named '{}'".format(type, name)+
      # ~ "has already been registered.",
      # ~ ImportWarning
    # ~ )
  # ~ d[name]=algorithm

# ~ def correct_brightness (img, name):
  # ~ if name not in default_algorithms['brightness']:
    # ~ raise ValueError(
      # ~ "Unregistered brightness algorithm: '{}'".format(algorithm)
    # ~ )
  # ~ return default_algorithms['brightness'][name](img)

# ~ algorithms_dir = os.path.dirname(os.path.abspath(__file__))
# ~ for f in os.listdir(algorithms_dir):
  # ~ if f.endswith('.py') and f != '__init__.py':
    # ~ module = os.path.splitext(f)[0]
    # ~ importlib.import_module(__name__+'.'+module)

# ~ brightness_dir = os.path.join(algorithms_dir, 'brightness')
# ~ for f in os.listdir(brightness_dir):
  # ~ if f.endswith('.py') and f != '__init__.py':
    # ~ module = os.path.splitext(f)[0]
    # ~ importlib.import_module(__name__+'.brightness.'+module)


########################################################################
