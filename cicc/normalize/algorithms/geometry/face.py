try:
  import cicc.normalize.algorithms as alg
except AttributeError:
  from ... import algorithms as alg
from .basic import stretch, extend, fit

def facecrop (fn):
  def normalize (IN, img, highres=None):
    import cv2
    from cicc.normalize import landmarks
    
    if img.ndim == 3 and img.shape[2] == 3:
      gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    else:
      gray = img
    faces = landmarks.get_face(gray, 1)
    
    ret = []
    if highres is not None:
      img = highres
    for face in faces:
      ret.append(fn(IN, img[face.top():face.bottom(),face.left():face.right()])[0])
    
    return ret
  return normalize

alg.default_algorithms['geometry']['crop face + stretch'] = facecrop(stretch)
alg.default_algorithms['geometry']['crop face + extend'] = facecrop(extend)
alg.default_algorithms['geometry']['crop face + fit'] = facecrop(fit)
