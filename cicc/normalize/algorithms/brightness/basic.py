try:
  import cicc.normalize.algorithms as alg
except AttributeError:
  from ... import algorithms as alg

def linear (img):
  import numpy as np
  import cv2
  
  img = img.astype(np.float32)
  img = img - img.min()
  img = img * 255 / img.max()
  return img.astype(np.uint8)

def gaussian (img):
  import numpy as np
  
  mean = img.mean()
  std  = img.std()
  img = (img-mean)/std
  img = (img*128/2.5)+128
  img = np.clip(img, 0, 255)
  
  return img.astype(np.uint8)

alg.default_algorithms['brightness']["none"] = lambda x:x
alg.default_algorithms['brightness']["linear"] = linear
alg.default_algorithms['brightness']["gaussian"] = gaussian
