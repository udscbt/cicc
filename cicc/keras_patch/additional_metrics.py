import keras
from keras.layers import Layer
import keras.backend as K
import numpy as np
from . import patches
from . import make_multidim


def prob_to_onehot(tensor):
    onlymax = K.equal(
        tensor,
        K.reshape(  # to allow comparison between tensors
            K.max(tensor, 1), (None, 1)))
    return onlymax#/K.reshape(K.sum(onlymax, 1), (None,1))


class CrossTotal(Layer):

    def __init__(self, true_class, pred_class, num_classes):
        Layer.__init__(self, name='{}_vs_{}'.format(true_class, pred_class))
        self.stateful = True

        self.true_class = K.variable(
            keras.utils.to_categorical(true_class, num_classes))
        self.pred_class = K.variable(
            keras.utils.to_categorical(pred_class, num_classes))

        self.total = K.variable(value=0, dtype='int32')

    def reset_states(self):
        K.set_value(self.total, 0)

    def __call__(self, y_true, y_pred):
        batch_total = K.cast(
            K.sum(
                K.prod([
                    K.equal(K.argmax(y_true), K.argmax(self.true_class)),
                    K.equal(K.argmax(y_pred), K.argmax(self.pred_class))
                ])), 'int32')
        updates = [K.update_add(self.total, batch_total)]
        self.add_update(updates, inputs=[y_true, y_pred])
        return self.total

    @property
    def score(self):
        return self.total.eval()


def confusion_matrix_metrics(num_classes):
    return [
        CrossTotal(i, j, num_classes)
        for i in range(num_classes)
        for j in range(num_classes)
    ]


@make_multidim
class ConfusionMatrix(Layer):

    def __init__(self, num_classes):
        Layer.__init__(self, name='confusion_matrix')
        self.num_classes = num_classes

        self.stateful = True

        self.confusion = K.zeros((num_classes, num_classes))

    def reset_states(self):
        K.set_value(self.confusion,
                    np.zeros((self.num_classes, self.num_classes)))

    def __call__(self, y_true, y_pred):
        pred_onehot = prob_to_onehot(y_pred)

        true_mat = K.permute_dimensions(
            K.repeat(y_true, self.num_classes),
            [0, 2, 1]  # transposition except for first axis
        )
        pred_mat = K.repeat(pred_onehot, self.num_classes)
        positions = true_mat * pred_mat

        confusion = K.sum(positions, 0)

        updates = [K.update_add(self.confusion, confusion)]
        self.add_update(updates, inputs=[y_true, y_pred])
        return self.confusion

    @property
    def score(self):
        return self.confusion.eval()


@make_multidim
class Recalls(Layer):

    def __init__(self):
        Layer.__init__(self, name='recall')
        self.stateful = True
        self.true_positives = K.variable(value=0, dtype='float32')
        self.total_positives = K.variable(value=0, dtype='float32')

    def reset_states(self):
        K.set_value(self.true_positives, 0.0)
        K.set_value(self.total_positives, 0.0)

    def __call__(self, y_true, y_pred):
        '''Update recall computation.
    # Arguments
        y_true: Tensor, batch_wise labels
        y_pred: Tensor, batch_wise predictions
    # Returns
        Overall recall for the epoch at the completion of the batch.
    '''
        # Batch
        correct_samples = K.equal(K.argmax(y_true), K.argmax(y_pred))
        true_positives = K.sum(K.prod([correct_samples, y_true]), 0)
        total_positives = K.sum(y_true, 0)
        # Current
        current_true_positives = self.true_positives * 1
        current_total_positives = self.total_positives * 1
        # Updates
        updates = [
            K.update_add(self.true_positives, true_positives),
            K.update_add(self.total_positives, total_positives)
        ]
        self.add_update(updates, inputs=[y_true, y_pred])
        # Compute recall
        return (current_true_positives + true_positives) / \
               (current_total_positives + total_positives + K.epsilon())


patches(keras.metrics, 'recall')(Recalls())


@make_multidim
class Total(Layer):

    def __init__(self):
        Layer.__init__(self, name='total')
        self.stateful = True
        self.total = K.variable(value=0, dtype='float32')

    def reset_states(self):
        K.set_value(self.total, 0.0)

    def __call__(self, y_true, y_pred):
        # ~ correct_samples = K.equal(K.argmax(y_true), K.argmax(y_pred))
        # ~ true_positives  = K.sum(K.prod([correct_samples, y_true]), 0)
        self.total_positives = K.sum(y_true, 0)
        # Updates
        updates = [
            K.update_add(self.total, self.total_positives),
        ]
        self.add_update(updates, inputs=[y_true, y_pred])
        # Compute recall
        return self.total

@patches(keras.metrics, 'pseudoaccuracy')
def pseudoaccuracy (y_true, y_pred):
    return K.max(K.prod([prob_to_onehot(y_pred),y_true]))

patches(keras.metrics, 'total')(Total())
