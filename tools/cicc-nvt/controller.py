import pydot as pd
import keras
from keras import backend as K
import view as view_module
from layers import Layer, Value, add_layer
from layers.input import Input

class ModelIntegrator:
  LAYOUTS = ['dot', 'neato', 'twopi', 'fdp', 'sfdp']
  
  # Dot command to generate graph
  COMMAND = ['dot', '-Kneato', '-n']
  CMD_NOPOS = lambda self, layout: [
    'dot',
    '-K{}'.format(layout),
    '-Goverlap=false',
    '-Gsep=0.5'
  ]
  
  def __init__ (self, *args, **kwargs):
    self.view = None
    self.layers = []
    self.inputs = []
    self.outputs = []
    self.keras = None
    self.setupGraph()
  
  def setupGraph (self):
    self.graph = pd.Dot()
    self.graph.add_node(pd.Node('node'))
    self.graph.add_node(pd.Node('edge'))
  
  def graphElementFromName (self, name):
    try:
      if isinstance(name, tuple):
        return self.graph.get_edge(name)[0]
      else:
        return self.graph.get_node(name)[0]
    except:
      return None
  
  def elementFromPosition (self, x, y):
    search_space = [
      output for layer in self.layers for output in layer.outputs
    ]
    search_space += self.layers
    
    for element in search_space:
      rx,ry,rX,rY = element.ROI
      if (
        x > rx and x < rX and
        y > ry and y < rY
      ):
        if isinstance(element, Value):
          return (element.layer, element)
        elif isinstance (element, Layer):
          return (element, None)
        else:
          raise Exception("Unrecognized element: {}".format(repr(element)))
    return (None, None)
  
  def addLayer (self, x, y):
    add_layer(self, x=x, y=y)
  
  def reposition (self, layout='fdp'):
    for layer in self.layers:
      attr = layer.node.obj_dict['attributes']
      if 'pos' in attr:
        attr.pop('pos')
    
    # Render graph
    try:
      self.annotated = pd.graph_from_dot_data(
        self.graph.create_dot(
          prog=self.CMD_NOPOS(layout)
        ).decode()
      )[0]
    except FileNotFoundError:
      raise FileNotFoundError(
        "The required Graphviz executables were not found. "+
        "Install them from https://www.graphviz.org/"
      )
    
    # Zeroing positions
    x,_,_,y = map(
      float,
      self.annotated.get_bb()[1:-1].split(",")
    )
    for layer in self.layers:
      layer.updateROI()
      layer.x -= x
      layer.y -= y
  
  def update (self):
    # Render graph
    try:
      self.annotated = pd.graph_from_dot_data(
        self.graph.create_dot(
          prog=self.COMMAND
        ).decode()
      )[0]
    except FileNotFoundError:
      raise FileNotFoundError(
        "The required Graphviz executables were not found. "+
        "Install them from https://www.graphviz.org/"
      )
    
    # Setup ROIs
    for layer in self.layers:
      layer.updateROI()
  
  def _recursive (self, tensor, keras2view):
    if tensor in self._reclist:
      return self._reclist[tensor]
    
    layer, node, _ = tensor._keras_history
    node = layer._inbound_nodes[node]
    
    if tensor in self.keras.inputs:
      v = keras2view[layer]._lastoutput
      v.isinput = True
      self.inputs.append(v)
    else:
      L = keras2view[layer]
      
      for newtensor in node.input_tensors:
        value = self._recursive(newtensor, keras2view)
        L.connect(value)
      
      v = L._lastoutput
    
    self._reclist[tensor] = v
    if tensor in self.keras.outputs:
      v.isoutput = True
      self.outputs.append(L._lastoutput)
    
    v.keras = tensor
    v.keras_f = K.function(
      self.keras.inputs,
      [tensor]
    )
    
    return v
  
  def loadHDF5 (self, h5, layout=None):
    self.loadKerasModel(keras.models.load_model(h5))
  
  def loadKerasModel (self, model, layout=None):
    self.setupGraph()
    if self.view is not None:
      self.view.setupGraph()
    self.keras = model
    self.layers = []
    self.inputs = []
    self.outputs = []
    keras2view = {}
    for layer in self.keras.layers:
      keras2view[layer] = add_layer(self, fromkeras=layer)
    
    self._reclist = {}
    for tensor in self.keras.outputs:
      self._recursive(tensor, keras2view)
    
    # Recalculate graph
    kwargs = {}
    if layout is not None:
      kwargs['layout'] = layout
    self.reposition(**kwargs)
    self.update()
  
  def saveHDF5 (self, h5):
    self.keras.save(h5)
