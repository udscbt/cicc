from .. import (
  Layer, Value,
  add_unique_name_generator,
  available_layers
)

import keras

@add_unique_name_generator('maxpool_')
class Maxpool (Layer):
  CLASSNAME = 'Max Pooling'
  KERAS = keras.layers.MaxPooling2D
  
  PARAMETERS = [
    (
      'Pool size', (int, int), ('pool_size',)
    ), (
      'Strides', (int, int), ('strides',)
    ), (
      'Padding type', ['valid', 'same'], ('padding',)
    ), (
      'Data format',
      ['channels_last', 'channels_first'],
      ('data_format',)
    ),
  ]
  
  ONE2ONE = [
    'pool_size',
    'strides',
    'padding',
    'data_format',
  ]
  
  def __init__ (self, *args, **kwargs):
    if kwargs.get('keras', None) is None:
      kwargs['keras'] = keras.layers.MaxPooling2D()
    Layer.__init__(self, *args, **kwargs)

available_layers[Maxpool.KERAS] = Maxpool

