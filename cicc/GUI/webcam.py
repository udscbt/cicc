from collections import defaultdict
import threading
import cv2

class Webcam (threading.Thread):
  # `cap` is shared between all instances
  _cap = {}
  @property
  def cap (self):
    return Webcam._cap.get(self.port, None)
  @cap.setter
  def cap (self, value):
    Webcam._cap[self.port] = value
  
  _active_count = defaultdict(lambda: 0)
  _active = False
  @property
  def active (self):
    return self._active
  @active.setter
  def active (self, value):
    if value and not self._active: # Not active -> active
      if Webcam._active_count[self.port] == 0: # No active instances yet
        if self.cap is None: # Never started before
          self.cap = cv2.VideoCapture(self.port)
          self.cap.set(cv2.CAP_PROP_BUFFERSIZE, 1)
        else: # Just need reopen
          self.cap.open(self.port)
      Webcam._active_count[self.port] += 1
    elif not value and self._active: # Active -> not active
      Webcam._active_count[self.port] -= 1
      if Webcam._active_count[self.port] == 0: # No active instances remaining
        self.cap.release()
    self._active = value
  
  main_lock = threading.Lock()
  @property
  def lastread (self):
    return Webcam._lastread
  @lastread.setter
  def lastread (self, value):
    Webcam._lastread = value
  
  def __init__ (self, callback=None, port=0):
    self.callback = callback
    self.port = port
    
    self.update_lock = threading.Condition()
    self.stop_lock = threading.Condition()
    
    self.enqueued = False
    self.started = False
    self.stopped = False
    self.really_stopped = False
    super().__init__()
  
  def open (self):
    try:
      self.active = True
    except Exception as e:
      print(type(e), e.args)
      self.active = False
    
  def close (self):
    self.active = False
  
  def read (self):
    if self.active:
      if self.main_lock.acquire(False): # No concurrent read
        reraise_later = None
        try:
          self.lastread = self.cap.read()[1] # Actually read from webcam
        except Exception as e: # Postpone exception until lock release
          reraise_later = e
        self.main_lock.release()
        if reraise_later is not None:
          raise reraise_later
      else: # Concurrent read
        self.main_lock.acquire(True) # Just wait for other read to finish
        self.main_lock.release()
      return self.lastread
    else:
      return None

  def start (self):
    self.open()
    self.update_lock.acquire()
    self.started = True
    self.update_lock.notify()
    self.update_lock.release()
    if not self._started.is_set():
      super().start()
    
  def stop (self):
    self.update_lock.acquire()
    self.started = False
    self.close()
    self.update_lock.notify()
    self.update_lock.release()

  def hard_stop (self):
    self.update_lock.acquire()
    self.started = False
    self.stopped = True
    self.update_lock.notify()
    self.update_lock.release()
  
  def wait_stop (self):
    while True:
      self.stop_lock.acquire()
      if self.really_stopped:
        self.stop_lock.release()
        return
      self.stop_lock.wait()
      self.stop_lock.release()

  def update (self):
    self.update_lock.acquire()
    self.enqueued = True
    self.update_lock.notify()
    self.update_lock.release()

  def run (self):
    while True:
      self.update_lock.acquire()
      
      ## Note: no changes can happen between these checks due to the lock ##
      
      # Check if stopped, in that case break
      if self.stopped:
        self.update_lock.release()
        break
        
      # Check if started, otherwise wait for changes and retry
      if not self.started:
        self.update_lock.wait()
        self.update_lock.release()
        continue
      
      # Check if something is queued, otherwise wait for changes and retry
      if not self.enqueued:
        self.update_lock.wait()
        self.update_lock.release()
        continue
      
      ## Checks finished:                  ##
      ## Webcam started and request queued ##
      
      # Request will be processed, clear queue
      self.enqueued = False
      self.update_lock.release()
      
      if self.callback is not None:
        self.callback(self.read())
          
    self.close()
    self.stop_lock.acquire()
    self.really_stopped = True
    self.stop_lock.notify()
    self.stop_lock.release()

class WebcamApp:
  def __init__ (self, func=None):
    self.source = Webcam()
    self._loop = func
  
  def loop (self, img, button, key):
    if self._loop is None:
      cv2.imshow('Webcam', img)
      return True
    else:
      return self._loop(self, img, button, key)
  
  def start (self, stop=(None, ord('q'))):
    self.source.open()
    btncheck = stop[0] or -1
    keycheck = stop[1] or -1
    while True:
      img = self.source.read()
      button = cv2.waitKeyEx(1)
      key = None
      if button == -1:
        button = None
      else:
        key = button & 0xFF
      if button == btncheck or key == keycheck:
        break
      if not self.loop(img, button, key):
        break
    self.source.close()
    cv2.destroyAllWindows()
