import numpy as np
import cv2
import os

#Algorithms
from .algorithms import default_algorithms

class ImageNormalizer:
  def __init__ (self, input_shape, algorithm=None):
    self.input_shape = input_shape
    if algorithm is not None:
      if isinstance(algorithm, str):
        if algorithm in default_algorithms['geometry'].keys():
          self._algorithm = default_algorithms['geometry'][algorithm]
        else:
          raise ValueError(
            "Unregistered geometry algorithm: '{}'".format(algorithm)
          )
      else:
        calltest = algorithm.__call__
        self._algorithm = algorithm
  
  def normalize (self, img, highres=None, *args, **kwargs):
    if self._algorithm is None:
      return [img] if highres is None else [highres]
    else:
      return self._algorithm(self, img, highres, *args, **kwargs)

input_shape = (128, 128, 1)
output_size = 8
output_interpretation = [
  'anger',
  'contempt',
  'disgust',
  'fear',
  'happiness',
  'neutral',
  'sadness',
  'surprise'
]
output_interpretation_extended = output_interpretation + [
  'none',
  'uncertain',
  'no-face',
]
